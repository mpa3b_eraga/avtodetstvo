<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$title = 'Производители';

$p = explode('?', $_SERVER['REQUEST_URI']);
$path = $p[0];

$parts = explode('/', $path);
$parts = array_values(array_filter($parts));

switch (count($parts)) {
    case 1:
        // Общий каталог
        break;
    case 2:
        // Бренд
        $dbRes = CIBlockElement::GetList([], [
            'IBLOCK_ID' => 11,
            'CODE' => $parts[1]
        ]);
        $brand = $dbRes->GetNext();
        if ($brand) {
            // Бренд найден
            $APPLICATION->AddChainItem($brand['NAME'], $_SERVER['REQUEST_URI']);
            $title = $brand['NAME'];
            $arrFilter['PROPERTY_BRAND'] = [$brand['ID']];
        } else {
            // Не найден бренд
            include $_SERVER['DOCUMENT_ROOT'].'/404.php';
            exit;
        }
        break;
    default:
        // Не найден бренд
        include $_SERVER['DOCUMENT_ROOT'].'/404.php';
        exit;
        break;
}

include '../catalog/.sections.php';
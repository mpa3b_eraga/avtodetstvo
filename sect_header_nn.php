<?
global $shopFilter;
$id = CCatalogStore::GetList([], [
    'IBLOCK_ID' => 14,
    'UF_SUBDOMAIN' => 'nn'
], false, false, [
    'UF_SUBDOMAIN'
])->GetNext()['ID'];
$shopFilter = [
    'PROPERTY_STORE' => $id
];
?>

<div id="shops" class="three-fifths unit">

    <h2 class="title center-on-mobiles">Наши магазины:</h2>
    <div class="grid no-stacking-on-mobiles">

        <div id="shops-list" class="two-thirds unit">
            <? $APPLICATION->IncludeComponent(
                "bitrix:news.list",
                "shops-list",
                array(
                    "ACTIVE_DATE_FORMAT" => "d.m.Y",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "AJAX_MODE" => "N",
                    "AJAX_OPTION_ADDITIONAL" => "",
                    "AJAX_OPTION_HISTORY" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "CACHE_FILTER" => "N",
                    "CACHE_GROUPS" => "Y",
                    "CACHE_TIME" => "36000000",
                    "CACHE_TYPE" => "A",
                    "CHECK_DATES" => "Y",
                    "COMPONENT_TEMPLATE" => "shops-list",
                    "DETAIL_URL" => "",
                    "DISPLAY_BOTTOM_PAGER" => "N",
                    "DISPLAY_DATE" => "N",
                    "DISPLAY_NAME" => "N",
                    "DISPLAY_PICTURE" => "N",
                    "DISPLAY_PREVIEW_TEXT" => "N",
                    "DISPLAY_TOP_PAGER" => "N",
                    "FIELD_CODE" => array(
                        0 => "",
                        1 => "",
                    ),
                    "FILTER_NAME" => "shopFilter",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "IBLOCK_ID" => "14",
                    "IBLOCK_TYPE" => "content",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "INCLUDE_SUBSECTIONS" => "Y",
                    "MESSAGE_404" => "",
                    "NEWS_COUNT" => "",
                    "PAGER_BASE_LINK_ENABLE" => "N",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => ".default",
                    "PAGER_TITLE" => "Новости",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => "",
                    "PREVIEW_TRUNCATE_LEN" => "",
                    "PROPERTY_CODE" => array(
                        0 => "ADDRESS",
                        1 => "LOCATION",
                        2 => "",
                    ),
                    "SET_BROWSER_TITLE" => "N",
                    "SET_LAST_MODIFIED" => "N",
                    "SET_META_DESCRIPTION" => "N",
                    "SET_META_KEYWORDS" => "N",
                    "SET_STATUS_404" => "N",
                    "SET_TITLE" => "N",
                    "SHOW_404" => "N",
                    "SORT_BY1" => "ACTIVE_FROM",
                    "SORT_BY2" => "SORT",
                    "SORT_ORDER1" => "DESC",
                    "SORT_ORDER2" => "ASC"
                ),
                false
            ); ?>
        </div>

        <div id="shops-message" class="one-third  unit align-left">
            <p>
                <span id="shops-message-time">9<sup>00</sup> — 20<sup>00</sup></span>
                <br>
                <span style="font-size:0.9em;">каждый день</span>
            </p>
        </div>

    </div>

</div>

<div id="contact" class="two-fifths unit align-right center-on-mobiles">
    <div class="grid">

        <aside id="contact-block" class="align-left pull-right">

            <h2 class="title">Единый телефон для справок</h2>
            <p>
                <a id="contact-phone" href="tel:89307070103">+7 930 707 01 06</a>
            </p>
            <p>
                <a href="#callback">Заказать обратный звонок</a>
            </p>

            <? $APPLICATION->IncludeComponent(
                "bitrix:form",
                "webform",
                Array(
                    "AJAX_MODE" => "N",
                    "AJAX_OPTION_ADDITIONAL" => "",
                    "AJAX_OPTION_HISTORY" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "CACHE_TIME" => "3600",
                    "CACHE_TYPE" => "A",
                    //            "CHAIN_ITEM_LINK" => "Found cheaper?",
                    //            "CHAIN_ITEM_TEXT" => "Нашли дешевле?",
                    "EDIT_ADDITIONAL" => "N",
                    "EDIT_STATUS" => "N",
                    "IGNORE_CUSTOM_TEMPLATE" => "Y",
                    "NOT_SHOW_FILTER" => array("", ""),
                    "NOT_SHOW_TABLE" => array("", ""),
                    "RESULT_ID" => $_REQUEST[RESULT_ID],
                    "SEF_MODE" => "N",
                    "SHOW_ADDITIONAL" => "N",
                    "SHOW_ANSWER_VALUE" => "N",
                    "SHOW_EDIT_PAGE" => "N",
                    "SHOW_LIST_PAGE" => "N",
                    "SHOW_STATUS" => "N",
                    "SHOW_VIEW_PAGE" => "N",
                    "START_PAGE" => "new",
                    "SET_NAVIGATION" => "N",
                    "SUCCESS_URL" => "",
                    "USE_EXTENDED_ERRORS" => "N",
                    "VARIABLE_ALIASES" => Array(
                        "action" => "action"
                    ),
                    "WEB_FORM_ID" => "2"
                )
            ); ?>

        </aside>

    </div>
</div>
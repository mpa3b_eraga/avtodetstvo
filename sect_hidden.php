<div id="one-click-buy">

    <?
    $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/lib/jquery.js');
    $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/lib/mask/jquery.mask.js');
    $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/one-click-buy.js');

    $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/one-click-buy.css');
    ?>

    <div class="wrapper">

        <div class="grid product">

            <div class="two-fifths unit align-center center-on-mobiles">
                <img class="image"/>
            </div>

            <div class="three-fifths unit center-on-mobiles">
                <h2 class="name title"></h2>
                <p class="colorname"><span class="label">Цвет: </span> <span class="value"></span></p>
                <p class="price"></p>
            </div>

        </div>
    
        <div class="grid no-gutters">

            <div class="whole unit">

                <h2 class="align-center title">Оставьте свои контакты и мы вам перезвоним</h2>

                <form action="/ajax/one-click-buy.php" name="one-click-buy" id="one-click-buy-form">

                    <input type="hidden" name="one-click-buy-form-product-id">

                    <div class="grid">
                        <div class="name field one-third unit">
                            <label class="hidden" for="one-click-buy-form-name">Ваши имя и фамилия</label>
                            <input type="text" name="one-click-buy-form-name" id="one-click-buy-form-name"
                                   placeholder="Иван Петров">
                        </div>

                        <div class="phone required field one-third unit">
                            <label class="hidden" for="one-click-buy-form-phone">Ваш телефон</label>
                            <input type="text" name="one-click-buy-form-phone" id="one-click-buy-form-phone"
                                   placeholder="+7 987 654 32 10">
                        </div>

                        <div class="submit field one-third unit">
                            <button type="submit">Оформить заказ</button>
                        </div>
                    </div>

                </form>

            </div>

        </div>

        <div class="loader"></div>

    </div>

    <div class="result hidden">
        <h2 class="title"></h2>
        <p class="body"></p>
    </div>

</div>

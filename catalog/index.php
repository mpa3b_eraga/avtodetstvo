<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");

$title = 'Каталог';

$p = explode('?', $_SERVER['REQUEST_URI']);
$path = $p[0];

$parts = explode('/', $path);
$parts = array_values(array_filter($parts));

switch (count($parts)) {
    case 1:
        // Общий каталог
        break;
    case 2:
        // Раздел каталога
        $dbRes = CIBlockSection::GetList([], [
            'CODE' => $parts[1]
        ]);
        $section = $dbRes->GetNext();
        if ($section) {
            // Раздел найден
            $APPLICATION->AddChainItem($section['NAME'], $_SERVER['REQUEST_URI']);
            $title = $section['NAME'];
            $arrFilter['SECTION_ID'] = [$section['ID']];
        } else {
            // Не найден раздел
            include $_SERVER['DOCUMENT_ROOT'].'/404.php';
            exit;
        }
        break;
    default:
        // Страница товара
        include '.element.php';
        exit;
        break;
}

include '.sections.php';

?>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>

<p>
    Доставка курьером по городу при заказе от <strong>6000 руб. бесплатно</strong>.
</p>
<p>
    При заказе до 17:00 доставка <strong>в день заказа</strong>.
</p>
<p>
    После 17:00 — на следующий день.
</p>

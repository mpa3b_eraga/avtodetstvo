<?

if ($title) $APPLICATION->SetTitle($title);

///// Фильтры
$arrFilter['ACTIVE'] = 'Y';

// цена
$priceId = 5;
$price_min = (isset($_REQUEST['price_min'])) ? $_REQUEST['price_min'] : 1;
$price_max = (isset($_REQUEST['price_max'])) ? $_REQUEST['price_max'] : 99999;

$arrFilter[] = array(
    '>=CATALOG_PRICE_' . $priceId => $price_min
);

$arrFilter[] = array(
    '<=CATALOG_PRICE_' . $priceId => $price_max
);

// ajax-фильтры

if ($_REQUEST['AJAX'] == 'Y') {

    $arrFilter['PROPERTY_BRAND'] = [];
    $arrFilter['SECTION_ID'] = [];

    if (isset($_REQUEST['SECTIONS']) && !empty($_REQUEST['SECTIONS'])) {
        foreach ($_REQUEST['SECTIONS'] as $section_id)
            $arrFilter['SECTION_ID'][] = $section_id;
    }

    if (!empty($_REQUEST['properties'])) {

        foreach ($_REQUEST['properties'] as $propId => $values) {
            $property = CIBlockElement::GetProperty(
                6,
                $propId,
                [],
                ['ID' => $propId]
            )->Fetch()['CODE'];
            $array = [];
            foreach ($values as $value)
                $array[] = $value;
            $arrFilter['PROPERTY_' . $property] = $array;
        }

    }

    if ($_REQUEST['crush-tested']) {
        $arrFilter['!PROPERTY_CRASH_TEST_RESULTS'] = false;
    }

    if ($_REQUEST['avaliable']) {
        $arrFilter['>CATALOG_QUANTITY'] = 0;
    }

}
?>

    <div id="catalog-section" class="catalog sections grid">

        <aside id="catalog-section-left" class="one-quarter unit">

            <? $APPLICATION->IncludeComponent(
                "bitrix:catalog.smart.filter",
                "",
                array(
                    "IBLOCK_TYPE" => 'catalog',
                    "IBLOCK_ID" => 6,
//                "SECTION_ID" => $arCurSection['ID'],
                    "FILTER_NAME" => 'arrFilter',
                    "PRICE_CODE" => ['BASE'],
                    "CACHE_TYPE" => 'A',
                    "CACHE_TIME" => 36000000,
                    "CACHE_GROUPS" => 'N',
                    "SAVE_IN_SESSION" => "N",
//                "FILTER_VIEW_MODE" => $arParams["FILTER_VIEW_MODE"],
                    "XML_EXPORT" => "N",
                    "SECTION_TITLE" => "NAME",
                    "SECTION_DESCRIPTION" => "DESCRIPTION",
                    'HIDE_NOT_AVAILABLE' => 'N',
//                "TEMPLATE_THEME" => $arParams["TEMPLATE_THEME"],
                    'CONVERT_CURRENCY' => 'N',
//                'CURRENCY_ID' => $arParams['CURRENCY_ID'],
                    "SEF_MODE" => 'Y',
//                "SEF_RULE" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["smart_filter"],
//                "SMART_FILTER_PATH" => $arResult["VARIABLES"]["SMART_FILTER_PATH"],
//                "PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
                    "FILTER_TITLE" => 'Фильтр'
                ),
                $component,
                array('HIDE_ICONS' => 'Y')
            ); ?>

        </aside>

        <div id="catalog-sections" class="three-quarters unit catalog section">

            <? if ($brand) : ?>
                <section id="section-description" class="grid">
                    <div class="whole unit">
                        <? if ($brand['DETAIL_PICTURE']) { ?>
                            <? $picture = CFile::ResizeImageGet(
                                $brand['DETAIL_PICTURE'],
                                ['width' => 200, 'height' => 200]
                            ); ?>
                            <img src="<?= $picture['src'] ?>" style="float:left; margin:0 2em 2em 0;"/>
                        <? } ?>
                        <? print $brand['DETAIL_TEXT']; ?></div>
                </section>
            <? endif; ?>

            <? if ($section) : ?>
                <section id="section-description" class="grid">
                    <div class="whole unit"><? print $section['DESCRIPTION']; ?></div>
                </section>
            <? endif; ?>

            <?
            if ($_REQUEST["CATALOG_SORT_MODE"])
                $_REQUEST["CATALOG_SORT_MODE"] = 'DEFAULT';
            switch ($_REQUEST["CATALOG_SORT_MODE"]) :
                case 'PRICE_ASC':
                    $arParams["ELEMENT_SORT_FIELD"] = "CATALOG_PRICE_" . $priceId;
                    $arParams["ELEMENT_SORT_ORDER"] = "asc";
                    break;
                case 'PRICE_DSC':
                    $arParams["ELEMENT_SORT_FIELD"] = "CATALOG_PRICE_" . $priceId;
                    $arParams["ELEMENT_SORT_ORDER"] = "desc";
                    break;
                case 'NAME_ASC':
                    $arParams["ELEMENT_SORT_FIELD"] = "name";
                    $arParams["ELEMENT_SORT_ORDER"] = "asc";
                    break;
                case 'NAME_DSC':
                    $arParams["ELEMENT_SORT_FIELD"] = "name";
                    $arParams["ELEMENT_SORT_ORDER"] = "desc";
                    break;
                default:
                    $arParams["ELEMENT_SORT_FIELD"] = "shows";
                    $arParams["ELEMENT_SORT_ORDER"] = "desc";
                    $_REQUEST['CATALOG_SORT_MODE'] = 'DEFAULT';
                    break;
            endswitch;
            ?>

            <aside id="catalog-sort" class="pull-right center-on-mobiles">
                <form id="catalog-sort-form" name="CATALOG_SORT" method="post">
                    <label for="catalog-sort-form-select">Сортировать </label>
                    <?
                    $CATALOG_SORT_MODE = array(
                        'DEFAULT' => 'по умолчанию',
                        'PRICE_ASC' => 'по цене (дешевле)',
                        'PRICE_DSC' => 'по цене (дороже)',
                        'NAME_ASC' => 'по названию (А-Я)',
                        'NAME_DSC' => 'по названию (Я-А)'
                    );
                    ?>
                    <select name="CATALOG_SORT_MODE" id="catalog-sort-form-select">
                        <? foreach ($CATALOG_SORT_MODE as $value => $option) : ?>
                            <option
                                    value="<? echo $value ?>" <? if ($_REQUEST['CATALOG_SORT_MODE'] == $value) echo 'selected="selected"'; ?>><? echo $option ?></option>
                        <? endforeach; ?>
                    </select>
                </form>
            </aside>

            <? if ($_REQUEST['AJAX'] == 'Y') $APPLICATION->RestartBuffer(); ?>

            <? $APPLICATION->IncludeComponent(
                "bitrix:catalog.top",
                "",
                array(
                    "DISPLAY_TOP_PAGER" => 'N',
                    "DISPLAY_BOTTOM_PAGER" => 'Y',
                    "IBLOCK_TYPE" => 'catalog',
                    "IBLOCK_ID" => '6',
                    "FILTER_NAME" => 'arrFilter',
                    "ELEMENT_SORT_FIELD" => $arParams["ELEMENT_SORT_FIELD"],
                    "ELEMENT_SORT_ORDER" => $arParams["ELEMENT_SORT_ORDER"],
                    //                "ELEMENT_SORT_FIELD2" => $arParams["TOP_ELEMENT_SORT_FIELD2"],
                    //                "ELEMENT_SORT_ORDER2" => $arParams["TOP_ELEMENT_SORT_ORDER2"],
//                "SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
//                "DETAIL_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["element"],
                    "BASKET_URL" => '/cart/',
                    "ACTION_VARIABLE" => 'action',
                    "PRODUCT_ID_VARIABLE" => 'id',
                    "SECTION_ID_VARIABLE" => 'SECTION_CODE',
                    "PRODUCT_QUANTITY_VARIABLE" => '',
                    "PRODUCT_PROPS_VARIABLE" => 'prop',
                    "DISPLAY_COMPARE" => 'N',
                    "TOP_PER_PAGE" => 12,
                    "ELEMENT_COUNT" => 9999999999,
                    "LINE_ELEMENT_COUNT" => 3,
                    "PROPERTY_CODE" => [
                        0 => "VIDEO",
                        1 => "WEIGHT_GROUP",
                        2 => "WARRANTY",
                        3 => "TIER",
                        4 => "SIMILAR_CARCHAIR",
                        5 => "BRAND",
                        6 => "CRASH_TEST_RESULTS",
                        7 => "COMPLIANT_BRAND",
                        8 => "ACCOMPANYING_GOODS",
                        9 => ""
                    ],
                    "PRICE_CODE" => [
                        0 => "BASE"
                    ],
                    "USE_PRICE_COUNT" => 'N',
                    "SHOW_PRICE_COUNT" => 1,
                    "PRICE_VAT_INCLUDE" => 'N',
                    "PRICE_VAT_SHOW_VALUE" => 'N',
                    "USE_PRODUCT_QUANTITY" => 'N',
                    "ADD_PROPERTIES_TO_BASKET" => 'Y',
                    "PARTIAL_PRODUCT_PROPERTIES" => 'Y',
                    "PRODUCT_PROPERTIES" => [],
                    "CACHE_TYPE" => 'A',
                    "CACHE_TIME" => 36000000,
                    "CACHE_GROUPS" => 'N',
                    "OFFERS_CART_PROPERTIES" => [
                        0 => "COLOR_NAME"
                    ],
                    "OFFERS_FIELD_CODE" => [
                        0 => "NAME"
                    ],
                    "OFFERS_PROPERTY_CODE" => [
                        0 => "COLOR_NAME",
                        1 => "COLOR"
                    ],
                    "OFFERS_SORT_FIELD" => 'shows',
                    "OFFERS_SORT_ORDER" => 'desc',
//                "OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
//                "OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
                    "OFFERS_LIMIT" => 0,
                    'CONVERT_CURRENCY' => 'N',
//                'CURRENCY_ID' => $arParams['CURRENCY_ID'],
                    'HIDE_NOT_AVAILABLE' => 'N',
                    'VIEW_MODE' => '',
                    'ROTATE_TIMER' => '',
                    'TEMPLATE_THEME' => '',
//                'LABEL_PROP' => $arParams['LABEL_PROP'],
//                'ADD_PICT_PROP' => $arParams['ADD_PICT_PROP'],
//                'PRODUCT_DISPLAY_MODE' => $arParams['PRODUCT_DISPLAY_MODE'],

                    'OFFER_ADD_PICT_PROP' => $arParams['OFFER_ADD_PICT_PROP'],
                    'OFFER_TREE_PROPS' => $arParams['OFFER_TREE_PROPS'],
                    'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
                    'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
                    'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
                    'MESS_BTN_BUY' => $arParams['MESS_BTN_BUY'],
                    'MESS_BTN_ADD_TO_BASKET' => $arParams['MESS_BTN_ADD_TO_BASKET'],
                    'MESS_BTN_SUBSCRIBE' => $arParams['MESS_BTN_SUBSCRIBE'],
                    'MESS_BTN_DETAIL' => $arParams['MESS_BTN_DETAIL'],
                    'MESS_NOT_AVAILABLE' => $arParams['MESS_NOT_AVAILABLE'],
//                'ADD_TO_BASKET_ACTION' => $basketAction,
                    'SHOW_CLOSE_POPUP' => '',
//                'COMPARE_PATH' => $arResult['FOLDER'] . $arResult['URL_TEMPLATES']['compare']
                ),
                $component
            ); ?>

            <? if ($_REQUEST['AJAX'] == 'Y') die(); ?>

        </div>

    </div>
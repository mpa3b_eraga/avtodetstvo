<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Оплата");
$APPLICATION->SetAdditionalCSS('/pay/style.css');

?>


<div class="page-payment">
    <h3><u>Пластиковыми картами</u></h3>
    <ul>
        <li>Visa, Visa Electron&nbsp;</li>
        <li>MasterCard</li>
        <li>Maestro</li></ul>
    <h3><u>Электронными деньгами</u></h3>
    <p>Платежи обрабатываются в онлайне, дополнительная комиссия не взимается.</p>
    <ul>
        <li>Яндекс-деньги.</li>
        <li>WebMoney</li></ul>
    <h3><u>Банковскими переводами</u></h3>
    <p>Переводом с расчетного счета в любом банке.&nbsp;Сроки прохождения и идентификации платежей:&nbsp;1–2 рабочих дня по России, 5–6 рабочих дней по Украине.</p>
</div>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
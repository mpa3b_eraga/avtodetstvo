<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Гарантия");
$APPLICATION->SetAdditionalCSS('/warranty/style.css');

?>


<div class="page-warranty">
    <p>Мы уверены в качестве автокресел, которые предлагаем нашим покупателям. Именно поэтому мы предоставляем расширенную гарантию на все наши автокресла!</p>
    <table class="table table-striped">
        <thead>
        <tr>
            <th>Производитель кресел</th>
            <th>Гарантия производителя</th>
            <th>Срок гарантии</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>Actrum</td>
            <td>6 месяцев</td>
            <td>1 год</td>
        </tr>
        <tr>
            <td>Britax-Romer</td>
            <td>2 года</td>
            <td>4 года</td>
        </tr>
        <tr>
            <td>CasualPlay</td>
            <td>2 года</td>
            <td>3 года</td>
        </tr>
        <tr>
            <td>Concord</td>
            <td>2 года</td>
            <td>3 года</td>
        </tr>
        <tr>
            <td>Cybex</td>
            <td>3 года</td>
            <td>4 года</td>
        </tr>
        <tr>
            <td>Diono</td>
            <td>2 года</td>
            <td>3 года</td>
        </tr>
        <tr>
            <td>Inglesina</td>
            <td>1 год</td>
            <td>2 года</td>
        </tr>
        <tr>
            <td>Kenga</td>
            <td>6 месяцев</td>
            <td>1 год</td>
        </tr>
        <tr>
            <td>Kiddy&nbsp;</td>
            <td>3 года</td>
            <td>4 года</td>
        </tr>
        <tr>
            <td>Little King</td>
            <td>6 месяцев</td>
            <td>1 год</td>
        </tr>
        <tr>
            <td>Maxi-Cosi</td>
            <td>2 года</td>
            <td>3 года</td>
        </tr>
        <tr>
            <td>Nania</td>
            <td>6 месяцев</td>
            <td>1 год</td>
        </tr>
        <tr>
            <td>Peg-Perego</td>
            <td>1 год</td>
            <td>2 года</td>
        </tr>
        <tr>
            <td>Rant</td>
            <td>6 месяцев</td>
            <td>1 год</td>
        </tr>
        <tr>
            <td>Recaro</td>
            <td>3 года</td>
            <td>4 года</td>
        </tr>
        <tr>
            <td>Siger</td>
            <td>6 месяцев</td>
            <td>1 год</td>
        </tr>
        <tr>
            <td>Sparco</td>
            <td>6 месяцев</td>
            <td>1 год</td>
        </tr>
        <tr>
            <td>STM</td>
            <td>3 года</td>
            <td>4 года</td>
        </tr>
        <tr>
            <td>Zlatek</td>
            <td>6 месяцев</td>
            <td>1 год</td>
        </tr>
        <tr>
            <td>Еду-Еду</td>
            <td>6 месяцев</td>
            <td>1 год</td>
        </tr>
        <tr>
            <td>Мишутка</td>
            <td>6 месяцев</td>
            <td>1 год</td>
        </tr>
        <tr>
            <td>Смешарики</td>
            <td>6 месяцев</td>
            <td>1 год</td>
        </tr>
        </tbody>
    </table>
    <p></p>
    <p>В случае выявления производственного дефекта, мы обязуемся произвести ремонт автокресла или его замену (при невозможности устранения дефекта).</p>
    <p>Гарантийный срок исчисляется со дня покупки и не распространяется:</p>
    <ul>
        <li>на естественный износ;</li>
        <li>на повреждение ткани;</li>
        <li>на повреждения, вызванные неправильной эксплуатацией;</li>
        <li>на автокресла, с признаками постороннего вмешательства в конструкцию.</li></ul>
    <p><b>Внимание:</b> мы не производим ремонт автокресел, купленных не в нашем магазине.</p>
</div>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
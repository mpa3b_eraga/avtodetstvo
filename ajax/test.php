<?php
/**
 * Created by PhpStorm.
 * User: ilya_
 * Date: 26.03.2016
 * Time: 14:54
 */

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

switch ($_REQUEST['TYPE']) {

    case 'ADD_TO_FAVORITE':
        global $USER;
        $rsUser = CUser::GetByID($USER->GetID());
        $arUser = $rsUser->Fetch();
        $arUser['UF_FAVORITE'][] = $_REQUEST['ID'];
        $user = new CUser;
        $fields = Array(
            "UF_FAVORITE" => $arUser['UF_FAVORITE'],
        );

        if ($user->Update($USER->GetID(), $fields)) {
            echo 'SUCCESS';
        } else {
            $strError .= $user->LAST_ERROR;
            echo 'FAIL: ' . $strError;
        };

    case 'GEOIP':
        if (isset($_REQUEST['CITY_NAME']) && !empty($_REQUEST['CITY_NAME'])) {

            $url_arr = parse_url($_REQUEST['URL']);

            if (CModule::IncludeModule("sale")) {

                $db_vars = CSaleLocation::GetList(
                    array(
                        "SORT" => "ASC",
                        "COUNTRY_NAME_LANG" => "ASC",
                        "CITY_NAME_LANG" => "ASC"
                    ),
                    array("LID" => LANGUAGE_ID, "CITY_NAME" => $_REQUEST['CITY_NAME']),
                    false,
                    false,
                    array()
                );

                if ($db_vars->SelectedRowsCount() > 0) {
                    $vars = $db_vars->Fetch();
                    $_SESSION['SESS_CITY_NAME'] = $vars['CITY_NAME'];
                    $_SESSION['SESS_CITY_ID'] = $vars['ID'];
                    $_SESSION['CITY_DETECTED'] = 'Y';
                    $translit_city = $vars['CITY_NAME_ORIG'];
                } else {
                    $rb_vars = CSaleLocation::GetList(
                        array(
                            "SORT" => "ASC",
                            "COUNTRY_NAME_LANG" => "ASC",
                            "CITY_NAME_LANG" => "ASC"
                        ),
                        array("LID" => LANGUAGE_ID, "%REGION_NAME" => $_REQUEST['REGION_NAME']),
                        false,
                        false,
                        array()
                    );

                    if ($rb_vars->SelectedRowsCount() > 0) {
                        $rvars = $rb_vars->Fetch();
                        $_SESSION['SESS_CITY_NAME'] = $_REQUEST['CITY_NAME'];
                        $_SESSION['SESS_CITY_ID'] = $rvars['ID'];
                        $_SESSION['CITY_DETECTED'] = 'Y';
                        $translit_city = Cutil::translit($_REQUEST['CITY_NAME'], "ru");
                    }
                }
            }


            /*$db_vars = CSaleLocation::GetList(
                array(
                    "SORT" => "ASC",
                    "COUNTRY_NAME_LANG" => "ASC",
                    "CITY_NAME_LANG" => "ASC"
                ),
                array("LID" => LANGUAGE_ID, "%REGION_NAME"=>$_REQUEST['REGION_NAME']),
                false,
                false,
                array()
            );

            if ($db_vars->SelectedRowsCount() > 0) {
                $vars = $db_vars->Fetch();
                printer($vars);
                $translit_city = $vars['CITY_NAME_ORIG'];
            }
            else {
                $translit_city = transliterate($_REQUEST['CITY_NAME']);
            }*/

            $rsSites = CSite::GetByID(SITE_ID);
            $arSite = $rsSites->Fetch();

            $url = strtolower($translit_city) . '.' . $arSite['SERVER_NAME'] . $url_arr['path'];

            $user_agent = "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, true);
            curl_setopt($ch, CURLOPT_FAILONERROR, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_NOBODY, true);
            curl_setopt($ch, CURLOPT_TIMEOUT, 5);
            curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);
            $document = curl_exec($ch);
            curl_close($ch);

            if (empty($document)) {
                echo json_encode($arSite['SERVER_NAME'] . $url_arr['path']);
            } else {
                echo json_encode($url);
            }
        }
        break;

    case 'REGIONLIST':
        if (CModule::IncludeModule("sale")) {
            $db_vars = CSaleLocation::GetList(
                array(
                    "SORT" => "ASC",
                    "COUNTRY_NAME_LANG" => "ASC",
                    "CITY_NAME_LANG" => "ASC"
                ),
                array("LID" => LANGUAGE_ID, "%CITY_NAME" => $_REQUEST['CITY_QUERY']),
                false,
                false,
                array()
            );
            while ($vars = $db_vars->Fetch()) {
                $arCityNames[] = array('CITY_NAME' => $vars['CITY_NAME'], 'CITY_ID' => $vars['ID']);
            }
            echo json_encode($arCityNames);
        }
        break;

    case 'REGISTRATION':
        $APPLICATION->IncludeComponent("bitrix:system.auth.registration", "registration", Array(), false);
        break;

    case 'AUTH':
        $APPLICATION->IncludeComponent("bitrix:system.auth.authorize", "authorization", Array(), false);
        break;

    case 'SEND_PWD':
        $APPLICATION->IncludeComponent("bitrix:system.auth.forgotpasswd", "forgot_password", Array(), false);
        break;

    case 'CONFIRM_REG':
        $APPLICATION->IncludeComponent("bitrix:system.auth.confirmation", "confirmation", Array(), false);
        break;

    case 'CHANGE_PWD':
        $APPLICATION->IncludeComponent("bitrix:system.auth.changepasswd", "change_password", Array(), false);
        break;

    case 'RECALCULATE_CART':

        $_REQUEST['QUANTITY_' . $_REQUEST['ID']] = $_REQUEST['count'];

        $APPLICATION->IncludeComponent(
            "upcar:sale.basket.order.ajax",
            "cart",
            array(
                "PATH_TO_PERSONAL" => "/profile/orders/?show_all=Y",
                "PATH_TO_PAYMENT" => "/cart/",
                "SEND_NEW_USER_NOTIFY" => "Y",
                "COLUMNS_LIST" => array(
                    0 => "NAME",
                    1 => "QUANTITY",
                    2 => "DISCOUNT",
                    3 => "PRICE",
                    4 => "DELETE",
                ),
                "HIDE_COUPON" => "Y",
                "QUANTITY_FLOAT" => "N",
                "PRICE_VAT_SHOW_VALUE" => "N",
                "PRICE_TAX_SHOW_VALUE" => "N",
                "SHOW_BASKET_ORDER" => "Y",
                "TEMPLATE_LOCATION" => "popup",
                "SET_TITLE" => "N"
            ),
            false
        );
        break;

    case 'ADD_TO_CART':
        if (CModule::IncludeModule("catalog")) {
            if ($_REQUEST['id'] != 0 && IntVal($_REQUEST['id']) > 0) {
                foreach ($_REQUEST['id'] as $item_id) {
                    Add2BasketByProductID(
                        $item_id,
                        1,
                        array(),
                        array()
                    );
                }
            }
            $APPLICATION->IncludeComponent("upcar:sale.basket.basket.line", "header_cart_link", array(), false);
        }
        break;

    case 'GET_LOCATION_ID':
        if (CModule::IncludeModule("sale")) {

            $db_vars = CSaleLocation::GetList(
                array(
                    "SORT" => "ASC",
                    "COUNTRY_NAME_LANG" => "ASC",
                    "CITY_NAME_LANG" => "ASC"
                ),
                array("LID" => LANGUAGE_ID, "CITY_NAME" => $_REQUEST['CITY_NAME']),
                false,
                false,
                array()
            );

            if ($db_vars->SelectedRowsCount() > 0) {
                $vars = $db_vars->Fetch();
                echo json_encode($vars['ID']);
            } else {
                $rb_vars = CSaleLocation::GetList(
                    array(
                        "SORT" => "ASC",
                        "COUNTRY_NAME_LANG" => "ASC",
                        "CITY_NAME_LANG" => "ASC"
                    ),
                    array("LID" => LANGUAGE_ID, "%REGION_NAME" => $_REQUEST['REGION_NAME']),
                    false,
                    false,
                    array()
                );

                if ($rb_vars->SelectedRowsCount() > 0) {
                    $rvars = $rb_vars->Fetch();
                    echo json_encode($rvars['ID']);
                }
            }
        }
        break;

    case 'CALC_DELIVERY':
        CModule::IncludeModule("edost.delivery");

        if (isset($_REQUEST['cart_items']) && $_REQUEST['cart_items'] == 'Y') {

            $arID = array();

            $arBasketItems = array();

            $dbBasketItems = CSaleBasket::GetList(
                array(
                    "NAME" => "ASC",
                    "ID" => "ASC"
                ),
                array(
                    "FUSER_ID" => CSaleBasket::GetBasketUserID(),
                    "LID" => SITE_ID,
                    "ORDER_ID" => "NULL"
                ),
                false,
                false,
                array("ID", "CALLBACK_FUNC", "MODULE", "PRODUCT_ID", "QUANTITY", "PRODUCT_PROVIDER_CLASS")
            );
            while ($arItems = $dbBasketItems->Fetch()) {
                if ('' != $arItems['PRODUCT_PROVIDER_CLASS'] || '' != $arItems["CALLBACK_FUNC"]) {
                    CSaleBasket::UpdatePrice($arItems["ID"],
                        $arItems["CALLBACK_FUNC"],
                        $arItems["MODULE"],
                        $arItems["PRODUCT_ID"],
                        $arItems["QUANTITY"],
                        "N",
                        $arItems["PRODUCT_PROVIDER_CLASS"]
                    );
                    $arID[] = $arItems["ID"];
                }
            }
            if (!empty($arID)) {
                $dbBasketItems = CSaleBasket::GetList(
                    array(
                        "NAME" => "ASC",
                        "ID" => "ASC"
                    ),
                    array(
                        "ID" => $arID,
                        "ORDER_ID" => "NULL"
                    ),
                    false,
                    false,
                    array("ID", "CALLBACK_FUNC", "MODULE", "PRODUCT_ID", "QUANTITY", "DELAY", "CAN_BUY", "PRICE", "HEIGHT", "WEIGHT", "PRODUCT_PROVIDER_CLASS", "NAME")
                );
                while ($arItems = $dbBasketItems->Fetch()) {
                    $arBasketItems[] = $arItems;
                }
            }
            foreach ($arBasketItems as $BasketItem) {
                if ($BasketItem['CAN_BUY'] == 'Y') {
                    $res = $ar_res = CCatalogProduct::GetByID($BasketItem['PRODUCT_ID']);
                    $ItemSum += $BasketItem["PRICE"] * $BasketItem["QUANTITY"];
                    $ItemWeight += $BasketItem["WEIGHT"] * $BasketItem["QUANTITY"];
                    $ItemHeight += $ar_res['HEIGHT'] * $BasketItem["QUANTITY"];
                    $ItemWidth += $ar_res['WIDTH'] * $BasketItem["QUANTITY"];
                    $ItemLength += $ar_res['LENGTH'] * $BasketItem["QUANTITY"];
                }
            }

            $city = $_REQUEST['to_city'];
            $weight = ($ItemWeight / 1000) + $_REQUEST['weight'];
            $strah = $ItemSum + $_REQUEST['strah'];
            $width = $ItemWidth + $_REQUEST['width'];
            $height = $ItemHeight + $_REQUEST['height'];
            $length = $ItemLength + $_REQUEST['length'];

        } else {
            $city = $_REQUEST['to_city'];
            $weight = $_REQUEST['weight'];
            $strah = $_REQUEST['strah'];
            $width = $_REQUEST['width'];
            $height = $_REQUEST['height'];
            $length = $_REQUEST['length'];
        }

        $parse_url = parse_url("http://www.edost.ru/api2.php");
        $path = $parse_url["path"];

        $host = $parse_url["host"];
        $rez = "";

        $size = array(
            isset($length) && $length > 0 ? $length : 0,
            isset($width) && $width > 0 ? $width : 0,
            isset($height) && $height > 0 ? $height : 0
        );

        $fp = fsockopen($host, 80, $errno, $errstr, 4);

        if (strlen($_REQUEST['region']) >= 13) {
            $region = substr($_REQUEST['region'], 0, 2);
        } else {
            $region_arr = CExtendDelivery::GetEdostLocation($_REQUEST['region']);
            $region = $region_arr['region'];
        }

        $ar[] = 'id=5632';
        $ar[] = 'p=T59083vy0qY7f5oiQSeMf3UBHaDDLBw9';
        $ar[] = 'country=0';
        $ar[] = 'region=' . $region;
        $ar[] = 'city=' . urlencode($GLOBALS['APPLICATION']->ConvertCharset($city, LANG_CHARSET, 'windows-1251'));
        $ar[] = 'weight=' . urlencode($weight);
        $ar[] = 'insurance=' . urlencode($strah);
        $ar[] = 'size=' . urlencode(implode('|', $size));

        $post = implode('&', $ar);

        //$post = "to_city=".$city."&strah=".$strah."&weight=".$weight."&id=5632&p=T59083vy0qY7f5oiQSeMf3UBHaDDLBw9&ln=".$length."&wd=".$width."&hg=".$height."&zip=''&from_city=''\n\n";

        if ($errno == 13) return ('Err14');

        if ($fp) {
            $out = "POST " . $path . " HTTP/1.0\n" .
                "Host: " . $host . "\n" .
                "Referer: http://www.edost.ru/api2.php\n" .
                "Content-Type: application/x-www-form-urlencoded\n" .
                "Content-Length: " . strlen($post) . "\n\n" .
                $post . "\n\n";

            fputs($fp, $out);

            $q = 0;
            while ($gets = fgets($fp, 512)) {
                $rez .= $gets;
                $q++;
            }
            fclose($fp);
        }
        $rez = stristr($rez, 'api_data:', false);
        $data = substr($rez, 9);

        $r = CExtendEdost_Class::ParseData($data);

        echo "<table>";
        foreach ($r['data'] as $tarif) {
            echo '<tr class="delivery_item"><td class="image"><img src="/bitrix/images/delivery_edost_img/' . $tarif['id'] . '.gif" alt=""/></td><td class="name">' . $tarif['company'] . '</td><td class="time">' . $tarif['day'] . '</td><td class="price">' . $tarif['price'] . ' руб.</td></tr>';
        }
        echo "</table>";

        break;

    case 'ONE_CLICK_BY':

        //header('Content-type: application/json; charset=utf-8');

        /*if (!function_exists('inputClean'))
        {
            function inputClean($input, $sql=false)
            {
                $input = htmlentities($input, ENT_QUOTES, LANG_CHARSET);
                if(get_magic_quotes_gpc ())	{$input = stripslashes ($input);}
                if ($sql){$input = mysql_real_escape_string ($input);}
                $input = strip_tags($input);
                $input=str_replace ("\n"," ", $input);
                $input=str_replace ("\r","", $input);
                return $input;
            }
        }

        if (!function_exists('json_encode'))
        {
            function json_encode($value)
            {
                if (is_int($value)) { return (string)$value; }
                elseif (is_string($value))
                {
                    $value = str_replace(array('\\', '/', '"', "\r", "\n", "\b", "\f", "\t"),  array('\\\\', '\/', '\"', '\r', '\n', '\b', '\f', '\t'), $value);
                    $convmap = array(0x80, 0xFFFF, 0, 0xFFFF);
                    $result = "";
                    for ($i = mb_strlen($value) - 1; $i >= 0; $i--)
                    {
                        $mb_char = mb_substr($value, $i, 1);
                        if (mb_ereg("&#(\\d+);", mb_encode_numericentity($mb_char, $convmap, "UTF-8"), $match)) { $result = sprintf("\\u%04x", $match[1]) . $result;  }
                        else { $result = $mb_char . $result;  }
                    }
                    return '"' . $result . '"';
                }
                elseif (is_float($value)) { return str_replace(",", ".", $value); }
                elseif (is_null($value)) {  return 'null';}
                elseif (is_bool($value)) { return $value ? 'true' : 'false';   }
                elseif (is_array($value))
                {
                    $with_keys = false;
                    $n = count($value);
                    for ($i = 0, reset($value); $i < $n; $i++, next($value))  { if (key($value) !== $i) {  $with_keys = true; break;  }  }
                }
                elseif (is_object($value)) { $with_keys = true; }
                else { return ''; }
                $result = array();
                if ($with_keys)  {  foreach ($value as $key => $v) {  $result[] = json_encode((string)$key) . ':' . json_encode($v); }  return '{' . implode(',', $result) . '}'; }
                else {  foreach ($value as $key => $v) { $result[] = json_encode($v); } return '[' . implode(',', $result) . ']';  }
            }
        }*/

        function getJson($message, $res = 'N', $error = '')
        {
            global $APPLICATION;
            $result = array(
                'result' => $res == 'Y' ? 'Y' : 'N',
                'message' => $APPLICATION->ConvertCharset($message, SITE_CHARSET, 'utf-8')
            );
            if (strlen($error) > 0) {
                $result['err'] = $APPLICATION->ConvertCharset($error, SITE_CHARSET, 'utf-8');
            }
            return json_encode($result);
        }

        if (CModule::IncludeModule('sale') && CModule::IncludeModule('iblock') && CModule::IncludeModule('catalog') && CModule::IncludeModule('currency')) {

            global $APPLICATION;

            $user_registered = false;
            $currency = CCurrencyLang::GetByID($_POST['CURRENCY'], LANGUAGE_ID);

            $_POST['ONE_CLICK_BUY']['FIO'] = $APPLICATION->ConvertCharset($_POST['ONE_CLICK_BUY']['FIO'], 'utf-8', SITE_CHARSET);

            if (!empty($_POST['ONE_CLICK_BUY']['EMAIL']) && !check_email($_POST['ONE_CLICK_BUY']['EMAIL'])) {
                die(getJson('Неверный формат E-Mail'));
            } elseif (empty($_POST['ONE_CLICK_BUY']['PHONE'])) {
                die(getJson('Не указан номер телефонa'));
            } elseif (empty($_POST['ONE_CLICK_BUY']['FIO'])) {
                die(getJson("Не указано имя"));
            } elseif (!$currency) {

                $_POST['CURRENCY'] = COption::GetOptionString('sale', 'default_currency', 'RUB');

                $currency = CCurrencyLang::GetByID($_POST['CURRENCY'], LANGUAGE_ID);

                if (!$currency) {
                    die(getJson(GetMessage('Валюта не найдена')));
                }

            }

            global $USER;

            if (!$USER->IsAuthorized()) {
                // если нет email

                if (!isset($_POST['ONE_CLICK_BUY']['EMAIL']) || trim($_POST['ONE_CLICK_BUY']['EMAIL']) == '') {

                    $login = 'user_' . (microtime(true) * 10000);

                    if (strlen(SITE_SERVER_NAME)) {
                        $server_name = SITE_SERVER_NAME;
                    } else {
                        $server_name = $_SERVER["SERVER_NAME"];
                    }

                    $server_name = Cutil::translit($server_name, "ru");

                    if ($dotPos = strrpos($server_name, "_")) {
                        $server_name = substr($server_name, 0, $dotPos) . str_replace("_", ".", substr($server_name, $dotPos));
                    } else {
                        $server_name .= ".ru";
                    }

                    $_POST['ONE_CLICK_BUY']['EMAIL'] = $login . '@' . $server_name;

                    $user_registered = true;

                } else {
                    $dbUser = CUser::GetList(($by = 'ID'), ($order = 'ASC'), array('=EMAIL' => trim($_POST['ONE_CLICK_BUY']['EMAIL'])));

                    if ($dbUser->SelectedRowsCount() == 0) {
                        $login = 'user_' . (microtime(true) * 10000);
                        $user_registered = true;
                    } elseif ($dbUser->SelectedRowsCount() == 1) {
                        $ar_user = $dbUser->Fetch();
                        $registeredUserID = $ar_user['ID'];
                    } else {
                        die(getJson('Найдено более 1 пользователя с указанным email.'));
                    }
                }

                if ($user_registered) {
                    $captcha = COption::GetOptionString('main', 'captcha_registration', 'N');

                    if ($captcha == 'Y') {
                        COption::SetOptionString('main', 'captcha_registration', 'N');
                    }

                    $userPassword = randString(10);

                    $username = explode(' ', trim($_POST['ONE_CLICK_BUY']['FIO']));

                    $newUser = $USER->Register(
                        $login,
                        $username[0],
                        $username[1],
                        $userPassword,
                        $userPassword,
                        $_POST['ONE_CLICK_BUY']['EMAIL']
                    );

                    /* added by mpa3b */

                    if (!empty($_POST['ONE_CLICK_BUY']['PHONE'])) {

                        $rsUser = CUser::GetByLogin($login);
                        $arUser = $rsUser->Fetch();

                        $registeredUserID = $arUser["ID"];

                        $arrFIO = explode(' ', $_POST['ONE_CLICK_BUY']['FIO']);

                        if (count($arrFIO) == 3) {
                            $USER->Update(
                                $registeredUserID,
                                array(
                                    'NAME' => $arrFIO[0],
                                    'SECOND_NAME' => $arrFIO[1],
                                    'LAST_NAME' => $arrFIO[2]
                                )
                            );
                        } elseif (count($arrFIO == 2)) {
                            $USER->Update(
                                $registeredUserID,
                                array(
                                    'NAME' => $arrFIO[0],
                                    'LAST_NAME' => $arrFIO[1]
                                )
                            );
                        } elseif (count($arrFIO == 1)) {
                            $USER->Update(
                                $registeredUserID,
                                array(
                                    'NAME' => $arrFIO[0]
                                )
                            );
                        }

                        $USER->Update(
                            $registeredUserID,
                            array(
                                'PERSONAL_PHONE' => $_POST['ONE_CLICK_BUY']['PHONE']
                            )
                        );

                    }

                    /* end */

                    if ($captcha == 'Y') {
                        COption::SetOptionString('main', 'captcha_registration', 'Y');
                    }

                    if ($newUser['TYPE'] == 'ERROR') {
                        die(getJson('Ошибка регистрации пользователя.', 'N', $newUser['MESSAGE']));
                    } else {
                        $rsUser = CUser::GetByLogin($login);
                        $arUser = $rsUser->Fetch();

                        $registeredUserID = $arUser["ID"];

                        if (!empty($_POST['ONE_CLICK_BUY']['PHONE'])) {
                            $USER->Update(
                                $registeredUserID,
                                array('PERSONAL_PHONE' => $_POST['ONE_CLICK_BUY']['PHONE'])
                            );
                        }
                    }
                }
            } else {

                $registeredUserID = $USER->GetID();

            }

            $newOrder = array(
                'LID' => SITE_ID,
                'PERSON_TYPE_ID' => 1,
                'PHONE' => $_POST['ONE_CLICK_BUY'],
                'PAYED' => 'N',
                'CURRENCY' => $_POST['CURRENCY'],
                'USER_ID' => $registeredUserID,
                'PAY_SYSTEM_ID' => 0,
                'DELIVERY_ID' => 0
            );

            $orderID = CSaleOrder::Add($newOrder);

            if ($orderID == false) {

                $strError = '';

                if ($ex = $APPLICATION->GetException()) $strError = $ex->GetString();

                die(getJson('Ошибка создания заказа.', 'N', $strError));

            }

            /* mpa3b, order properties */

            CSaleOrderPropsValue::Add(
                array(
                    'ORDER_ID' => $orderID,
                    'ORDER_PROPS_ID' => 5,
                    'NAME' => 'Контактное лицо',
                    'VALUE' => $_POST['ONE_CLICK_BUY']['FIO']
                )
            );

            CSaleOrderPropsValue::Add(
                array(
                    'ORDER_ID' => $orderID,
                    'ORDER_PROPS_ID' => 6,
                    'NAME' => 'Номер телефона',
                    'VALUE' => $_POST['ONE_CLICK_BUY']['PHONE']
                )
            );

            /* end */

            $res = CSaleBasket::GetList(

                array(
                    'SORT' => 'DESC'
                ),
                array(
                    'FUSER_ID' => CSaleBasket::GetBasketUserID(),
                    'LID' => SITE_ID,
                    'ORDER_ID' => 'NULL',
                    'DELAY' => 'N'
                )

            );

            $orderPrice = 0;
            $orderList = '';

            $basketUserID = CSaleBasket::GetBasketUserID();

            $db_basket_items = CSaleBasket::GetList(

                array(
                    'SORT' => 'DESC'
                ),
                array(
                    'FUSER_ID' => $basketUserID,
                    'LID' => SITE_ID,
                    'ORDER_ID' => 'NULL',
                    'DELAY' => 'N'
                )

            );

            $addProduct = true;

            if ($_POST['BUY_TYPE'] == 'ALL') {
                while ($ar_tmp = $db_basket_items->Fetch()) {
                    if ($ar_tmp['CAN_BUY'] == 'Y') {
                        if ($_POST['CURRENCY']) {
                            if ($ar_tmp['CURRENCY'] != $_POST['CURRENCY']) {
                                $ar_tmp['PRICE'] = CCurrencyRates::ConvertCurrency($ar_tmp['PRICE'], $ar_tmp['CURRENCY'], $_POST['CURRENCY']);
                            }
                        }

                        CSaleBasket::Update(
                            $ar_tmp['ID'],
                            array(
                                'ORDER_ID' => $orderID,
                                'PRICE' => $ar_tmp['PRICE'],
                                'QUANTITY' => $ar_tmp['QUANTITY'],
                                'FUSER_ID' => $registeredUserID)
                        );

                        $curPrice = roundEx($ar_tmp['PRICE'], SALE_VALUE_PRECISION) * DoubleVal($ar_tmp['QUANTITY']);
                        $orderPrice += $curPrice;
                        $orderList .= 'Название: ' . $ar_tmp['NAME'] . ' Цена: ' . str_replace('#', $ar_tmp['PRICE'], $currency['FORMAT_STRING']) . ' Количество: ' . intval($ar_tmp['QUANTITY']) . ' Итого: ' . str_replace('#', $curPrice, $currency['FORMAT_STRING']) . "\n";
                    }
                }
            } else {
                $db_product = CIBlockElement::GetByID($_POST['ELEMENT_ID']);
                $arProduct = $db_product->GetNext();
                $product_quantity = 1;
                $arProps = array();
                $product_desc_string = $arProduct['DETAIL_TEXT'];

                $added = Add2BasketByProductID(
                    $_POST['ELEMENT_ID'],
                    $product_quantity,
                    array(
                        'ORDER_ID' => $orderID
                    ),
                    $arProps
                );

                if (!$added) {
                    $strError = '';
                    if ($ex = $APPLICATION->GetException()) {
                        $strError = $ex->GetString();
                    }
                    die(getJson('Ошибка добавления товара в заказ.', 'N', $strError));
                } else {
                    $ar_basket_item = CSaleBasket::GetByID($added);

                    if ($_POST['CURRENCY']) {
                        if ($ar_basket_item['CURRENCY'] != $_POST['CURRENCY']) {
                            $ar_basket_item['PRICE'] = CCurrencyRates::ConvertCurrency($ar_basket_item['PRICE'], $ar_basket_item['CURRENCY'], $_POST['CURRENCY']);
                        }
                    }

                    $orderPrice += roundEx($ar_basket_item['PRICE'], SALE_VALUE_PRECISION) * DoubleVal($ar_basket_item['QUANTITY']);
                    $orderList .= " Название товара: " . $arProduct['NAME'] . $product_desc_string . " Цена: " . str_replace('#', $ar_basket_item['PRICE'], $currency['FORMAT_STRING']) . " Количество: " . intval($ar_basket_item['QUANTITY']) . " Итого: " . str_replace('#', (roundEx($ar_basket_item['PRICE'], SALE_VALUE_PRECISION) * DoubleVal($ar_basket_item['QUANTITY'])), $currency['FORMAT_STRING']) . "\n";
                }

            }

            $_SESSION['SALE_BASKET_NUM_PRODUCTS'][SITE_ID] = 0;

            CSaleOrder::Update(
                $orderID,
                array(
                    'PRICE' => $orderPrice
                )
            );

            $arMessageFields = array(
                "RS_ORDER_ID" => $orderID,
                "CLIENT_NAME" => $_POST['ONE_CLICK_BUY']['FIO'],
                "PHONE" => $_POST["ONE_CLICK_BUY"]["PHONE"],
                "ORDER_ITEMS" => $orderList,
                "ORDER_PRICE" => str_replace('#', $orderPrice, $currency['FORMAT_STRING']),
                "RS_DATE_CREATE" => ConvertTimeStamp(false, "FULL"));

            CEvent::Send("NEW_ONE_CLICK_BUY", SITE_ID, $arMessageFields);

            die(getJson('Заказ №' . $orderID . ' успешно создан. Наш менеджер свяжется с вами в ближайшее время', 'Y'));
        }

        die(getJson(GetMessage('Недостаточно данных')));

        break;

    case 'BUY_CREDIT':
        if (CModule::IncludeModule("catalog")) {
            if ($_REQUEST['id'] !== 0 && IntVal($_REQUEST['id']) > 0) {
                foreach ($_REQUEST['id'] as $item_id) {
                    Add2BasketByProductID(
                        $item_id,
                        1,
                        array(),
                        array()
                    );
                }
            }
            $APPLICATION->IncludeComponent("upcar:sale.basket.basket.line", "header_cart_link", array(), false);
        }
        break;

    case 'BUY_SET' :

        if (CModule::IncludeModule("sale")) {

            if ($_REQUEST['id'] !== 0 && IntVal($_REQUEST['id']) > 0) {

                foreach ($_REQUEST['id'] as $item_id) {
                    Add2BasketByProductID(
                        $item_id,
                        1,
                        array(),
                        array()
                    );
                }

            }
            $APPLICATION->IncludeComponent("upcar:sale.basket.basket.line", "header_cart_link", array(), false);

        }

        break;

};



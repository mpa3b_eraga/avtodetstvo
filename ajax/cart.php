<?php

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

if (!empty($_POST)) {
    
    // Время последнего обновления корзины (для избежания рассинхрона в обновлении данных корзины)
    
    if (!isset($_SESSION['cart_last_update'])) {
        $_SESSION['cart_last_update'] = 0;
    }
    
    // Ответ
    $output = [
        'status' => 'fail'
    ];
    
    // Проверка времени запроса
    
    if (isset($_POST['time']) && $_POST['time'] > $_SESSION['cart_last_update']) {
        
        if (CModule::IncludeModule('sale')) {
            
            // Действия с корзиной
            
            switch ($_POST['action']) {
                
                case 'add':
                    
                    Add2BasketByProductID($_POST['id'], 1); // ID товарного предложения!!!
                    
                    $output['status'] = 'success';
                    
                    break;
                
                case 'update':
                    
                    CSaleBasket::Update(
                        $_POST['id'],  // ID элемента корзины
                        ['QUANTITY' => $_POST['quantity']]
                    );
                    
                    $output['status'] = 'success';
                    
                    break;
                
                case 'delete':
                    
                    CSaleBasket::Delete($_POST['id']); // ID элемента корзины
                    
                    $output['status'] = 'success';
                    
                    break;
                
            }
        }
    }
    // При успешном запросе
    
    if ($output['status'] == 'success') {
        
        // Данные корзины
        
        $rsCart = CSaleBasket::GetList(
            [],
            [
                'FUSER_ID' => CSaleBasket::GetBasketUserID(),
                'ORDER_ID' => null
            ]
        );
        
        $cart = [];
        $count = 0;
        $total = 0;
        
        while ($cartItem = $rsCart->GetNext()) {
            
            $cart[$cartItem['ID']] = $cartItem['QUANTITY'];
            
            $count = $count + $cartItem['QUANTITY'];
            
            $price = $cartItem['PRICE'] * $cartItem['QUANTITY'];
            
            $total = $total + $price;
        }
        
        $output['cart'] = $cart;
        $output['count'] = $count;
        $output['total'] = number_format($total, 0, ',', ' ');
        
        // Суффикс количества товаров для small-cart
        
        $suffix = 'товаров';
        
        if (substr($count, strlen($count) - 2, 1) != '1' || $count < 10) {
            
            switch (substr($count, strlen($count) - 1, 1)) {
        
                case '1':
                    $suffix = 'товар';
                    break;
                
                case '2':
                
                case '3':
                
                case '4':
                    
                    $suffix = 'товара';
                    break;
                
            }
        }
        
        $output['suffix'] = $suffix;
        
    }
    
    $_SESSION['cart_last_update'] = $_POST['time'];
    
}

echo json_encode($output, JSON_UNESCAPED_UNICODE);
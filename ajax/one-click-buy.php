<?php

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

if (!empty($_POST)) :

    if (empty($_POST['name']) || empty($_POST['phone'])) :

        $arReturn = array(
            'type' => 'error',
            'message' => 'Укажите свой телефон и имя для заказа.',
            'title' => 'Ошибка оформления заказа'
        );

    else :

        if (CModule::IncludeModule('sale') && CModule::IncludeModule('iblock') && CModule::IncludeModule('catalog') && CModule::IncludeModule('currency')) :

            $rsProduct = CIBlockElement::GetByID($_POST['id']);
            $arProduct = $rsProduct->Fetch();
            $arPrice = CPrice::GetBasePrice($arProduct['ID']);
            $arDiscount = CCatalogProduct::GetOptimalPrice($arProduct["ID"], 1);

            // проверить авторизацию

            global $USER;

            if ($USER->IsAuthorized()) {

                $userID = $USER->GetID();

            } else {

                // создать учётку

                $user = new CUser;

                $arrName = explode(' ', $_POST['name']);

                $name = $arrName[0];
                $surname = $arrName[1];

                $password = randString(8);

                $customerPhone = $_POST['phone'];
                $customerLogin = str_replace(' ', '-', CUtil::translit(strtolower($_POST['name']), 'ru'));

                $customerEmail = $customerLogin . '@' . $_SERVER['HTTP_HOST'];

                // проверить логин на уникальность
                $rsUsers = $user->GetList(
                    $by = 'id',
                    $order = 'desc',
                    array(
                        'LOGIN' => $customerLogin
                    ),
                    array()
                );

                $arUser = $rsUsers->Fetch();

                // логин существует

                if ($arUser) {
                    $userID = $arUser['ID'];
                } else {

                    $userID = $USER->Add(
                        array(
                            'LOGIN' => $customerLogin,
                            'NAME' => $name,
                            'LAST_NAME' => $surname,
                            'EMAIL' => $customerEmail,
                            'PASSWORD' => $password,
                            'CONFIRM_PASSWORD' => $password,
                            'ACTIVE ' => 'Y',
                            'PERSONAL_PHONE' => $customerPhone,
                        )
                    );
                }
            }

            // создать заказ

            $arFields = array(
                "LID" => SITE_ID,
                "PERSON_TYPE_ID" => 1,
                "PAYED" => "N",
                "CANCELED" => "N",
                "STATUS_ID" => "N",
                'PRICE' => $arPrice["PRICE"],
                'CURRENCY' => $arPrice["CURRENCY"],
                'USER_ID' => $userID,
                "USER_DESCRIPTION" => ""
            );

            $orderID = CSaleOrder::Add($arFields);

            //создать корзину

            $arFields = array(
                "PRODUCT_ID" => $arProduct["ID"],
                "PRODUCT_PRICE_ID" => 0,
                "PRICE" => $arDiscount['DISCOUNT_PRICE'],
                "CURRENCY" => $arPrice["CURRENCY"],
                "WEIGHT" => 0,
                "QUANTITY" => 1,
                "LID" => SITE_ID,
                "DELAY" => "N",
                "CAN_BUY" => "Y",
                "NAME" => $arProduct["NAME"],
                "ORDER_ID" => $orderID,
                "DETAIL_PAGE_URL" => $arProduct["DETAIL_PAGE_URL"],
                "DISCOUNT_PRICE" => $arDiscount["RESULT_PRICE"]["DISCOUNT"],
                "DISCOUNT_VALUE" => $arDiscount["DISCOUNT"]["VALUE"]
            );

            $basket = CSaleBasket::Add($arFields);

            $arReturn = array(
                'type' => 'success',
                'message' => 'Заказ №' . $orderID . ' оформлен.',
                'title' => 'Заказ создан!'
            );

        endif;
    endif;

else :

    $arReturn = array(
        'type' => 'fail',
        'message' => 'Ошибка!',
        'title' => 'Что-то пошло не так...'
    );

endif;

echo json_encode($arReturn, JSON_UNESCAPED_UNICODE);

?>
<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Лучшие предложения");
?>

<?

global $bestOffersFilter, $subdomain;

$bestOffersFilter = array(
    '!PROPERTY_CML2_LINK' => false,
    '!PROPERTY_BEST_OFFER' => false,
    '!DETAIL_PICTURE' => false,
    'ACTIVE' => 'Y'
);
?>

<? $APPLICATION->IncludeComponent(
    "bitrix:catalog.top",
    "best-offers",
    array(
        "ACTION_VARIABLE" => "action",
        "ADD_PICT_PROP" => "-",
        "ADD_PROPERTIES_TO_BASKET" => "N",
        "ADD_TO_BASKET_ACTION" => "ADD",
        "BASKET_URL" => "/cart/",
        "CACHE_FILTER" => "N",
        "CACHE_GROUPS" => "Y",
        "CACHE_TIME" => "36000000",
        "CACHE_TYPE" => "A",
        "COMPONENT_TEMPLATE" => "best-offers",
        "CONVERT_CURRENCY" => "N",
        "DETAIL_URL" => "",
        "DISPLAY_COMPARE" => "N",
        "ELEMENT_COUNT" => "6",
        "ELEMENT_SORT_FIELD" => "shows",
        "ELEMENT_SORT_FIELD2" => "",
        "ELEMENT_SORT_ORDER" => "desc",
        "ELEMENT_SORT_ORDER2" => "",
        "FILTER_NAME" => "bestOffersFilter",
        "HIDE_NOT_AVAILABLE" => "N",
        "IBLOCK_ID" => "18",
        "IBLOCK_TYPE" => "catalog",
        "LABEL_PROP" => "-",
        "LINE_ELEMENT_COUNT" => "3",
        "MESS_BTN_ADD_TO_BASKET" => "В корзину",
        "MESS_BTN_BUY" => "Купить",
        "MESS_BTN_COMPARE" => "Сравнить",
        "MESS_BTN_DETAIL" => "Подробнее",
        "MESS_NOT_AVAILABLE" => "Нет в наличии",
        "OFFERS_LIMIT" => "0",
        "PARTIAL_PRODUCT_PROPERTIES" => "N",
        "PRICE_CODE" => array(
            0 => "BASE",
        ),
        "PRICE_VAT_INCLUDE" => "N",
        "PRODUCT_ID_VARIABLE" => "id",
        "PRODUCT_PROPERTIES" => array(),
        "PRODUCT_PROPS_VARIABLE" => "prop",
        "PRODUCT_QUANTITY_VARIABLE" => "",
        "PROPERTY_CODE" => array(
            0 => "CML2_LINK",
            1 => "COLOR_NAME",
            2 => "COLOR",
            3 => "",
        ),
        "SECTION_ID_VARIABLE" => "",
        "SECTION_URL" => "",
        "SEF_MODE" => "N",
        "SHOW_CLOSE_POPUP" => "N",
        "SHOW_DISCOUNT_PERCENT" => "N",
        "SHOW_OLD_PRICE" => "N",
        "SHOW_PRICE_COUNT" => "1",
        "TITLE" => "Лучшие предложения",
        "USE_PRICE_COUNT" => "N",
        "USE_PRODUCT_QUANTITY" => "N",
        "VIEW_MODE" => "SECTION",
        "TITLE_AS_LINK" => "N",
        "SEF_RULE" => "",
        "SUBDOMAIN" => getSubDomain()
    ),
    false
); ?>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>
<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<? if (!$arResult["NavShowAlways"]) {
    if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false)) return;
} ?>

<div class="pager grid">
    <nav class="whole unit align-center">
    
        <?
        $strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"] . "&amp;" : "");
        $strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?" . $arResult["NavQueryString"] : "");
        ?>
    
        <? /* <span class="title"><?= GetMessage("pages") ?></span> */ ?>
    
        <? if ($arResult["bDescPageNumbering"] === true):
            $bFirst = true;
            if ($arResult["NavPageNomer"] < $arResult["NavPageCount"]):
                if ($arResult["bSavePage"]): ?>
                    <a class="previous pager-link"
                       href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] + 1) ?>"><?= GetMessage("nav_prev") ?></a>
                <? else:
                    if ($arResult["NavPageCount"] == ($arResult["NavPageNomer"] + 1)): ?>
                        <a class="previous pager-link"
                           href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>"><?= GetMessage("nav_prev") ?></a>
                    <? else: ?>
                        <a class="previous pager-link"
                           href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] + 1) ?>"><?= GetMessage("nav_prev") ?></a>
                    <? endif;
                endif;
    
                if ($arResult["nStartPage"] < $arResult["NavPageCount"]):
                    $bFirst = false;
                    if ($arResult["bSavePage"]): ?>
                        <a class="first pager-link"
                           href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= $arResult["NavPageCount"] ?>">1</a>
                    <? else: ?>
                        <a class="first pager-link"
                           href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>">1</a>
                    <? endif;
                    if ($arResult["nStartPage"] < ($arResult["NavPageCount"] - 1)): ?>
                        <a class="dots pager-link"
                           href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= intVal($arResult["nStartPage"] + ($arResult["NavPageCount"] - $arResult["nStartPage"]) / 2) ?>">...</a>
                    <? endif;
                endif;
            endif;
        
            do {
                $NavRecordGroupPrint = $arResult["NavPageCount"] - $arResult["nStartPage"] + 1;
            
                if ($arResult["nStartPage"] == $arResult["NavPageNomer"]): ?>
                    <span class="<?= ($bFirst ? "first " : "") ?>current pager-link"><?= $NavRecordGroupPrint ?></span>
                <? elseif ($arResult["nStartPage"] == $arResult["NavPageCount"] && $arResult["bSavePage"] == false): ?>
                    <a href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>"
                       class="pager-link<?= ($bFirst ? " first" : "") ?>"><?= $NavRecordGroupPrint ?></a>
                <? else: ?>
                    <a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= $arResult["nStartPage"] ?>"
                       class="pager-link<?= ($bFirst ? " first" : "") ?>"><?= $NavRecordGroupPrint ?></a>
                <? endif;
            
                $arResult["nStartPage"]--;
                $bFirst = false;
            
            } while ($arResult["nStartPage"] >= $arResult["nEndPage"]);
        
            if ($arResult["NavPageNomer"] > 1):
                if ($arResult["nEndPage"] > 1):
                    if ($arResult["nEndPage"] > 2): ?>
                        <a class="dots pager-link"
                           href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= round($arResult["nEndPage"] / 2) ?>">...</a>
                    <? endif; ?>
                    <a class="pager-link"
                       href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=1"><?= $arResult["NavPageCount"] ?></a>
                <? endif; ?>
                <a class="next pager-link"
                   href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] - 1) ?>"><?= GetMessage("nav_next") ?></a>
            <? endif;
    
        else:
            $bFirst = true;
        
            if ($arResult["NavPageNomer"] > 1):
                if ($arResult["bSavePage"]): ?>
                    <a class="previous pager-link"
                       href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] - 1) ?>"><?= GetMessage("nav_prev") ?></a>
                    <?
                else:
                    if ($arResult["NavPageNomer"] > 2): ?>
                        <a class="previous pager-link"
                           href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] - 1) ?>"><?= GetMessage("nav_prev") ?></a>
                    <? else: ?>
                        <a class="previous pager-link"
                           href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>"><?= GetMessage("nav_prev") ?></a>
                    <? endif;
                endif;
            
                if ($arResult["nStartPage"] > 1):
                    $bFirst = false;
                    if ($arResult["bSavePage"]): ?>
                        <a class="first pager-link"
                           href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=1">1</a>
                    <? else: ?>
                        <a class="first pager-link"
                           href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>">1</a>
                    <? endif;
                    if ($arResult["nStartPage"] > 2): ?>
                        <a class="dots pager-link"
                           href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= round($arResult["nStartPage"] / 2) ?>">...</a>
                    <? endif;
                endif;
            endif;
        
            do {
                if ($arResult["nStartPage"] == $arResult["NavPageNomer"]): ?>
                    <span
                        class="<?= ($bFirst ? "first " : "") ?>current disabled pager-link"><?= $arResult["nStartPage"] ?></span>
                <? elseif ($arResult["nStartPage"] == 1 && $arResult["bSavePage"] == false): ?>
                    <a href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>"
                       class="<?= ($bFirst ? "first " : "") ?>pager-link"><?= $arResult["nStartPage"] ?></a>
                <? else: ?>
                    <a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= $arResult["nStartPage"] ?>"
                       class="<?= ($bFirst ? "first " : "") ?>pager-link"><?= $arResult["nStartPage"] ?></a>
                <? endif;
                $arResult["nStartPage"]++;
                $bFirst = false;
            } while ($arResult["nStartPage"] <= $arResult["nEndPage"]);
        
            if ($arResult["NavPageNomer"] < $arResult["NavPageCount"]):
                if ($arResult["nEndPage"] < $arResult["NavPageCount"]):
                    if ($arResult["nEndPage"] < ($arResult["NavPageCount"] - 1)): ?>
                        <a class="dots pager-link"
                           href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= round($arResult["nEndPage"] + ($arResult["NavPageCount"] - $arResult["nEndPage"]) / 2) ?>">...</a>
                    <? endif; ?>
                    <a class="pager-link"
                       href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= $arResult["NavPageCount"] ?>"><?= $arResult["NavPageCount"] ?></a>
                <? endif; ?>
                <a class="next pager-link"
                   href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] + 1) ?>"><?= GetMessage("nav_next") ?></a>
            <? endif;
        endif;
    
        if ($arResult["bShowAll"]):
            if ($arResult["NavShowAll"]): ?>
                <a class="pagen pager-link"
                   href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>SHOWALL_<?= $arResult["NavNum"] ?>=0"><?= GetMessage("nav_paged") ?></a>
            <? else: ?>
                <a class="all pager-link"
                   href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>SHOWALL_<?= $arResult["NavNum"] ?>=1"><?= GetMessage("nav_all") ?></a>
            <? endif;
        endif
        ?>
    </nav>
</div>
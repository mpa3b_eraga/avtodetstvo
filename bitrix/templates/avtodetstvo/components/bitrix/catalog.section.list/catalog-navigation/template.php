<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

?>

<nav id="<? echo $arParams['COMPONENT_TEMPLATE']; ?>">
    
    <ul id="catalog-sections" class="menu hide-on-mobiles">
        
        <? foreach ($arResult['SECTIONS'] as $arSection) : ?>
            <li class="item">
                <a href="<? echo $arSection['SECTION_PAGE_URL']; ?>">
                    <? echo $arSection['NAME']; ?>
                    <? if ($arSection['UF_SECTION_SUBTITLE']) : ?>
                        <br><? echo $arSection['UF_SECTION_SUBTITLE']; ?>
                    <? endif; ?>
                </a>
            </li>
        <? endforeach; ?>
    
    </ul>
    
    <div id="catalog-sections-select" class="only-on-mobiles">
        
        <form action="/">
            
            <span class="label">Группа кресла (вес ребёнка):</span>
            
            <select id="sections-list" name="section-id">
                <? foreach ($arResult['SECTIONS'] as $arSection) : ?>
                    <option value="<? echo $arSection['SECTION_PAGE_URL']; ?>">
                        <? echo $arSection['NAME']; ?>
                        <? if ($arSection['UF_SECTION_SUBTITLE']) : ?>
                            (<? echo $arSection['UF_SECTION_SUBTITLE']; ?>)
                        <? endif; ?>
                    </option>
                <? endforeach; ?>
            </select>
        </form>
    
    </div>

</nav>


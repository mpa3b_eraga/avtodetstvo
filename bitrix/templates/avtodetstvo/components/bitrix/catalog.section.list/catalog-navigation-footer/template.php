<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

?>

<div id="<? echo $arParams['COMPONENT_TEMPLATE']; ?>" class="wrapper">
    
    <? if ($arParams['BLOCK_TITLE']) : ?>
        <h2 class="title center-on-mobiles"><? echo $arParams['BLOCK_TITLE']; ?></h2>
    <? endif; ?>
    
    <nav>
        <ul class="menu grid no-gutters center-on-mobiles no-stacking-on-mobiles">
            <? foreach ($arResult['SECTIONS'] as &$arSection) : ?>
                <li class="item half unit">
                    <a href="<? echo $arSection['SECTION_PAGE_URL']; ?>">
                        <? echo $arSection['NAME']; ?>
                        <? if ($arSection['UF_SECTION_SUBTITLE']) : ?>
                            (<? echo $arSection['UF_SECTION_SUBTITLE']; ?>)
                        <? endif; ?>
                    </a>
                </li>
            <? endforeach; ?>
        </ul>
    </nav>

</div>

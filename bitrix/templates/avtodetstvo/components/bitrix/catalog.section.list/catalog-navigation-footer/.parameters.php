<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

$arTemplateParameters = array(
    
    'BLOCK_TITLE' => array(
        "NAME" => 'Заголовок раздела',
        "TYPE" => 'TEXT',
        "DEFAULT" => ''
    )

);
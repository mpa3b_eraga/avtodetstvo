<?php
/**
 * Created by PhpStorm.
 * User: mpa3b
 * Date: 04.02.2016
 * Time: 13:22
 */

$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/lib/jquery/jquery.js');;

$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/lib/chosen/chosen.jquery.js');
$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/lib/chosen/chosen.css');

$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/lib/nicescroll/jquery.nicescroll.js');

$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/lib/ion.rangeSlider/ion.rangeSlider.js');
$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/lib/ion.rangeSlider/ion.rangeSlider.css');
$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/lib/ion.rangeSlider/ion.rangeSlider.skin.css');
<?php
/**
 * Created by PhpStorm.
 * User: mpa3b
 * Date: 09.02.2016
 * Time: 16:54
 */

foreach ($arResult['ITEMS'] as $id => $item) {
    $arResult['ITEMS'][$id]['HTML_ID'] = 'property-' . str_replace('_', '-', strtolower($item['CODE']));
}

$rsSections = CIBlockSection::GetList(
    array(),
    array(
        'IBLOCK_ID' => $arParams['IBLOCK_ID'],
        'DEPTH_LEVEL' => '2'
    ),
    false
);

$items = $arResult['ITEMS'];
unset($arResult['ITEMS']);

foreach ($items as $id => $item) :
    $arResult['ITEMS'][$item['CODE']] = $item;
endforeach;

$section['ID'] = 0;
$section['HTML_ID'] = 'property-section';
$section['IBLOCK_ID'] = $arParams['IBLOCK_ID'];
$section['CODE'] = 'SECTION';
$section['NAME'] = 'Группы';
$section['PROPERTY_TYPE'] = 'L';
$section['DISPLAY_TYPE'] = 'F';
$section['DISPLAY_EXPANDED'] = 'Y';
$section['FILTER_HINT'] = 'возраст и вес ребёнка';

while ($arSection = $rsSections->GetNext()) :

    $section['VALUES'][$arSection['ID']]['CONTROL_ID'] = $arSection['ID'];
    $section['VALUES'][$arSection['ID']]['CONTROL_NAME'] = 'SECTION_' . $arSection['ID'];
    $section['VALUES'][$arSection['ID']]['VALUE'] = $arSection['NAME'];
    $section['VALUES'][$arSection['ID']]['UPPER'] = strtoupper($arSection['NAME']);
    $section['VALUES'][$arSection['ID']]['HTML_VALUE'] = 'Y';
    $section['VALUES'][$arSection['ID']]['URL_ID'] = strtolower($arSection['NAME']);

endwhile;

array_unshift($arResult['ITEMS'], $section);


$tier = $arResult['ITEMS']['TIER'];

foreach ($tier['VALUES'] as $id => $value) :
    if ($value['UPPER'] == 'ISOFIX') $tier['VALUES'][$id]['VALUE'] = 'С ISOfix';
    else $tier['VALUES'][$id]['VALUE'] = 'Крепление ремнём';
endforeach;

unset($arResult['ITEMS']['TIER']);

array_unshift($arResult['ITEMS'], $tier);

$nav = CIBlockSection::GetNavChain(false,$Section['ID']);
while($arSectionPath = $nav->GetNext()){
    if ($GLOBALS['USER']->IsAdmin()){ echo '<pre>';print_r($arSectionPath);echo '</pre>';}
}

?>


/**
 * Created by mpa3b on 04.02.2016.
 */

$(document).on(
    'ready',
    function () {
        
        var filter = $('#catalog-smart-filter');

        $('select#weight-group', filter).chosen(
            {
                width: '100%'
            }
        );

        $('#price-slider', filter).ionRangeSlider(
            {
                type: "double",
                min: 0,
                max: 100000,
                from: 1,
                to: 99999,
                step: 1000,
                onStart: function (data) {
                    $('input#arrFilter_P5_MIN').val(data.from);
                    $('input#arrFilter_P5_MAX').val(data.to);
                },
                onFinish: function (data) {
                    $('input#arrFilter_P5_MIN').val(data.from);
                    $('input#arrFilter_P5_MAX').val(data.to);
                    $('form[name="arrFilter_form"]').trigger('filter-change');
                }
            }
        );
    
        $('ul.list', '#property-brand', filter).niceScroll(
            {
                cursorcolor: "#b3b5b6",
                cursorwidth: 10,
                cursoropacitymin: .5,
                background: "#e5e6e7",
                cursorborder: "0"
            }
        );

    }
);
<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */


?>

<div id="<? echo $arParams['COMPONENT_TEMPLATE']; ?>" class="wrapper">
    
    <? if ($arParams['TITLE']) : ?>
        <h3 class="title"><? echo $arParams['TITLE']; ?></h3>
    <? endif; ?>
    
    <? foreach ($arResult['ITEMS'] as $property) : ?>
        <? $propertyCode = strtolower(str_replace('_', '-', $property['CODE'])) ?>
        <nav class="<? echo $propertyCode; ?> grid no-gutters">
    
            <? foreach ($property['VALUES'] as $value) : ?>
                <?
                $valueCode = Cutil::translit(
                    str_replace('+', 'p', $value['VALUE']),
                    "ru",
                    array(
                        'replace_space' => '-',
                        'replace_other' => '-'
                    )
                );
                ?>
                <a class="half unit"
                   href="<? echo $arParams['SEF_RULE'] . $valueCode . '/'; ?>"><? echo $value['VALUE'] ?></a>
            <? endforeach; ?>

        </nav>
    <? endforeach; ?>
</div>
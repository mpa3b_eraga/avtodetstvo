<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

$arTemplateParameters = array(
    
    'CATALOG_PATH' => array(
        "NAME" => 'Раздел каталога',
        "TYPE" => 'TEXT',
        "DEFAULT" => ''
    )

);
<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */


?>

<div id="<? echo $arParams['COMPONENT_TEMPLATE']; ?>" class="grid">
    <div class="whole unit">
        <? foreach ($arResult['ITEMS'] as $property) : ?>
            <? $propertyCode = strtolower(str_replace('_', '-', $property['CODE'])) ?>
            <div class="<? echo $propertyCode; ?> wrapper">
                <ul id="property-<? echo $propertyCode; ?>" class="property <? echo $propertyCode; ?>">
                    <? foreach ($property['VALUES'] as $value) : ?>
                        <?
                        $valueCode = Cutil::translit(
                            str_replace('+', 'p', $value['VALUE']),
                            "ru",
                            array(
                                'replace_space' => '-',
                                'replace_other' => '-'
                            )
                        );
                        ?>
                        <li>
                            <a href="<? echo $arParams['SEF_RULE'] . $valueCode . '/'; ?>"><? echo $value['VALUE'] ?></a>
                        </li>
                    <? endforeach; ?>
                </ul>
            </div>
        <? endforeach; ?>
    </div>
</div>
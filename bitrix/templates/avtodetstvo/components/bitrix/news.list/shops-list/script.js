(function ($) {

    $(document).on(
        'ready',
        function () {
            $('a.colorbox', '#shops-list').colorbox(
                {
                    href: $(this).attr('href'),
                    inline: true
                }
            );
        }
    );

})(jQuery);
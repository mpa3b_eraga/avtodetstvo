<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */


global $USER;
?>

<ul id="<? echo $arParams['COMPONENT_TEMPLATE']; ?>" class="list">
    
    <? foreach ($arResult["ITEMS"] as $arItem): ?>
        
        <?
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>
        
        <li class="item" <? if ($USER->isAdmin()) : ?> id="<?= $this->GetEditAreaId($arItem['ID']); ?>" <? endif; ?>>
            
            <? echo $arItem['DISPLAY_PROPERTIES']['ADDRESS']['VALUE']; ?>
            
            <? /*

            <a href="#shop-<? echo $arItem['ID']; ?>-location" title="<? echo $arItem['NAME'] ?>"
               class="colorbox"><? echo $arItem['DISPLAY_PROPERTIES']['ADDRESS']['VALUE']; ?></a>


            <div class="hidden">

                <div id="shop-<? echo $arItem['ID']; ?>-location">

                    <? $arPos = explode(",", $arItem['DISPLAY_PROPERTIES']['LOCATION']['VALUE']); ?>

                    <? $APPLICATION->IncludeComponent("bitrix:map.yandex.view", ".default", Array(
                        "INIT_MAP_TYPE" => "MAP",    // Стартовый тип карты
                        "MAP_DATA" => serialize(array("yandex_lat" => $arPos[0], "yandex_lon" => $arPos[1], "yandex_scale" => 13, "PLACEMARKS" => array(array("LON" => $arPos[1], "LAT" => $arPos[0],),),)),    // Данные, выводимые на карте
                        "MAP_WIDTH" => "640",    // Ширина карты
                        "MAP_HEIGHT" => "360",    // Высота карты
                        "CONTROLS" => "SCALELINE",    // Элементы управления
                        "OPTIONS" => "ENABLE_DRAGGING",    // Настройки
                        "MAP_ID" => "shop-" . $arItem["ID"] . "-location-map",    // Идентификатор карты
                    ),
                        false
                    ); ?>

                </div>

            </div>

            */ ?>
        
        </li>
    
    <? endforeach; ?>

</ul>
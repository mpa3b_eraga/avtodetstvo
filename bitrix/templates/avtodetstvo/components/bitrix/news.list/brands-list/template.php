<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

global $USER;

?>

<? if (count($arResult["ITEMS"]) > 0) : ?>
    <div id="<? echo $arParams['COMPONENT_TEMPLATE'] ?>" class="grid">
        <ul class="menu hide-on-mobiles">
    
            <? if (empty($arParams['LIST_LIMIT'])) : ?>
        
                <? foreach ($arResult["ITEMS"] as $arItem): ?>
            
                    <?
                    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                    ?>

                    <? if ($arItem['DISPLAY_PROPERTIES']['MAIN']['VALUE']) {  ?>
                        <li class="item"
                            <? if ($USER->IsAdmin()) : ?>id="<?= $this->GetEditAreaId($arItem['ID']); ?>" <? endif; ?>>
                            <a href="<? echo $arItem["DETAIL_PAGE_URL"] ?>"><? echo $arItem["NAME"] ?></a>
                        </li>
                    <? } ?>
        
                <? endforeach; ?>

                <li id="all-brands">

                    <a href="#" id="all-brands-trigger" class="collapsed">Все бренды</a>

                    <ul id="more-brands" class="dropdown-list">

                        <? foreach ($arResult["ITEMS"] as $num => $arItem): ?>

                            <?
                            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                            ?>

                            <? if (!$arItem['DISPLAY_PROPERTIES']['MAIN']['VALUE']) {  ?>
                                <li class="item" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                                    <a href="<? echo $arItem["DETAIL_PAGE_URL"] ?>"><? echo $arItem["NAME"] ?></a>
                                </li>
                            <? } ?>

                        <? endforeach; ?>

                    </ul>

                </li>
    
            <? else : ?>
        
                <? foreach ($arResult["ITEMS"] as $num => $arItem): ?>
            
                    <?
                    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                    ?>
            
                    <? if ($num < $arParams['LIST_LIMIT']) : ?>
                        <li class="item"
                            <? if ($USER->IsAdmin()) : ?>id="<?= $this->GetEditAreaId($arItem['ID']); ?>" <? endif; ?>>
                            <a href="<? echo $arItem["DETAIL_PAGE_URL"] ?>"><? echo $arItem["NAME"]; ?></a>
                        </li>
                    <? endif; ?>
        
                <? endforeach; ?>
        
                <li id="all-brands">
            
                    <a href="#" id="all-brands-trigger" class="collapsed">Все бренды</a>
            
                    <ul id="more-brands" class="dropdown-list">
                
                        <? foreach ($arResult["ITEMS"] as $num => $arItem): ?>
                    
                            <?
                            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                            ?>
                    
                            <? if ($num > $arParams['LIST_LIMIT']) : ?>
                                <li class="item" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                                    <a href="<? echo $arItem["DETAIL_PAGE_URL"] ?>"><? echo $arItem["NAME"] ?></a>
                                </li>
                            <? endif; ?>
                
                        <? endforeach; ?>
            
                    </ul>
        
                </li>
    
            <? endif; ?>

        </ul>
    
        <div id="mobile-select" class="only-on-mobiles align-center">
        
            <form id="brand-select" action="/">
            
                <span class="label">Выберите марку: </span>
            
                <select name="brand-select-dropdown" id="brand-select-dropdown">
                    <? foreach ($arResult["ITEMS"] as $arItem): ?>
                        <option value="<? echo $arItem["DETAIL_PAGE_URL"] ?>">
                            <? echo $arItem["NAME"] ?>
                        </option>
                    <? endforeach; ?>
                </select>
        
            </form>
    
        </div>

    </div>
<? endif; ?>

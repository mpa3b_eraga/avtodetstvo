/**
 * Created by mpa3b on 14.03.2016.
 */

(function ($) {
    $(document).on(
        'ready',
        function () {

            $('a#all-brands-trigger', '#all-brands').on(
                'click',
                function (event) {

                    event.preventDefault();

                    if ($(this).hasClass('collapsed')) {
                        $('#more-brands').show();
                        $(this).removeClass('expanded').addClass('collapsed');
                    }
                    else {
                        $('#more-brands').hide();
                        $(this).removeClass('collapsed').addClass('expanded');
                    }

                }
            );

            $('select#brand-select-dropdown', '#brand-select').on(
                'change',
                function () {
                    window.location = $(this).find("option:selected").val()
                }
            );

        }
    );
})(jQuery);
(function ($) {

    $(document).on(
        'ready',
        function () {
    
            if (parseInt($(window).width()) < 568 || cssua.ua.mobile) {
                $('img.slide-image', '.slide', '.slider', '#front-slider').each(
                    function () {
                        $(this).attr('src', $(this).data('image-small'));
                    }
                );
            }

            $('.slider', '#front-slider').slick(
                {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    autoplay: true,
                    autoplaySpeed: 2000,
                    cssEase: 'ease',
                    fade: true,
                    dots: false,
                    arrows: false,
                    lazyLoad: 'progressive',
                    dots: true,
                }
            );
        }
    );

})(jQuery);
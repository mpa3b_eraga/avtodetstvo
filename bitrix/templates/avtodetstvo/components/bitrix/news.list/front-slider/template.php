<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

?>

<aside id="<? echo $arParams['COMPONENT_TEMPLATE'] ?>" class="grid">
    
    <div class="slider whole unit">
        <? foreach ($arResult["ITEMS"] as $slide): ?>
    
            <?
            $this->AddEditAction($slide['ID'], $slide['EDIT_LINK'], CIBlock::GetArrayByID($slide["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($slide['ID'], $slide['DELETE_LINK'], CIBlock::GetArrayByID($slide["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
    
            $image_full = CFile::ResizeImageGet(
                $slide['DETAIL_PICTURE'],
                array(
                    'width' => 960,
                    'height' => 400
                ),
                BX_RESIZE_IMAGE_EXACT
            );
    
            $image_small = CFile::ResizeImageGet(
                $slide['DETAIL_PICTURE'],
                array(
                    'width' => 640,
                    'height' => 240
                ),
                BX_RESIZE_IMAGE_EXACT
            );
            ?>
    
            <div class="slide" id="<?= $this->GetEditAreaId($slide['ID']); ?>" data-id="<? echo $slide['ID'] ?>">
                <img class="slide-image" src="<?= $image_full['src']; ?>" alt="<? echo $slide['NAME'] ?>"
                     data-image-small="<?= $image_small['src']; ?>"/>
            </div>

        <? endforeach; ?>
    </div>

</aside>
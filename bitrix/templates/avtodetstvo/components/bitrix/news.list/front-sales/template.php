<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */


global $USER;
?>

<? if (count($arResult['ITEMS']) > 0) : ?>
    
    <section id="<? echo $arParams['COMPONENT_TEMPLATE'] ?>">
        
        <? if ($arParams['TITLE']) : ?>
            <? if ($arParams['TITLE_AS_LINK']) : ?>
                <h2 class="title align-center">
                    <a href="/<? echo $arResult['CODE'] ?>"><? echo $arParams['TITLE'] ?></a>
                </h2>
            <? else: ?>
                <h2 class="title align-center"><? echo $arParams['TITLE'] ?></h2>
            <? endif; ?>
        <? endif; ?>
        
        <div class="grid">
            <? foreach ($arResult["ITEMS"] as $arItem): ?>
    
                <?
    
                if ($USER->IsAdmin()) {
                    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                }
    
                ?>
    
                <div <? if ($USER->IsAdmin()) : ?>id="<?= $this->GetEditAreaId($arItem['ID']); ?>"<? endif; ?>
                     class="one-third unit item">
                    <div class="wrapper grid no-stacking-on-mobiles">
    
                        <? if ($arItem['DETAIL_PICTURE']) : ?>
        
                            <?
                            $image = CFile::ResizeImageGet(
                                $arItem['DETAIL_PICTURE'],
                                array(
                                    'width' => 48,
                                    'height' => 48
                                ),
                                BX_RESIZE_IMAGE_PROPORTIONAL
                            );
                            ?>
                            <div class="one-quarter unit align-center">
                                <img src="<? echo $image['src']; ?>" alt="<? echo $arItemp['NAME']; ?>">
                            </div>
                            <div class="three-quarters unit">
                                <h3 class="title"><? echo $arItem['NAME'] ?></h3>
                            </div>
                            <p><? echo $arItem['PREVIEW_TEXT']; ?></p>
    
                        <? else : ?>
                            <div class="whole unit">
                                <h3 class="title"><? echo $arItem['NAME'] ?></h3>
                                <p><? echo $arItem['PREVIEW_TEXT']; ?></p>
                            </div>
                        <? endif; ?>
    
                        <a class="details" href="<? echo $arItem['DETAIL_PAGE_URL'] ?>"></a>

                    </div>
    
    
                </div>

            <? endforeach; ?>
        
        </div>
    </section>

<? endif; ?>

<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

$arTemplateParameters = array(
    
    'LIST_LIMIT' => array(
        "NAME" => 'Ограничение списка',
        "TYPE" => 'INT',
        "MULTIPLE" => "N",
        "DEFAULT" => '0'
    )


);
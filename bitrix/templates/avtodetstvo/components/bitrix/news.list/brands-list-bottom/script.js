/**
 * Created by mpa3b on 14.03.2016.
 */

(function ($) {

    $(document).on(
        'ready',
        function () {

            $('a#all-brands-trigger', '#all-brands').on(
                'click',
                function () {

                    if ($(this).hasClass('expanded') == false) {
                        $('#more-brands').fadeIn();
                        $(this).removeClass('collapsed').addClass('expanded');
                    }

                    else {
                        $('#more-brands').hide();
                        $(this).removeClass('expanded').addClass('collapsed');
                    }

                }
            );

            $('select#brand-select-dropdown', '#brand-select').on(
                'change',
                function () {
                    window.location = $(this).find("option:selected").val()
                }
            );

        }
    );

})(jQuery);
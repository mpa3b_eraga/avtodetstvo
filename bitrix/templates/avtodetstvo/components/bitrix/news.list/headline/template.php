<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */


global $USER;
?>

<? if (count($arResult["ITEMS"]) > 0) : ?>
    
    <aside id="<? echo $arParams['COMPONENT_TEMPLATE']; ?>" class="wrap">
        
        <div class="slider">
            <? foreach ($arResult["ITEMS"] as $arItem): ?>
                <?
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                ?>
    
                <?
    
                $headlineImage = CFile::ResizeImageGet(
                    $arItem['DISPLAY_PROPERTIES']['HEADLINE_IMAGE']['FILE_VALUE']['ID'],
                    array(
                        'width' => 1140,
                        'height' => 54
                    ),
                    BX_RESIZE_IMAGE_EXACT
                );
                ?>

                <a href="<?= $arItem['DETAIL_PAGE_URL']; ?>">
                    <div
                        class="item" <? if ($USER->isAdmin()) : ?> id="<?= $this->GetEditAreaId($arItem['ID']); ?>" <? endif; ?>
                        style="background-image: url(<? echo $headlineImage['src']; ?>); text-align:center;">
                        <span class="grid no-gutters">
                            <span class="whole unit">
                                <? echo $arItem['DISPLAY_PROPERTIES']['HEADLINE_TEXT']['VALUE']; ?>
                            </span>
                        </span>
                    </div>
                </a>

            <? endforeach; ?>
        </div>
    
    </aside>

<? endif; ?>
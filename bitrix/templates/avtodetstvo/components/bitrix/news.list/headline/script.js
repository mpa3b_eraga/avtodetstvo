(function ($) {

    $(document).on(
        'ready',
        function () {

            $('.slider', '#headline').slick(
                {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    autoplay: true,
                    autoplaySpeed: 2000,
                    cssEase: 'ease',
                    fade: true,
                    dots: false,
                    arrows: false,
                    lazyLoad: 'progressive',
                    dots: false,
                }
            );
        }
    );

})(jQuery);
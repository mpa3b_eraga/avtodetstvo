<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
?>

<div id="search-page">
    <form action="" method="get">
        <? if ($arParams["USE_SUGGEST"] === "Y"):
            if (strlen($arResult["REQUEST"]["~QUERY"]) && is_object($arResult["NAV_RESULT"])) {
                $arResult["FILTER_MD5"] = $arResult["NAV_RESULT"]->GetFilterMD5();
                $obSearchSuggest = new CSearchSuggest($arResult["FILTER_MD5"], $arResult["REQUEST"]["~QUERY"]);
                $obSearchSuggest->SetResultCount($arResult["NAV_RESULT"]->NavRecordCount);
            }
            ?>
            <? $APPLICATION->IncludeComponent(
                "bitrix:search.suggest.input",
                "",
                array(
                    "NAME" => "q",
                    "VALUE" => $arResult["REQUEST"]["~QUERY"],
                    "INPUT_SIZE" => 40,
                    "DROPDOWN_SIZE" => 10,
                    "FILTER_MD5" => $arResult["FILTER_MD5"],
                ),
                $component, array("HIDE_ICONS" => "Y")
            ); ?>
        <? else: ?>
            <input type="text" name="q" value="<?= $arResult["REQUEST"]["QUERY"] ?>" size="40"/>
        <? endif; ?>

        <input type="submit" value="<?= GetMessage("SEARCH_GO") ?>"/>

        <input type="hidden" name="how" value="<? echo $arResult["REQUEST"]["HOW"] == "d" ? "d" : "r" ?>"/>

        <? if ($arParams["SHOW_WHEN"]): ?>
            <script>
                var switch_search_params = function () {
                    var sp = document.getElementById('search_params');
                    var flag;
                    var i;
                    
                    if (sp.style.display == 'none') {
                        flag             = false;
                        sp.style.display = 'block'
                    }
                    else {
                        flag             = true;
                        sp.style.display = 'none';
                    }
                    
                    var from = document.getElementsByName('from');
                    for (
                        i = 0;
                        i < from.length;
                        i++
                    )
                        if (from[i].type.toLowerCase() == 'text') {
                            from[i].disabled = flag;
                        }
                    
                    var to = document.getElementsByName('to');
                    for (
                        i = 0;
                        i < to.length;
                        i++
                    )
                        if (to[i].type.toLowerCase() == 'text') {
                            to[i].disabled = flag;
                        }
                    
                    return false;
                }
            </script>
        <? endif ?>
    </form>


    <? if (isset($arResult["REQUEST"]["ORIGINAL_QUERY"])):
        ?>
        <div class="search-language-guess">
            <? echo GetMessage("CT_BSP_KEYBOARD_WARNING", array("#query#" => '<a href="' . $arResult["ORIGINAL_QUERY_URL"] . '">' . $arResult["REQUEST"]["ORIGINAL_QUERY"] . '</a>')) ?>
        </div><?
    endif; ?>

    <? if ($arResult["ERROR_CODE"] != 0): ?>
        <p><?= GetMessage("SEARCH_ERROR") ?></p>
        <? ShowError($arResult["ERROR_TEXT"]); ?>
        <p><?= GetMessage("SEARCH_CORRECT_AND_CONTINUE") ?></p>

        <p><?= GetMessage("SEARCH_SINTAX") ?><b><?= GetMessage("SEARCH_LOGIC") ?></b></p>
        <table border="0" cellpadding="5">
            <tr>
                <td align="center" valign="top"><?= GetMessage("SEARCH_OPERATOR") ?></td>
                <td valign="top"><?= GetMessage("SEARCH_SYNONIM") ?></td>
                <td><?= GetMessage("SEARCH_DESCRIPTION") ?></td>
            </tr>
            <tr>
                <td align="center" valign="top"><?= GetMessage("SEARCH_AND") ?></td>
                <td valign="top">and, &amp;, +</td>
                <td><?= GetMessage("SEARCH_AND_ALT") ?></td>
            </tr>
            <tr>
                <td align="center" valign="top"><?= GetMessage("SEARCH_OR") ?></td>
                <td valign="top">or, |</td>
                <td><?= GetMessage("SEARCH_OR_ALT") ?></td>
            </tr>
            <tr>
                <td align="center" valign="top"><?= GetMessage("SEARCH_NOT") ?></td>
                <td valign="top">not, ~</td>
                <td><?= GetMessage("SEARCH_NOT_ALT") ?></td>
            </tr>
            <tr>
                <td align="center" valign="top">( )</td>
                <td valign="top">&nbsp;</td>
                <td><?= GetMessage("SEARCH_BRACKETS_ALT") ?></td>
            </tr>
        </table>
    <? elseif (count($arResult["SEARCH"]) > 0): ?>

        <div id="search-result">

            <h2><? echo $arParams["PAGER_TITLE"] . ' (' .$arResult["NAV_RESULT"]->NavRecordCount  . ')'?></h2>

            <? foreach ($arResult["SEARCH"] as $arItem): ?>

                <div id="<? echo 'element' . $arItem["ITEM_ID"] ?>" class="search-element grid">

                    <div class="four-fifths unit">

                        <div class="grid">

                            <div class="image one-fifth unit">

                                <? $rsElementList = CIBlockElement::GetList(
                                    array(),
                                    array("IBLOCK_ID"=> 18, "ID"=>$arItem["ITEM_ID"], "ACTIVE"=> "Y"),
                                    false,
                                    array(),
                                    array("PREVIEW_PICTURE")
                                );

                                while($arElementList = $rsElementList->Fetch()) {

                                    $preview_path = (CFile::GetPath($arElementList['PREVIEW_PICTURE'])); ?>

                                    <? if (!empty($arElementList['PREVIEW_PICTURE'])) { ?>

                                        <img src="<?= $preview_path; ?>">

                                    <? } ?>

                                <?} ?>

                            </div>

                            <div class="description four-fifths unit">

                                <div class="name">
                                    <h3>
                                        <a class="link" href="<? echo $arItem["URL"] ?>"><? echo $arItem["TITLE_FORMATED"] ?></a>
                                    </h3>
                                </div>

                                <div class="brand">

                                    <? $itemID = CCatalogSku::GetProductInfo($arItem["ITEM_ID"]) ; ?>

                                    <? if (!empty($itemID)) {?>

                                        <? $rsElementList = CIBlockElement::GetList(
                                            array(),
                                            array("IBLOCK_ID"=> $itemID["IBLOCK_ID"], "ID"=>$itemID["ID"], "ACTIVE"=> "Y"),
                                            false,
                                            array(),
                                            array("PROPERTY_BRAND.NAME")
                                        ); ?>

                                        <? while($arElementList = $rsElementList->Fetch()) { ?>

                                            <span><? echo $arElementList['PROPERTY_BRAND_NAME'] ?></span>

                                        <?} ?>

                                    <? } ?>

                                </div>
                            </div>
                        </div>

                    </div>


                    <div class="more one-fifth unit">
                        <div class="price">

                            <? $arPrice = CCatalogProduct::GetOptimalPrice($arItem["ITEM_ID"], 1); ?>

                            <? //TODO отобразить скидку ?>
                            <span class="base-price"><? echo number_format($arPrice["RESULT_PRICE"]["BASE_PRICE"],  0, '', ' '); ?></span>

                        </div>

                        <div class="details">
                            <a href="<? echo $arItem["URL"] ?>" class="button">Подробнее</a>
                        </div>
                    </div>


                </div>
            <? endforeach; ?>

        </div>

        <?if($arParams["DISPLAY_BOTTOM_PAGER"] != "N") echo $arResult["NAV_STRING"]?>
    <? else: ?>
        <? ShowNote(GetMessage("SEARCH_NOTHING_TO_FOUND")); ?>
    <? endif; ?>
</div>
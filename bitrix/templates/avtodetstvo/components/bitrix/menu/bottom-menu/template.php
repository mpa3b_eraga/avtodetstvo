<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<? if (!empty($arResult)): ?>
    <nav id="<? echo $arParams['COMPONENT_TEMPLATE'] ?>"
         class="grid no-gutters center-on-mobiles no-stacking-on-mobiles">
    
        <? foreach ($arResult as $arItem): if ($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) continue; ?>
            <span class="half unit">
                <a href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></a>
            </span>
        <? endforeach ?>

    </nav>
<? endif ?>
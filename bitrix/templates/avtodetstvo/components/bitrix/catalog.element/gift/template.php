<div class="grid gift item">
    
    <?
    
    $picture = CFile::ResizeImageGet(
        $arResult['DETAIL_PICTURE'],
        array(
            'width' => '120',
            'height' => '120'
        ),
        BX_RESIZE_IMAGE_EXACT
    );
    
    ?>
    
    <div class="one-third image unit">
        <img src="<? echo $picture['src']; ?>" alt="<? echo $arResult['NAME'] ?>">
    </div>
    
    <div class="two-thirds caption unit">
        <h3 class="title"><? echo $arResult['NAME'] ?></h3>
        
        <? if (!empty($arResult['PREVIEW_TEXT'])) : ?>
            <p><? echo $arResult['PREVIEW_TEXT']; ?></p>
        <? endif; ?>
    </div>

</div>




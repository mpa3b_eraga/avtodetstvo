<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

?>

<? if ($arResult["FILE"] <> '') : ?>
    <aside id="<? echo $arParams['AREA_FILE_SUFFIX'] ?>" class="wrap">
        <div class="grid <? echo $arParams['CLASSES'] ?>">
            <? include($arResult["FILE"]); ?>
        </div>
    </aside>
<? endif; ?>


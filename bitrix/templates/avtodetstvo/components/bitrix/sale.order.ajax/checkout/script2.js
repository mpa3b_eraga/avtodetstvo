$(function () {
        
    $(document).on('ready', function () {
        $('menu#order-checkout-menu a:first').click();
    });
        
    $(document).on('click', 'menu#order-checkout-menu a', function(e){
        e.preventDefault();
        var block = $($(this).attr('block'));
        if (block.attr('data-visited') === 'true') {
            $('menu#order-checkout-menu a').removeClass('selected');
            $(this).addClass('selected');
            block.find('.bx-soa-section-title-container').click();
        }
    });

    $(document).on('click', '.bx-soa-pp-item-container .bx-soa-pp-company', function(e) {
        $(this).find('input[type="checkbox"]').click();
    });

});
<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */


global $USER;
?>

<? if (count($arResult['ITEMS']) > 0) : ?>
    
    <section id="sales">
        
        <div class="grid">
            <? foreach ($arResult["ITEMS"] as $num => $arItem): ?>
            
            <?
            
            if ($USER->IsAdmin()) {
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            }
            
            ?>
            
            <div <? if ($USER->IsAdmin()) : ?>id="<?= $this->GetEditAreaId($arItem['ID']); ?>"<? endif; ?>
                 class="one-third unit item">
                <div class="wrapper grid">
    
                    <? if ($arItem['PREVIEW_PICTURE'] || $arItem['DETAIL_PICTURE']) : ?>
        
                        <?
                        if ($arItem['DETAIL_PICTURE']) $picture = $arItem['DETAIL_PICTURE'];
                        elseif ($arItem['PREVIEW_PICTURE']) $picture = $arItem['PREVIEW_PICTURE'];
                        ?>
        
                        <?
                        $image = CFile::ResizeImageGet(
                            $picture,
                            array(
                                'width' => 48,
                                'height' => 48
                            ),
                            BX_RESIZE_IMAGE_PROPORTIONAL
                        );
                        ?>
                        <img src="<? echo $image['src']; ?>" alt="<? echo $arItem['NAME']; ?>" class="one-fifth unit">
        
                        <h3 class="title four-fifths unit"><? echo $arItem['NAME'] ?></h3>
        
                        <p><? echo $arItem['PREVIEW_TEXT']; ?></p>
    
                    <? else : ?>
                        <div class="whole unit">
                            <h3 class="title"><? echo $arItem['NAME'] ?></h3>
                            <p><? echo $arItem['PREVIEW_TEXT']; ?></p>
                        </div>
                    <? endif; ?>
    
                    <a class="details" href="<? echo $arItem['DETAIL_PAGE_URL'] ?>"></a>

                </div>
            
            </div>
            
            <? if (($num + 1) % 3 == 0) { ?>
        </div>
        <div class="grid">
            <? } ?>

            <? endforeach; ?>

        </div>
    </section>

<? endif; ?>

<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */


global $USER;

?>

<? if (count($arResult['ITEMS']) > 0) : ?>
    
    
    <? if ($arParams["DISPLAY_TOP_PAGER"]): ?>
        <div class="top pager grid">
            <div class="whole unit">
                <?= $arResult["NAV_STRING"] ?>
            </div>
        </div>
    <? endif; ?>
    
    <div id="articles" class="articles">
        
        <ul class="list">
            <? foreach ($arResult["ITEMS"] as $arItem): ?>
    
                <?
                if ($USER->IsAdmin()) :
                    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));

                endif;
                ?>
    
                <li <? if ($USER->IsAdmin()) : ?>id="<?= $this->GetEditAreaId($arItem['ID']); ?>"<? endif; ?>
                    class="item grid">
        
                    <? if ($arItem['DETAIL_PICTURE']) : ?>
                        <div class="one-fifth unit">
                            <?
                            $image = CFile::ResizeImageGet(
                                $arItem['DETAIL_PICTURE'],
                                array(
                                    'width' => 240,
                                    'height' => 180
                                ),
                                BX_RESIZE_IMAGE_PROPORTIONAL
                            );
                            ?>
                            <img src="<? echo $image['src']; ?>" alt="<? echo $arItem['NAME'] ?>">
                        </div>
                        <div class="four-fifths unit">
                            <h2 class="title">
                                <a href="<? echo $arItem['DETAIL_PAGE_URL'] ?>"><? echo $arItem['NAME']; ?></a>
                            </h2>
                            <p><? echo $arItem['PREVIEW_TEXT'] ?></p>
                            <p class="date"><? echo $arItem['DISPLAY_ACTIVE_FROM'] ?></p>
                        </div>
                    <? else : ?>
                        <div class="whole unit">
                            <h2 class="title">
                                <a href="<? echo $arItem['DETAIL_PAGE_URL'] ?>"><? echo $arItem['NAME']; ?></a>
                            </h2>
                            <p><? echo $arItem['PREVIEW_TEXT'] ?></p>
    
                            <p class="date"><? echo $arItem['DISPLAY_ACTIVE_FROM'] ?></p>
                        </div>
                    <? endif; ?>
    
                </li>

            <? endforeach; ?>
        </ul>
    
    </div>
    
    <? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
        <div class="top pager grid">
            <div class="whole unit">
                <?= $arResult["NAV_STRING"] ?>
            </div>
        </div>
    <? endif; ?>

<? endif; ?>
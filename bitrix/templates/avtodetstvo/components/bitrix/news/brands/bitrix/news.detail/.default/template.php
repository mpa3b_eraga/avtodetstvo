<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

?>

<? if ($arResult) : ?>
    
    <article id="<? echo $arResult['CODE']; ?>" class="brand grid">
        
        <? if (!empty($arResult["DETAIL_PICTURE"]["SRC"])) : ?>
            <div class="one-quarter unit align-center image">
                <img class="brand-logo"
                     src="<?= $arResult["DETAIL_PICTURE"]["SRC"] ?>"
                     alt="<?= $arResult["DETAIL_PICTURE"]["ALT"] ?>"
                     title="<?= $arResult["DETAIL_PICTURE"]["TITLE"] ?>"/>
            </div>
            <div class="three-quarters unit caption">
                <? echo $arResult["DETAIL_TEXT"]; ?>
            </div>
        <? else : ?>
            <div class="whole unit caption">
                <? echo $arResult["DETAIL_TEXT"]; ?>
            </div>
        <? endif; ?>
    
    </article>

<? endif; ?>
<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

?>

<div id="brands-page" class="grid">
    
    <section id="brands" class="items grid no-gutters">
        
        <? foreach ($arResult["ITEMS"] as $index => $arItem): ?>
            
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
            
            <article id="<?= $this->GetEditAreaId($arItem['ID']); ?>" class="item one-quarter unit">
                
                <div class="wrapper align-center">
                    
                    <?
                    
                    $picture = CFile::ResizeImageGet(
                        $arItem['PREVIEW_PICTURE'],
                        array(
                            'width' => '180',
                            'height' => '120'
                        ),
                        BX_RESIZE_IMAGE_PROPORTIONAL
                    );
                    
                    ?>
                    
                    <? if ($arItem['PREVIEW_PICTURE']) : ?>
                        <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>">
                            <img src="<?= $picture["src"]; ?>"
                                 alt="<?= $arItem["PREVIEW_PICTURE"]["ALT"] ?>"
                                 title="<?= $arItem["PREVIEW_PICTURE"]["TITLE"] ?>"/>
                        </a>
                    <? endif; ?>
                    
                    <h2 class="title">
                        <a href="<? echo $arItem["DETAIL_PAGE_URL"] ?>"><? echo $arItem["NAME"] ?></a>
                    </h2>
                
                </div>
            
            </article>
        
        <? endforeach; ?>
    
    </section>

</div>

</div>

<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$priceId = CCatalogGroup::GetList(
    array(),
    array('NAME' => $arParams['PRICE_CODE'][0]),
    false,
    false,
    array()
)->Fetch()['ID'];

global $arrFilter;

// цена
$price_min = (isset($_REQUEST['price_min'])) ? $_REQUEST['price_min']: 1;
$price_max = (isset($_REQUEST['price_max'])) ? $_REQUEST['price_max']: 99999;
$arrFilter[] = array(
    '>=CATALOG_PRICE_' . $priceId => $price_min
);
$arrFilter[] = array(
    '<=CATALOG_PRICE_' . $priceId => $price_max
);

// ajax-фильтры
if ($_REQUEST['AJAX'] == 'Y') {
    if (isset($_REQUEST['SECTIONS']) && !empty($_REQUEST['SECTIONS'])) {
        $ids = [];
        foreach ($_REQUEST['SECTIONS'] as $section_id)
            $ids[] = $section_id;
        $arrFilter[] = ['SECTION_ID' => $ids];
    }
    if (!empty($_REQUEST['properties'])) {
        foreach ($_REQUEST['properties'] as $propId => $values) {
            $property = CIBlockElement::GetProperty(
                6,
                $propId,
                [],
                ['ID' => $propId]
            )->Fetch()['CODE'];
            foreach ($values as $value) {
                $filter['PROPERTY_' . $property] = $value;
            }
            $arrFilter[] = $filter;
        }
    }
    if ($_REQUEST['crush-tested']) {
        $arrFilter[] = [
            '!PROPERTY_CRASH_TEST_RESULTS' => false
        ];
    }
    if ($_REQUEST['avaliable']) {
        $arrFilter[] = [
            '>CATALOG_QUANTITY' => 0
        ];
    }
}

?>

<? if ($arParams["USE_COMPARE"] == "Y") { ?>
    
    <aside id="catalog-compare" class="grid">
        
        <? $APPLICATION->IncludeComponent(
            "bitrix:catalog.compare.list",
            "",
            array(
                "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                "NAME" => $arParams["COMPARE_NAME"],
                "DETAIL_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["element"],
                "COMPARE_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["compare"],
                "ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
                "PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
                'POSITION_FIXED' => isset($arParams['COMPARE_POSITION_FIXED']) ? $arParams['COMPARE_POSITION_FIXED'] : '',
                'POSITION' => isset($arParams['COMPARE_POSITION']) ? $arParams['COMPARE_POSITION'] : ''
            ),
            $component,
            array("HIDE_ICONS" => "Y")
        ); ?>
    
    </aside>

<? } ?>

<div id="catalog-section" class="catalog sections grid">
    
    <aside id="catalog-section-left" class="one-quarter unit">
        
        <? $APPLICATION->IncludeComponent(
            "bitrix:catalog.smart.filter",
            "",
            array(
                "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                "SECTION_ID" => $arCurSection['ID'],
                "FILTER_NAME" => $arParams["FILTER_NAME"],
                "PRICE_CODE" => $arParams["PRICE_CODE"],
                "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                "CACHE_TIME" => $arParams["CACHE_TIME"],
                "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                "SAVE_IN_SESSION" => "N",
                "FILTER_VIEW_MODE" => $arParams["FILTER_VIEW_MODE"],
                "XML_EXPORT" => "N",
                "SECTION_TITLE" => "NAME",
                "SECTION_DESCRIPTION" => "DESCRIPTION",
                'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],
                "TEMPLATE_THEME" => $arParams["TEMPLATE_THEME"],
                'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
                'CURRENCY_ID' => $arParams['CURRENCY_ID'],
                "SEF_MODE" => $arParams["SEF_MODE"],
                "SEF_RULE" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["smart_filter"],
                "SMART_FILTER_PATH" => $arResult["VARIABLES"]["SMART_FILTER_PATH"],
                "PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
                "FILTER_TITLE" => $arParams["FILTER_TITLE"],
                "USED_FILTER_FIELDS" => $arParams["USED_FILTER_FIELDS"]
            ),
            $component,
            array('HIDE_ICONS' => 'Y')
        ); ?>
    
    </aside>
    
    <div id="catalog-sections" class="three-quarters unit catalog section">
        
        <? if ($_REQUEST["CATALOG_SORT_MODE"]) {
            switch ($_REQUEST["CATALOG_SORT_MODE"]) :
                case 'PRICE_ASC':
                    $arParams["ELEMENT_SORT_FIELD"] = "CATALOG_PRICE_" . $priceID;
                    $arParams["ELEMENT_SORT_ORDER"] = "asc";
                    break;
                case 'PRICE_DSC':
                    $arParams["ELEMENT_SORT_FIELD"] = "CATALOG_PRICE_" . $priceID;
                    $arParams["ELEMENT_SORT_ORDER"] = "desc";
                    break;
                case 'NAME_ASC':
                    $arParams["ELEMENT_SORT_FIELD"] = "name";
                    $arParams["ELEMENT_SORT_ORDER"] = "asc";
                    break;
                case 'NAME_DSC':
                    $arParams["ELEMENT_SORT_FIELD"] = "name";
                    $arParams["ELEMENT_SORT_ORDER"] = "desc";
                    break;
                default:
                    $arParams["ELEMENT_SORT_FIELD"] = "shows";
                    $arParams["ELEMENT_SORT_ORDER"] = "desc";
                    $_REQUEST['CATALOG_SORT_MODE'] = 'DEFAULT';
                    break;
            endswitch;
        } ?>
        
        <aside id="catalog-sort" class="pull-right center-on-mobiles">
            <form id="catalog-sort-form" name="CATALOG_SORT" method="post">
                <label for="catalog-sort-form-select">Сортировать по: </label>
                <?
                $CATALOG_SORT_MODE = array(
                    'DEFAULT' => 'по умолчанию',
                    'PRICE_ASC' => 'по цене (дешевле)',
                    'PRICE_DSC' => 'по цене (дороже)',
                    'NAME_ASC' => 'по названию (А-Я)',
                    'NAME_DSC' => 'по названию (Я-А)'
                );
                ?>
                <select name="CATALOG_SORT_MODE" id="catalog-sort-form-select">
                    <? foreach ($CATALOG_SORT_MODE as $value => $option) : ?>
                        <option
                            value="<? echo $value ?>" <? if ($_REQUEST['CATALOG_SORT_MODE'] == $value) echo 'selected="selected"'; ?>><? echo $option ?></option>
                    <? endforeach; ?>
                </select>
            </form>
        </aside>
        
        <? if ($_REQUEST['AJAX'] == 'Y') $APPLICATION->RestartBuffer(); ?>
        
        <? $APPLICATION->IncludeComponent(
            "bitrix:catalog.top",
            "",
            array(
                "DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
                "DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
                "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                "FILTER_NAME" => $arParams["FILTER_NAME"],
                "ELEMENT_SORT_FIELD" => $arParams["ELEMENT_SORT_FIELD"],
                "ELEMENT_SORT_ORDER" => $arParams["ELEMENT_SORT_ORDER"],
                //                "ELEMENT_SORT_FIELD2" => $arParams["TOP_ELEMENT_SORT_FIELD2"],
                //                "ELEMENT_SORT_ORDER2" => $arParams["TOP_ELEMENT_SORT_ORDER2"],
                "SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
                "DETAIL_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["element"],
                "BASKET_URL" => $arParams["BASKET_URL"],
                "ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
                "PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
                "SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
                "PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
                "PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
                "DISPLAY_COMPARE" => $arParams["USE_COMPARE"],
                "TOP_PER_PAGE" => 12,
                "ELEMENT_COUNT" => $arParams["TOP_ELEMENT_COUNT"],
                "LINE_ELEMENT_COUNT" => $arParams["TOP_LINE_ELEMENT_COUNT"],
                "PROPERTY_CODE" => $arParams["TOP_PROPERTY_CODE"],
                "PRICE_CODE" => $arParams["PRICE_CODE"],
                "USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
                "SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],
                "PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
                "PRICE_VAT_SHOW_VALUE" => $arParams["PRICE_VAT_SHOW_VALUE"],
                "USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
                "ADD_PROPERTIES_TO_BASKET" => (isset($arParams["ADD_PROPERTIES_TO_BASKET"]) ? $arParams["ADD_PROPERTIES_TO_BASKET"] : ''),
                "PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ''),
                "PRODUCT_PROPERTIES" => $arParams["PRODUCT_PROPERTIES"],
                "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                "CACHE_TIME" => $arParams["CACHE_TIME"],
                "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                "OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
                "OFFERS_FIELD_CODE" => $arParams["TOP_OFFERS_FIELD_CODE"],
                "OFFERS_PROPERTY_CODE" => $arParams["TOP_OFFERS_PROPERTY_CODE"],
                "OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
                "OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
                "OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
                "OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
                "OFFERS_LIMIT" => $arParams["TOP_OFFERS_LIMIT"],
                'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
                'CURRENCY_ID' => $arParams['CURRENCY_ID'],
                'HIDE_NOT_AVAILABLE' => $arParams['HIDE_NOT_AVAILABLE'],
                'VIEW_MODE' => (isset($arParams['TOP_VIEW_MODE']) ? $arParams['TOP_VIEW_MODE'] : ''),
                'ROTATE_TIMER' => (isset($arParams['TOP_ROTATE_TIMER']) ? $arParams['TOP_ROTATE_TIMER'] : ''),
                'TEMPLATE_THEME' => (isset($arParams['TEMPLATE_THEME']) ? $arParams['TEMPLATE_THEME'] : ''),
                'LABEL_PROP' => $arParams['LABEL_PROP'],
                'ADD_PICT_PROP' => $arParams['ADD_PICT_PROP'],
                'PRODUCT_DISPLAY_MODE' => $arParams['PRODUCT_DISPLAY_MODE'],
    
                'OFFER_ADD_PICT_PROP' => $arParams['OFFER_ADD_PICT_PROP'],
                'OFFER_TREE_PROPS' => $arParams['OFFER_TREE_PROPS'],
                'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
                'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
                'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
                'MESS_BTN_BUY' => $arParams['MESS_BTN_BUY'],
                'MESS_BTN_ADD_TO_BASKET' => $arParams['MESS_BTN_ADD_TO_BASKET'],
                'MESS_BTN_SUBSCRIBE' => $arParams['MESS_BTN_SUBSCRIBE'],
                'MESS_BTN_DETAIL' => $arParams['MESS_BTN_DETAIL'],
                'MESS_NOT_AVAILABLE' => $arParams['MESS_NOT_AVAILABLE'],
                'ADD_TO_BASKET_ACTION' => $basketAction,
                'SHOW_CLOSE_POPUP' => isset($arParams['COMMON_SHOW_CLOSE_POPUP']) ? $arParams['COMMON_SHOW_CLOSE_POPUP'] : '',
                'COMPARE_PATH' => $arResult['FOLDER'] . $arResult['URL_TEMPLATES']['compare']
            ),
            $component
        ); ?>
        
        <? if ($_REQUEST['AJAX'] == 'Y') die(); ?>
    
    </div>

</div>
/**
 * Created by mpa3b on 11.02.2016.
 */

(function($) {
    $.fn.serializeAssoc = function() {
        var addData = function(dt, key, value) {
            if (key.length == 0) {
                dt = value;
            } else {
                var k = key[0];
                key.shift();
                if (k == '') {
                    if (key.length == 0) {
                        dt.push(value);
                    } else {
                        dt.push(addData([], key, value));
                    }
                } else {
                    if (!dt[k])
                        dt[k] = [];
                    if (key.length == 0) {
                        dt[k] = value;
                    } else {
                        dt[k] = addData(dt[k], key, value);
                    }
                }
            }
            return dt;
        }
        var data = {};
        var array = this.serializeArray();
        for (var a in array) {
            var key = array[a].name.split('[');
            for (var k in key)
                key[k] = key[k].replace(']', '');
            data = addData(data, key, array[a].value);
        }
        return data;
    };
})(jQuery);

$(function () {

    // Инициализация слайдера
    function initSlider() {
        var item = $('article.item', '.catalog.section');

        item.each(
            function () {
                var firstURL = $(this).find('.slider .slide.first a').attr('href');
                $(this).find('a.details').attr('href', firstURL);
            }
        );

        item.on(
            'mousewheel',
            function (event) {

                event.preventDefault();

                if (event.deltaY > 0) {
                    $(this).find('.slider').slick('slickPrev');
                }
                if (event.deltaY < 0) {
                    $(this).find('.slider').slick('slickNext');
                }

            }
        );

        item.find('.slider:not(.slick-initialized)').slick(
            {
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: true,
                buttons: false,
                prevArrow: '<a class="slick-arrow slick-prev" href="#"></a>',
                nextArrow: '<a class="slick-arrow slick-next" href="#"></a>',
                useCSS: true,
                infinite: false
            }
        );

        item.find('.slider').on(
            'afterChange',
            function () {
                var url = $(this).find('.slick-active a').attr('href');
                $('a.details', $(this).closest('.item')).attr('href', url);
            }
        );

        item.find('a.color').on(
            'click',
            function (event) {
                event.preventDefault();
                $('.slider', $(this).closest('.item')).slick('slickGoTo', $(this).data('index'));
            }
        );

        item.find('a.hide-colors').on(
            'click',
            function (event) {

                event.preventDefault();
                var parent = $(this).closest('.item');

                $(this).hide();
                $('a.show-colors', parent).show();
                $('.colors-list', parent).hide();
                $('.buy', parent).show();

            }
        );

        item.find('a.show-colors').on(
            'click',
            function (event) {

                event.preventDefault();
                var parent = $(this).closest('.item');

                $(this).hide();
                $('a.hide-colors', parent).show();
                $('.colors-list', parent).show();
                $('.buy', parent).hide();

            }
        );
    }

    // Обновление списка товаров
    function renewItems(renew) {
        var data               = $('form[name="arrFilter_form"]').serializeAssoc();
        data.CATALOG_SORT_MODE = $('select[name="CATALOG_SORT_MODE"]').val();
        data.AJAX              = 'Y';
        data.PAGEN_1           = (renew) ? 1 : parseInt($('#section-contents').data('page')) + 1;
        $.ajax({
            url: window.location.href,
            data: data,
                   dataType: 'json',
            method: 'POST',
            cache: false,
            success: function (response) {
                if (renew) {
                    $('#section-contents').html(response.content);
                } else {
                    $('#section-contents').append(response.content);
                }
                $('#total-count').html(response.total_count);
                $('#section-contents').data('page', response.current_page);
                $('article.item', '.catalog.section').find('.slide').removeClass('hidden-before-load');
                $('button#show-next-items').blur();
                if (response.next_page) {
                    $('button#show-next-items').css('display', 'inline-block');
                } else {
                    $('button#show-next-items').css('display', 'none');
                }
                initSlider();
                var button = $('.catalog.section button#show-next-items');
                button.removeClass('processing');
                if (button.data('text'))
                    button.html(button.data('text'));
            }
        });
    }

    // Поля фильтра
    $('form[name="arrFilter_form"] select').on('change', function() {
        renewItems(true);
    });
    $('form[name="arrFilter_form"] :checkbox').on('change', function() {
        renewItems(true);
    });
    $('form[name="arrFilter_form"]').on('filter-change', function() {
        renewItems(true);
    });

    // Следующие товары
    $('.catalog.section button#show-next-items').on('click', function(event) {
        $(this).addClass('processing');
        $(this).data('text', $(this).html());
        $(this).html('Загружаю...');
        event.preventDefault();
        renewItems(false);
        return false;
    });

    // Сортировка
    $('#catalog-sort-form').on('change', function() {
        renewItems(true);
    });


    $(document).on('ready', function () {
        $('.hidden-before-load').fadeIn();
        $('select#catalog-sort-form-select', '#catalog-sort-form').chosen({
            disable_search: true
        });
        initSlider();
    });

});
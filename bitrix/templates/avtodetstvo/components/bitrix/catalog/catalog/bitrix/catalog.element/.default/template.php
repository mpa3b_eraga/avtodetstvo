<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

// фильтры отдать наверх

global $arrAccompanyingFilter,
       $arrSimilarFilter;

// Сопутствующие товары

$arrAccompanyingFilter = array(
    'ACTIVE' => 'Y',
    'IBLOCK_TYPE' => $arParams['IBLOCK_TYPE'],
    'IBLOCK_ID' => $arParams['IBLOCK_ID'],
    'ID' => $arResult['PROPERTIES']['ACCOMPANYING_GOODS']['VALUE'],
);

// Похожие товары

$arrSimilarFilter = array(
    'ACTIVE' => 'Y',
    'IBLOCK_TYPE' => $arParams['IBLOCK_TYPE'],
    'IBLOCK_ID' => $arParams['IBLOCK_ID'],
    'ID' => $arResult['PROPERTIES']['SIMILAR_CARCHAIR']['VALUE'],
);

?>

<article id="product-<? echo $arResult['ID'] ?>" data-code="<? echo $arResult['CODE'] ?>" class="catalog element item">
    
    <section id="element" class="grid">
        
        <div class="one-third unit align-center">
            
            <?
            
            $detailPictureParams = array(
                'width' => '320',
                'height' => '320'
            );
            
            $bigPictureParams = array(
                'width' => '800',
                'height' => '600'
            );
            
            $picture = CFile::ResizeImageGet(
                $arResult['DETAIL_PICTURE'],
                $detailPictureParams,
                BX_RESIZE_BX_RESIZE_IMAGE_PROPORTIONAL
            );
            
            $bigPicture = CFile::ResizeImageGet(
                $arResult['DETAIL_PICTURE'],
                $bigPictureParams,
                BX_RESIZE_BX_RESIZE_IMAGE_PROPORTIONAL
            );
            
            ?>
            
            <div id="main-image" class="wrapper">
                
                <a href="<? echo $bigPicture['src']; ?>" class="colorbox" rel="images">
                    <img src="<? echo $picture['src'] ?>" alt="<? echo $arResult['NAME']; ?>">
                </a>
            
            </div>
        
        </div>
        
        <div class="two-thirds unit">
            
            <div class="grid">
                
                <div id="element-description" class="three-fifths unit">
                    
                    <h1 id="element-title" class="title center-on-mobiles"><? echo $arResult['NAME'] ?></h1>
                    <h2 id="element-brand" class="brand center-on-mobiles">
                        <? echo $arResult['DISPLAY_PROPERTIES']['BRAND']['DISPLAY_VALUE']; ?>
                        <? if (!empty($arResult['BRAND']['COUNTRY']['VALUE'])) : ?>
                            <span>(<? echo $arResult['BRAND']['COUNTRY']['VALUE']; ?>)</span>
                        <? endif; ?>
                    </h2>
    
    
                    <? if (!empty($arResult['PROPERTIES']['GIFT']['VALUE'])) : ?>
                        <span class="gift-marker" title="Подарок!">
                            <a href="#gifts?inline=true" class="colorbox inline"></a>
                        </span>
                    <? endif; ?>
                    
                    <ul id="properties">
                        
                        <li id="age-group" class="property item grid no-stacking-on-mobiles">
                            <span class="name three-fifths unit">Возрастная группа</span>
                            <span class="value two-fifths unit align-right">
                                <a href="<? echo $arResult['SECTION']['SECTION_PAGE_URL']; ?>">
                                    <? echo $arResult['SECTION']['NAME']; ?>
                                </a>
                            </span>
                        </li>
                        
                        <li id="weight-group" class="property item grid no-stacking-on-mobiles">
                            <span class="name three-fifths unit">Вес ребёнка</span>
                            <span class="value two-fifths unit align-right">
                                <? echo $arResult['SECTION_SUBTITLE']; ?>
                            </span>
                        </li>
                        
                        <? foreach ($arResult['DISPLAY_PROPERTIES'] as $code => $property): ?>
                            <?
                            switch ($code) {
    
                                case 'CRASH_TEST_RESULTS' : ?>
                                    <li id="test-results" class="property item grid no-stacking-on-mobiles">
                                        <span class="name half unit"><? echo $property['NAME']; ?></span>
                                        <span class="value half unit align-right"
                                              title="Оценка: <? echo $property['VALUE']; ?>">

                                            <div id="rating" data-rating="<? echo $property['VALUE']; ?>">
                                                <div class="background-red"></div>
                                               <div class="stars"></div>

                                            </div>
                                        </span>
                                    </li>
                                    <? break;
    
                                case 'TIER' : ?>
                                    <li id="tier" class="property item grid no-stacking-on-mobiles">
                                        <span class="name three-fifths unit"><? echo $property['NAME']; ?></span>
                                        <span class="value two-fifths unit align-right">
                                            <ul class="list">
                                                <? $count = count($property['VALUE_ENUM']); ?>
                                                <? foreach ($property['VALUE_ENUM'] as $value) : ?>
                                                    <li class="item"><? echo $value; ?></li>
                                                <? endforeach; ?>
                                            </ul>
                                        </span>
                                    </li>
                                    <? break;
    
                                case 'WARRANTY' : ?>
                                    <li id="warranty" class="property item grid no-stacking-on-mobiles">
                                        <span class="name three-fifths unit"><? echo $property['NAME']; ?></span>
                                        <span
                                            class="value two-fifths unit align-right"><? echo $property['VALUE']; ?></span>
                                    </li>
                                    <? break;
                                default :
                                    break;
                            }
                            ?>
                        <? endforeach; ?>
                        
                        <li id="article" class="property item grid no-stacking-on-mobiles">
                            <span class="name three-fifths unit">Артикул</span>
                            <span class="value two-fifths unit align-right">
                                <? echo $arResult['ARTICLE']; ?>
                            </span>
                        </li>
                        
                        <li id="color" class="property item grid">
                            <span class="name three-fifths unit">Цвет</span>
                            <span class="value two-fifths unit align-right">
                                <? echo $arResult['COLOR']; ?>
                            </span>
                        </li>
                    
                    </ul>
                
                </div>
                
                <div id="buy" class="two-fifths unit align-center">
                    
                    <div class="wrapper">
                        
                        <div id="purchase">
                            
                            <? if (!empty($arResult['MIN_PRICE']['VALUE'])) : ?>
                                <p class="price">
                                    <span
                                        class="value"><? echo number_format(floatval($arResult['MIN_PRICE']['VALUE']), 0, '', ' '); ?></span>
                                </p>
                            <? endif; ?>
    
                            <div class="status <? if ($arResult['CATALOG_QUANTITY'] < 1): ?>not-<? endif; ?>available"
                                 data-quantity="<? echo $arResult['CATALOG_QUANTITY']; ?>">
                                <p class="absent">Под заказ</p>
                                <p class="present">В наличии</p>
                            </div>
                            
                            <p>
                                <a href="<? echo $arResult['ADD_URL']; ?>" class="add button">Положить в корзину</a>
                            </p>
    
                            <div
                                class="status <? if ($arResult['CATALOG_QUANTITY'] < 1): ?>not-<? endif; ?>available">
                                <p>
                                    <a href="#<? echo $arResult['ID']; ?>" class="one-click-buy present">Купить в один
                                        клик</a>
                                </p>
                            </div>
    
                            <div id="additional-options"
                                 class="grid no-stacking-on-mobiles status <? if ($arResult['CATALOG_QUANTITY'] < 1) echo 'not-'; ?>availiable"
                                 data-quantity="<? echo $arResult['CATALOG_QUANTITY']; ?> ">
                                <div class="half unit align-left">
                                    <span id="delivery-info-btn">
                                        <a href="#delivery-info">Доставка сегодня</a>
                                    </span>
                                    <span id="delivery-delayed-info-btn">
                                        <a href="#delivery-info">Доставка за <? echo $arResult['BRAND']['DELIVERY_PERIOD']['VALUE']; ?></a>
                                    </span>
                                </div>
                                <div class="half unit align-right">
                                    <a href="#found-cheaper">Нашли дешевле?</a>
                                </div>
                            </div>

                        </div>
    
                        <aside id="delivery-info" class="hidden">
                            <div class="frame">
                                <? $APPLICATION->IncludeComponent(
                                    "bitrix:main.include",
                                    ".default",
                                    array(
                                        "AREA_FILE_SHOW" => "sect",
                                        "AREA_FILE_SUFFIX" => "delivery_info",
                                        "COMPONENT_TEMPLATE" => ".default",
                                        "EDIT_TEMPLATE" => "",
                                        "AREA_FILE_RECURSIVE" => "Y"
                                    ),
                                    false
                                ); ?>
                                <p><a href="#" class="close">Закрыть</a></p>
                            </div>
                        </aside>
    
                        <aside id="found-cheaper" class="hidden">
                            <div class="frame">
                                <? $APPLICATION->IncludeComponent(
                                    "bitrix:form",
                                    "webform",
                                    Array(
                                        "AJAX_MODE" => "N",
                                        "AJAX_OPTION_ADDITIONAL" => "",
                                        "AJAX_OPTION_HISTORY" => "N",
                                        "AJAX_OPTION_JUMP" => "N",
                                        "AJAX_OPTION_STYLE" => "N",
                                        "CACHE_TIME" => "3600",
                                        "CACHE_TYPE" => "A",
//                                        "CHAIN_ITEM_LINK" => "Found cheaper?",
//                                        "CHAIN_ITEM_TEXT" => "Нашли дешевле?",
                                        "EDIT_ADDITIONAL" => "N",
                                        "EDIT_STATUS" => "N",
                                        "IGNORE_CUSTOM_TEMPLATE" => "N",
                                        "NOT_SHOW_FILTER" => array("", ""),
                                        "NOT_SHOW_TABLE" => array("", ""),
                                        "RESULT_ID" => $_REQUEST[RESULT_ID],
                                        "SEF_MODE" => "N",
                                        "SHOW_ADDITIONAL" => "N",
                                        "SHOW_ANSWER_VALUE" => "N",
                                        "SHOW_EDIT_PAGE" => "N",
                                        "SHOW_LIST_PAGE" => "N",
                                        "SHOW_STATUS" => "N",
                                        "SHOW_VIEW_PAGE" => "N",
                                        "START_PAGE" => "new",
                                        "SUCCESS_URL" => "",
                                        "USE_EXTENDED_ERRORS" => "N",
                                        "VARIABLE_ALIASES" => Array(
                                            "action" => "action"
                                        ),
                                        "WEB_FORM_ID" => "1"
                                    )
                                ); ?>
                                <p><a href="#" class="close">Закрыть</a></p>
                            </div>
                        </aside>
                    
                    </div>
                
                </div>
            
            </div>
        
        </div>
    
    </section>
    
    <? if (count($arResult['OFFERS']) > 0) : ?>
        
        <section id="colors" class="grid">
            
            <h2 class="center-on-mobiles">Расцветки</h2>
            
            <div class="grid">
                
                <? foreach ($arResult['OFFERS'] as $num => $arOffer) : ?>
                    
                    <div id="offer-<? echo $arOffer['ID'] ?>"
                         class="unit item offer <?php if ($arOffer['ID'] == $arResult['SELECTED_ID'] || $arOffer['SELECTED'] == 'Y') { ?>selected<?php } ?>">
                        
                        <div class="wrapper">
                            
                            <?
                            $arPreviewParams = array(
                                'width' => '320',
                                'height' => '320'
                            );
                            
                            $arPreviewImg = CFile::ResizeImageGet(
                                $arOffer['DETAIL_PICTURE'],
                                $arPreviewParams,
                                BX_RESIZE_BX_RESIZE_IMAGE_PROPORTIONAL
                            );
                            ?>
                            
                            <article id="offer-<? echo $arOffer['ID']; ?>"
                                     data-id="<? echo $arOffer['ID']; ?>"
                                     data-article="<? echo $arOffer['PROPERTIES']['CML2_ARTICLE']['VALUE']; ?>"
                                     data-add-url="<? echo $arOffer['ADD_URL']; ?>"
                                     data-buy-url="<? echo $arOffer['BUY_URL']; ?>"
                                     data-price="<? echo number_format(floatval($arOffer['MIN_PRICE']['VALUE']), 0, '', ' '); ?>"
                                     data-quantity="<? echo $arOffer['CATALOG_QUANTITY']; ?>"
                                     data-status="<? if ($arOffer['CATALOG_QUANTITY'] < 1) echo 'not-'; ?>available"
                                     data-detail-image="<? echo $arOffer['DETAIL_PICTURE']['SRC']; ?>"
                                     data-preview-image="<? echo $arPreviewImg['src']; ?>"
                                     data-color-name="<? echo $arOffer['PROPERTIES']['COLOR_NAME']['VALUE']; ?>"
                                     class="color align-center product <? if ($arOffer['SELECTED'] == 'Y') echo 'selected'; ?>">
                                
                                <div class="header">
                                    <h3><? echo $arOffer['PROPERTIES']['COLOR_NAME']['VALUE']; ?></h3>
                                </div>
                                
                                <?
                                $arThumbParams = array(
                                    'width' => '110',
                                    'height' => '120'
                                );
                                
                                $arThumb = CFile::ResizeImageGet(
                                    $arOffer['DETAIL_PICTURE'],
                                    $arThumbParams,
                                    BX_RESIZE_BX_RESIZE_IMAGE_PROPORTIONAL_ALT
                                );
                                ?>
                                
                                <div class="image">
                                    <img src="<? echo $arThumb['src'] ?>" alt="<? echo $arOffer['NAME'] ?>">
                                </div>
                                
                                <div class="footer">
                                    <p class="price"><? print number_format($arOffer['PRICES']['BASE']['VALUE'], 0, '', ' '); ?></p>
                                    
                                    <div
                                        class="status <? if ($arOffer['CATALOG_QUANTITY'] < 1): ?> not-<? endif; ?>availiable"
                                        data-quantity="<? echo $arOffer['CATALOG_QUANTITY']; ?>">
                                        <? if ($arOffer['CATALOG_QUANTITY'] < 1) { ?>
                                            <p class="absent">Под заказ</p>
                                        <? } else { ?>
                                            <p class="present" title="<? print $arOffer['CATALOG_QUANTITY'] . ' шт.' ?>">Вналичии</p>
                                        <? } ?>
                                    </div>
                                
                                </div>
                            
                            
                            </article>
                            
                            <a href="#<? echo $arOffer['ID']; ?>" class="link"></a>
                        
                        </div>
                    
                    </div>
                
                <? endforeach; ?>
            </div>
        
        </section>
    
    <? endif; ?>
    
    <? $arResult['DISPLAY_PROPERTIES']['IMAGE']; ?>
    
    <? if (count($arResult['DISPLAY_PROPERTIES']['IMAGE']['FILE_VALUE']) > 0) : ?>
        
        <section id="images" class="grid">
            
            <h2 class="center-on-mobiles">Фотографии</h2>
            
            <div class="slider">
                
                <? foreach ($arResult['DISPLAY_PROPERTIES']['IMAGE']['FILE_VALUE'] as $num => $image) : ?>
                    
                    <?
                    
                    $thumb = CFile::ResizeImageGet(
                        $image,
                        array(
                            'width' => '120',
                            'height' => '120'
                        ),
                        BX_RESIZE_BX_RESIZE_IMAGE_EXACT
                    );
                    
                    $big = CFile::ResizeImageGet(
                        $image,
                        array(
                            'width' => '960',
                            'height' => '720'
                        ),
                        BX_RESIZE_BX_RESIZE_IMAGE_PROPORTIONAL
                    );
                    ?>
                    
                    <div class="item image">
                        <div class="wrapper">
                            <a href="<? echo $big['src']; ?>" class="colorbox" rel="images"
                               title="<? $arResult['NAME']; ?>">
                                <img src="<? echo $thumb['src'] ?>"
                                     alt="<? echo $arResult['NAME'] . ' фото #' . $num; ?>">
                            </a>
                        </div>
                    </div>
                
                <? endforeach; ?>
            
            </div>
        
        </section>
    
    <? endif; ?>
    
    <section id="description" class="grid">
        
        <div class="whole unit">
            
            <? if (count($arResult['DISPLAY_PROPERTIES']['VIDEO']['VALUE']) > 0) : ?>
                <aside id="video" class="pull-right">
                    <h3>Видео</h3>
                    <ul class="list">
                        <? foreach ($arResult['DISPLAY_PROPERTIES']['VIDEO']['VALUE'] as $video) : ?>
                            <li class="item">
                                <div class="wrapper">
                                    <a class="youtube colorbox"
                                       href="http://youtube.com/embed/<? echo $video; ?>?wmode=transparent">
                                        <img src="https://img.youtube.com/vi/<? echo $video; ?>/mqdefault.jpg"
                                             data-small="https://img.youtube.com/vi/<? echo $video; ?>/lqdefault.jpg"/>
                                    </a>
                                </div>
                            </li>
                        <? endforeach; ?>
                    </ul>
                </aside>
            <? endif; ?>
            
            <? if (!empty($arResult['DETAIL_TEXT'])) : ?>
                <section id="product-description">
                    <h2>Описание</h2>
                    <? echo $arResult['DETAIL_TEXT']; ?>
                </section>
            <? endif; ?>
        
        </div>
    
    </section>
    
    <? if (!empty($arResult['GIFTS'])) : ?>
        
        <div class="hidden">
            <aside id="gifts">
    
                <div class="caption align-center">
                    <h2 class="title">Ваш подарок</h2>
                    <p>Заказывая это автокресло вы получаете подарок!</p>
                </div>
    
                <ul id="gifts-list" class="gifts list">
                    <? foreach ($arResult['GIFTS'] as $arGift) : ?>
                        <li class="gift item">
    
                            <? $APPLICATION->IncludeComponent(
                                "bitrix:catalog.element",
                                "gift",
                                Array(
                                    "ACTION_VARIABLE" => "action",
                                    "ADD_DETAIL_TO_SLIDER" => "N",
                                    "ADD_ELEMENT_CHAIN" => "Y",
                                    "ADD_PROPERTIES_TO_BASKET" => "N",
                                    "ADD_SECTIONS_CHAIN" => "Y",
                                    "ADD_TO_BASKET_ACTION" => array("BUY"),
                                    "BACKGROUND_IMAGE" => "-",
                                    "BASKET_URL" => "",
                                    "BRAND_USE" => "N",
                                    "BROWSER_TITLE" => "-",
                                    "CACHE_GROUPS" => "Y",
                                    "CACHE_TIME" => "36000000",
                                    "CACHE_TYPE" => "A",
                                    "CHECK_SECTION_ID_VARIABLE" => "N",
                                    "CONVERT_CURRENCY" => "N",
                                    "DETAIL_PICTURE_MODE" => "IMG",
                                    "DETAIL_URL" => "",
                                    "DISABLE_INIT_JS_IN_COMPONENT" => "Y",
                                    "DISPLAY_COMPARE" => "N",
                                    "DISPLAY_NAME" => "Y",
                                    "DISPLAY_PREVIEW_TEXT_MODE" => "E",
                                    "ELEMENT_CODE" => "",
                                    "ELEMENT_ID" => $arGift['ID'],
                                    "HIDE_NOT_AVAILABLE" => "N",
                                    "IBLOCK_ID" => $arGift['IBLOCK_ID'],
                                    "IBLOCK_TYPE" => "catalog",
                                    "LINK_ELEMENTS_URL" => "link.php?PARENT_ELEMENT_ID=#ELEMENT_ID#",
                                    "LINK_IBLOCK_ID" => "",
                                    "LINK_IBLOCK_TYPE" => "",
                                    "LINK_PROPERTY_SID" => "",
                                    "MESSAGE_404" => "",
                                    "MESS_BTN_ADD_TO_BASKET" => "В корзину",
                                    "MESS_BTN_BUY" => "Купить",
                                    "MESS_BTN_SUBSCRIBE" => "Подписаться",
                                    "MESS_NOT_AVAILABLE" => "Нет в наличии",
                                    "META_DESCRIPTION" => "-",
                                    "META_KEYWORDS" => "-",
                                    "OFFERS_LIMIT" => "0",
                                    "PARTIAL_PRODUCT_PROPERTIES" => "N",
                                    "PRICE_CODE" => array(),
                                    "PRICE_VAT_INCLUDE" => "Y",
                                    "PRICE_VAT_SHOW_VALUE" => "N",
                                    "PRODUCT_ID_VARIABLE" => "id",
                                    "PRODUCT_PROPERTIES" => array(),
                                    "PRODUCT_PROPS_VARIABLE" => "prop",
                                    "PRODUCT_QUANTITY_VARIABLE" => "",
                                    "PRODUCT_SUBSCRIPTION" => "N",
                                    "PROPERTY_CODE" => array("", ""),
                                    "SECTION_CODE" => "",
                                    "SECTION_ID" => $_REQUEST["SECTION_ID"],
                                    "SECTION_ID_VARIABLE" => "SECTION_ID",
                                    "SECTION_URL" => "",
                                    "SEF_MODE" => "N",
                                    "SET_BROWSER_TITLE" => "Y",
                                    "SET_CANONICAL_URL" => "N",
                                    "SET_LAST_MODIFIED" => "N",
                                    "SET_META_DESCRIPTION" => "Y",
                                    "SET_META_KEYWORDS" => "Y",
                                    "SET_STATUS_404" => "N",
                                    "SET_TITLE" => "Y",
                                    "SET_VIEWED_IN_COMPONENT" => "N",
                                    "SHOW_404" => "N",
                                    "SHOW_CLOSE_POPUP" => "N",
                                    "SHOW_DEACTIVATED" => "N",
                                    "SHOW_DISCOUNT_PERCENT" => "N",
                                    "SHOW_MAX_QUANTITY" => "N",
                                    "SHOW_OLD_PRICE" => "N",
                                    "SHOW_PRICE_COUNT" => "1",
                                    "TEMPLATE_THEME" => "blue",
                                    "USE_COMMENTS" => "N",
                                    "USE_ELEMENT_COUNTER" => "Y",
                                    "USE_GIFTS_DETAIL" => "Y",
                                    "USE_GIFTS_MAIN_PR_SECTION_LIST" => "Y",
                                    "USE_MAIN_ELEMENT_SECTION" => "N",
                                    "USE_PRICE_COUNT" => "N",
                                    "USE_PRODUCT_QUANTITY" => "N",
                                    "USE_VOTE_RATING" => "N"
                                )
                            ); ?>

                        </li>
                    <? endforeach; ?>
                </ul>
            </aside>
        </div>
    
    <? endif; ?>

</article>

<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

?>

<div id="catalog-smart-filter"
     class="grid smart-filter <? if ($arParams['AJAX_MODE'] == 'Y') echo 'smart-filter-ajax'; ?>">
    
    <div class="whole unit">
        
        <? if ($arParams['FILTER_TITLE']) : ?>
            <h2 class="title"><? echo $arParams['FILTER_TITLE']; ?></h2>
        <? endif; ?>
        
        <form name="<? echo $arParams['FILTER_NAME'] . "_form" ?>"
              action="<? echo $arResult["FORM_ACTION"] ?>"
              method="post">
            
            <? foreach ($arResult["HIDDEN"] as $arItem): ?>
                <input type="hidden"
                       name="<? echo $arItem["CONTROL_NAME"] ?>"
                       id="<? echo $arItem["CONTROL_ID"] ?>"
                       value="<? echo $arItem["HTML_VALUE"] ?>"/>
            <? endforeach; ?>
            
            <? foreach ($arResult["ITEMS"] as $key => $arItem) {
                
                //prices
                
                $key = $arItem["ENCODED_ID"];
                if (isset($arItem["PRICE"])):
    
                    if ($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0) continue;
                    $precision = 2;
    
                    ?>
    
                    <div id="price-<? echo strtolower($arItem['CODE']); ?>" class="prices">
        
                        <h3>Цена</h3>
        
                        <div id="price-slider">
        
                        </div>
        
                        <div class="hidden">
            
                            <input
                                class="price min"
                                type="text"
                                name="price_min"
                                id="<? echo $arItem["VALUES"]["MIN"]["CONTROL_ID"] ?>"
                                value="<? echo $arItem["VALUES"]["MIN"]["HTML_VALUE"] ?>"
                            />
            
                            <input
                                type="text"
                                class="price max"
                                name="price_max"
                                id="<? echo $arItem["VALUES"]["MAX"]["CONTROL_ID"] ?>"
                                value="<? echo $arItem["VALUES"]["MAX"]["HTML_VALUE"] ?>"
                            />
        
                        </div>
    
                    </div>

                <?endif;
            } ?>
            
            <? // custom options ?>
            
            <div id="custom-options" class="checkboxes grid" data-property-id="0">
                
                <ul class="checkboxes list">
                    
                    <li class="item">
                        <input class="checkbox"
                               type="checkbox"
                               value="Y"
                               name="crush-tested"
                               id="value-crush-tested"
                               <? if ($_REQUEST['crush-tested'] == 'Y') : ?>checked="checked"<? endif; ?>
                        />
                        <label class="checkbox-label"
                               for="value-crush-tested">
                            Проверено краш-тестом
                        </label>
                    </li>
                    
                    <li class="item">
                        <input class="checkbox"
                               type="checkbox"
                               value="Y"
                               name="avaliable"
                               id="value-avaliable"
                               <? if ($_REQUEST['avaliable'] == 'Y') : ?>checked="checked"<? endif; ?>
                        />
                        <label class="checkbox-label"
                               for="value-avaliable">
                            В наличии
                        </label>
                    </li>
                
                </ul>
            
            </div>
            
            <?

            //not prices

            foreach ($arResult["ITEMS"] as $key => $arItem) {

                if (empty($arItem["VALUES"]) || isset($arItem["PRICE"])) continue;
                if ($arItem["DISPLAY_TYPE"] == "A" &&
                    ($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0)
                ) continue;
//                if (!in_array($arItem['CODE'], $arParams['USED_FILTER_FIELDS'])) continue;

                $arCur = current($arItem["VALUES"]);
    
                switch ($arItem["DISPLAY_TYPE"]) {
    
                    case "A"://NUMBERS_WITH_SLIDER
                        ?>
    
                        <div id="<? echo $arItem['HTML_ID']; ?>" class="numbers-w-slider grid"
                             data-property-id="<? echo $arItem['ID']; ?>">
        
                            <h3><? echo $arItem['NAME'] ?></h3>
        
                            <input
                                class="number min"
                                type="text"
                                name="<? echo $arItem["VALUES"]["MIN"]["CONTROL_NAME"] ?>"
                                id="<? echo $arItem["VALUES"]["MIN"]["CONTROL_ID"] ?>"
                                value="<? echo $arItem["VALUES"]["MIN"]["HTML_VALUE"] ?>"
                            />
        
                            <input
                                class="number max"
                                type="text"
                                name="<? echo $arItem["VALUES"]["MAX"]["CONTROL_NAME"] ?>"
                                id="<? echo $arItem["VALUES"]["MAX"]["CONTROL_ID"] ?>"
                                value="<? echo $arItem["VALUES"]["MAX"]["HTML_VALUE"] ?>"
                            />
                        </div>
    
                        <?
                        break;
                    case "B"://NUMBERS
                        ?>
    
                        <div id="<? echo $arItem['HTML_ID']; ?>" class="numbers grid"
                             data-property-id="<? echo $arItem['ID']; ?>">
        
                            <h3><? echo $arItem['NAME'] ?></h3>
        
                            <input
                                class="number min"
                                type="text"
                                name="<? echo $arItem["VALUES"]["MIN"]["CONTROL_NAME"] ?>"
                                id="<? echo $arItem["VALUES"]["MIN"]["CONTROL_ID"] ?>"
                                value="<? echo $arItem["VALUES"]["MIN"]["HTML_VALUE"] ?>"
                            />
        
                            <input
                                class="number max"
                                type="text"
                                name="<? echo $arItem["VALUES"]["MAX"]["CONTROL_NAME"] ?>"
                                id="<? echo $arItem["VALUES"]["MAX"]["CONTROL_ID"] ?>"
                                value="<? echo $arItem["VALUES"]["MAX"]["HTML_VALUE"] ?>"
                            />
    
                        </div>
    
                        <?
                        break;
                    case "G"://CHECKBOXES_WITH_PICTURES
                        ?>
    
                        <div id="<? echo $arItem['HTML_ID']; ?>" class="checkboxes-w-pix grid"
                             data-property-id="<? echo $arItem['ID']; ?>">
        
                            <h3><? echo $arItem['NAME'] ?></h3>
        
                            <? foreach ($arItem["VALUES"] as $val => $ar): ?>
                                <div class="checkbox">
                                    <input
                                        type="checkbox"
                                        name="<?= $ar["CONTROL_NAME"] ?>"
                                        id="<?= $ar["CONTROL_ID"] ?>"
                                        value="<?= $ar["HTML_VALUE"] ?>"
                                        <? echo $ar["CHECKED"] ? 'checked="checked"' : '' ?>
                                    />
                                    <?
                                    $class = "checkbox-label";
                                    if ($ar["CHECKED"]) $class .= " active";
                                    if ($ar["DISABLED"]) $class .= " disabled";
                                    ?>
                                    <label for="<?= $ar["CONTROL_ID"] ?>" class="<? echo $class; ?>">
                                        <? if (isset($ar["FILE"]) && !empty($ar["FILE"]["SRC"])): ?>
                                            <span style="background-image:url('<?= $ar["FILE"]["SRC"] ?>');"></span>
                                        <? endif ?>
                                    </label>
                                </div>
                            <? endforeach ?>
                        </div>
    
                        <?
                        break;
                    case "H"://CHECKBOXES_WITH_PICTURES_AND_LABELS
                        ?>
    
                        <div id="<? echo $arItem['HTML_ID']; ?>" class="checkboxes-w-pix-n-labels grid"
                             data-property-id="<? echo $arItem['ID']; ?>">
        
                            <h3><? echo $arItem['NAME'] ?></h3>
        
                            <? foreach ($arItem["VALUES"] as $val => $ar): ?>
                                <input
                                    type="checkbox"
                                    name="<?= $ar["CONTROL_NAME"] ?>"
                                    id="<?= $ar["CONTROL_ID"] ?>"
                                    value="<?= $ar["HTML_VALUE"] ?>"
                                    <? echo $ar["CHECKED"] ? 'checked="checked"' : '' ?>
                                />
                                <?
                                $class = "checkbox-label";
                                if ($ar["CHECKED"]) $class .= " active";
                                if ($ar["DISABLED"]) $class .= " disabled";
                                ?>
                                <label for="<?= $ar["CONTROL_ID"] ?>" class="<? echo $class; ?>">
                                    <? if (isset($ar["FILE"]) && !empty($ar["FILE"]["SRC"])): ?>
                                        <span style="background-image:url('<?= $ar["FILE"]["SRC"] ?>');"></span>
                                    <? endif ?>
                                    <span title="<?= $ar["VALUE"]; ?>">
                                        <?= $ar["VALUE"]; ?>
                                        <? if ($arParams["DISPLAY_ELEMENT_COUNT"] !== "N" && isset($ar["ELEMENT_COUNT"])): ?>
                                            (<? echo $ar["ELEMENT_COUNT"]; ?>)
                                        <? endif; ?>
                                    </span>
                                </label>
                            <? endforeach ?>
                        </div>
    
                        <?
                        break;
                    case "P"://DROPDOWN
                        ?>
    
                        <div id="<? echo $arItem['HTML_ID']; ?>" class="dropdown grid"
                             data-property-id="<? echo $arItem['ID']; ?>">
        
                            <h3><? echo $arItem['NAME'] ?></h3>
        
                            <select name="<? echo $arItem["CODE"]; ?>">
                                <option value="0">Все</option>
            
                                <? foreach ($arItem["VALUES"] as $val => $ar):
                                    $class = "dropdown-label";
                                    if ($ar["CHECKED"]) $class .= " selected";
                                    if ($ar["DISABLED"]) $class .= " disabled";
                                    ?>
                                    <option value="<?= $ar["CONTROL_ID"] ?>"
                                            class="<? echo $class; ?>" <? echo $ar["CHECKED"] ? 'selected="selected"' : '' ?>>
                                        <?= $ar["VALUE"] ?>
                                    </option>
                                <? endforeach ?>
                            </select>
    
                        </div>
    
                        <?
                        break;
                    case "R"://DROPDOWN_WITH_PICTURES_AND_LABELS
                        ?>
    
                        <div id="<? echo $arItem['HTML_ID']; ?>" class="dropdown-w-pix-n-labels grid"
                             data-property-id="<? echo $arItem['ID']; ?>">
        
                            <h3><? echo $arItem['NAME'] ?></h3>
        
                            <?
                            $checkedItemExist = false;
                            foreach ($arItem["VALUES"] as $val => $ar):
                                if ($ar["CHECKED"]) { ?>
                                    <? if (isset($ar["FILE"]) && !empty($ar["FILE"]["SRC"])): ?>
                                        <span style="background-image:url('<?= $ar["FILE"]["SRC"] ?>');"></span>
                                    <? endif ?>
                                    <span>
                                        <?= $ar["VALUE"] ?>
                                    </span>
                                    <?
                                    $checkedItemExist = true;
                                }
                            endforeach;
                            ?>
        
                            <input
                                style="display: none"
                                type="radio"
                                name="<?= $arCur["CONTROL_NAME_ALT"] ?>"
                                id="<? echo "all_" . $arCur["CONTROL_ID"] ?>"
                                value=""
                            />
        
                            <? foreach ($arItem["VALUES"] as $val => $ar): ?>
            
                                <input
                                    style="display: none"
                                    type="radio"
                                    name="<?= $ar["CONTROL_NAME_ALT"] ?>"
                                    id="<?= $ar["CONTROL_ID"] ?>"
                                    value="<?= $ar["HTML_VALUE_ALT"] ?>"
                                    <? echo $ar["CHECKED"] ? 'checked="checked"' : '' ?>
                                />
        
                            <? endforeach ?>
        
                            <ul class="list">
            
                                <li class="item">
                                    <label for="<?= "all_" . $arCur["CONTROL_ID"] ?>">
                                        Все
                                    </label>
                                </li>
            
                                <?
                                foreach ($arItem["VALUES"] as $val => $ar):
    
                                    $class = "item";
    
                                    if ($ar["CHECKED"])
                                        $class .= " selected";
                                    if ($ar["DISABLED"])
                                        $class .= " disabled";
                                    ?>
                                    <li class="<? echo $class; ?>">
                                        <label for="<?= $ar["CONTROL_ID"] ?>">
                                            <? if (isset($ar["FILE"]) && !empty($ar["FILE"]["SRC"])): ?>
                                                <span style="background-image:url('<?= $ar["FILE"]["SRC"] ?>');"></span>
                                            <? endif ?>
                                            <span>
                                                <?= $ar["VALUE"] ?>
                                            </span>
                                        </label>
                                    </li>
                                <? endforeach ?>
                            </ul>
    
                        </div>
    
                        <?
                        break;
                    case "K"://RADIO_BUTTONS
                        ?>
    
                        <div id="<? echo $arItem['HTML_ID']; ?>" class="radios grid"
                             data-property-id="<? echo $arItem['ID']; ?>">
        
                            <h3><? echo $arItem['NAME'] ?></h3>
        
                            <ul class="radios list">
                                <? foreach ($arItem["VALUES"] as $val => $ar): ?>
                                    <li class="item">
                                        <input
                                            class="radio"
                                            type="radio"
                                            value="<? echo $ar["HTML_VALUE_ALT"] ?>"
                                            name="<? echo $ar["CONTROL_NAME_ALT"] ?>"
                                            id="<? echo $ar["CONTROL_ID"] ?>"
                                            <? echo $ar["CHECKED"] ? 'checked="checked"' : '' ?>
                                        />
                                        <label for="<? echo $ar["CONTROL_ID"] ?>">
                                        <span title="<?= $ar["VALUE"]; ?>">
                                            <?= $ar["VALUE"]; ?>
                                            <? if ($arParams["DISPLAY_ELEMENT_COUNT"] !== "N" && isset($ar["ELEMENT_COUNT"])): ?>
                                                (<? echo $ar["ELEMENT_COUNT"]; ?>)
                                            <? endif; ?>
                                        </span>
                                        </label>
                                    </li>
                                <? endforeach; ?>
                            </ul>
    
                        </div>
                        <?
                        break;
                    case "U"://CALENDAR
                        ?>
    
                        <div id="<? echo strtolower($arItem['CODE']) ?>" class="calendar grid"
                             data-property-id="<? echo $arItem['ID']; ?>">
        
                            <h3><? echo $arItem['NAME'] ?></h3>
        
                            <? $APPLICATION->IncludeComponent(
                                'bitrix:main.calendar',
                                '',
                                array(
                                    'FORM_NAME' => $filterName . "_form",
                                    'SHOW_INPUT' => 'Y',
                                    'INPUT_ADDITIONAL_ATTR' => 'class="calendar" placeholder="' . FormatDate("SHORT", $arItem["VALUES"]["MIN"]["VALUE"]) . '"',
                                    'INPUT_NAME' => $arItem["VALUES"]["MIN"]["CONTROL_NAME"],
                                    'INPUT_VALUE' => $arItem["VALUES"]["MIN"]["HTML_VALUE"],
                                    'SHOW_TIME' => 'N',
                                    'HIDE_TIMEBAR' => 'Y',
                                ),
                                null,
                                array('HIDE_ICONS' => 'Y')
                            ); ?>
        
                            <? $APPLICATION->IncludeComponent(
                                'bitrix:main.calendar',
                                '',
                                array(
                                    'FORM_NAME' => $filterName . "_form",
                                    'SHOW_INPUT' => 'Y',
                                    'INPUT_ADDITIONAL_ATTR' => 'class="calendar" placeholder="' . FormatDate("SHORT", $arItem["VALUES"]["MAX"]["VALUE"]) . '"',
                                    'INPUT_NAME' => $arItem["VALUES"]["MAX"]["CONTROL_NAME"],
                                    'INPUT_VALUE' => $arItem["VALUES"]["MAX"]["HTML_VALUE"],
                                    'SHOW_TIME' => 'N',
                                    'HIDE_TIMEBAR' => 'Y',
                                ),
                                null,
                                array('HIDE_ICONS' => 'Y')
                            ); ?>
                        </div>
    
                        <?
                        break;
                    default://CHECKBOXES
                        ?>
                        <? if (!$arItem['PRICE']) : ?>

                            <? if ($arItem['CODE'] == 'SECTION') { ?>

                                <div id="<? echo $arItem['HTML_ID']; ?>" class="checkboxes grid"
                                     data-property-id="<? echo $arItem['ID']; ?>">
                                    <h3><? echo $arItem['NAME'] ?></h3>
                                    <ul class="checkboxes list">
                                        <? foreach ($arItem["VALUES"] as $val => $ar): ?>
                                            <li class="item">
                                                <input
                                                    class="checkbox"
                                                    type="checkbox"
                                                    value="<?= $val ?>"
                                                    name="SECTIONS[]"
                                                    id="<? echo $ar["CONTROL_ID"] ?>"
                                                />
                                                <label
                                                    class="<? echo $ar["DISABLED"] ? 'disabled' : '' ?>"
                                                    for="<? echo $ar["CONTROL_ID"] ?>">
                                                    <? echo $ar['VALUE']; ?>
                                                </label>
                                            </li>
                                        <? endforeach; ?>
                                </div>

                            <? } else { ?>

                                <div id="<? echo $arItem['HTML_ID']; ?>" class="checkboxes grid"
                                     data-property-id="<? echo $arItem['ID']; ?>">
                                    <h3><? echo $arItem['NAME'] ?></h3>
                                    <ul class="checkboxes list">
                                        <? foreach ($arItem["VALUES"] as $val => $ar): ?>
                                            <li class="item">
                                                <input
                                                    class="checkbox"
                                                    type="checkbox"
                                                    value="<?= $val ?>"
                                                    name="properties[<?= str_replace('arrFilter_', '', $ar['CONTROL_NAME_ALT']) ?>][]"
                                                    id="<? echo $ar["CONTROL_ID"] ?>"
                                                />
                                                <label
                                                    class="<? echo $ar["DISABLED"] ? 'disabled' : '' ?>"
                                                    for="<? echo $ar["CONTROL_ID"] ?>">
                                                    <? echo $ar['VALUE']; ?>
                                                </label>
                                            </li>
                                        <? endforeach; ?>
                                </div>

                            <? } ?>

                    <? endif; ?>

                    <? } ?>
            <? } ?>
    
            <? /*
            <? if ($arParams['AJAX_MODE'] !== 'Y') : ?>
                
                <div class="actions grid">
                    
                    <input
                        type="submit"
                        id="set_filter"
                        name="set_filter"
                        value="Показать"
                    />
                    
                    <? if ($_REQUEST['set_filter']) : ?>
                        <input
                            type="submit"
                            id="del_filter"
                            name="del_filter"
                            value="Отмена"
                        />
                    <? endif; ?>
                
                </div>
            
            <? endif; ?>
            */ ?>
        
        </form>
    
    </div>

</div>
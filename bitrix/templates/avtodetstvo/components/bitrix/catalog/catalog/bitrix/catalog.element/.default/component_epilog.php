<?php
/**
 * Created by PhpStorm.
 * User: mpa3b
 * Date: 01.02.2016
 * Time: 20:31
 */

$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/lib/mousewheel/jquery.mousewheel.js');

$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/lib/lazyload/jquery.lazyload.js');

$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/lib/colorbox/jquery.colorbox.js');
$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/lib/colorbox/colorbox.css');

$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/lib/slick/slick.js');
$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/lib/slick/slick.css');

if (empty($_GET['id'])) :
    
    $arResult['OFFERS'][0]['SELECTED'] = 'Y';

else :
    
    foreach ($arResult['OFFERS'] as $key => $arOffer) :
        if ($arOffer['ID'] == $_GET['id']) :
            $arResult['OFFERS'][$key]['SELECTED'] = 'Y';
        endif;
    endforeach;

endif;
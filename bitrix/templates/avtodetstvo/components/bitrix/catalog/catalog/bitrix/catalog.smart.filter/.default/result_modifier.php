<?php
/**
 * Created by PhpStorm.
 * User: mpa3b
 * Date: 09.02.2016
 * Time: 16:54
 */

foreach ($arResult['ITEMS'] as $id => $item) {
    $arResult['ITEMS'][$id]['HTML_ID'] = 'property-' . str_replace('_', '-', strtolower($item['CODE']));
}

$rsSections = CIBlockSection::GetList(
    array(),
    array(
        'IBLOCK_ID' => $arParams['IBLOCK_ID']
    ),
    false
);

$arResult['ITEMS']['SECTION']['ID'] = 0;
$arResult['ITEMS']['SECTION']['HTML_ID'] = 'property-section';
$arResult['ITEMS']['SECTION']['IBLOCK_ID'] = $arParams['IBLOCK_ID'];
$arResult['ITEMS']['SECTION']['CODE'] = 'SECTION';
$arResult['ITEMS']['SECTION']['NAME'] = 'Группы';
$arResult['ITEMS']['SECTION']['PROPERTY_TYPE'] = 'L';
$arResult['ITEMS']['SECTION']['DISPLAY_TYPE'] = 'F';
$arResult['ITEMS']['SECTION']['DISPLAY_EXPANDED'] = 'Y';
$arResult['ITEMS']['SECTION']['FILTER_HINT'] = 'возраст и вес ребёнка';

while ($arSection = $rsSections->GetNext()) :
    $arResult['ITEMS']['SECTION']['VALUES'][$arSection['ID']]['CONTROL_ID'] = $arSection['ID'];
    $arResult['ITEMS']['SECTION']['VALUES'][$arSection['ID']]['CONTROL_NAME'] = 'SECTION_' . $arSection['ID'];
    $arResult['ITEMS']['SECTION']['VALUES'][$arSection['ID']]['VALUE'] = $arSection['NAME'];
    $arResult['ITEMS']['SECTION']['VALUES'][$arSection['ID']]['UPPER'] = strtoupper($arSection['NAME']);
    $arResult['ITEMS']['SECTION']['VALUES'][$arSection['ID']]['HTML_VALUE'] = 'Y';
    $arResult['ITEMS']['SECTION']['VALUES'][$arSection['ID']]['URL_ID'] = strtolower($arSection['NAME']);
endwhile;

?>
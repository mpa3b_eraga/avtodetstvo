/**
 * Created by mpa3b on 01.02.2016.
 */

$(
    function () {
        $('img', 'article', '#colors').lazyload();
    }
);

$(document).on(
    'ready',
    function () {

        $('a.colorbox', '.gift-marker').colorbox(
            {
                href: '#gifts',
                inline: true,
                maxWidth: '65%'
            }
        );


        $('a.colorbox').colorbox(
            {
                width: '85%'
            }
        );

        $('a.colorbox.youtube').colorbox(
            {
                iframe: true,
                height: '50%',
                width: '65%',
                className: 'video'
            }
        );

        $('.slider', '#images').slick(
            {
                slidesToShow: 5,
                slidesToScroll: 1,
                dots: false,
                prevArrow: '<a class="slick-arrow slick-prev" href="#"></a>',
                nextArrow: '<a class="slick-arrow slick-next" href="#"></a>',
                responsive: [
                    {
                        breakpoint: 720,
                        settings: {
                            slidesToShow: 3
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 2
                        }
                    },
                    {
                        breakpoint: 320,
                        settings: {
                            slidesToShow: 1
                        }
                    }
                ]
            }
        );

        $('.slider', '#images').on(
            'mousewheel',
            function (event) {

                event.preventDefault();

                if (event.deltaY > 0) {
                    $('.slider', '#images').slick('slickNext');
                }
                if (event.deltaY < 0) {
                    $('.slider', '#images').slick('slickPrev');
                }

            }
        );
    
        // доставка сегодня
    
        $('a[href="#delivery-info"]', '#buy').on(
            'click',
            function (event) {
    
                event.preventDefault();
    
                $('#delivery-info').fadeIn();

            }
        );
    
        $('a.close', '#delivery-info').on(
            'click',
            function (event) {

                event.preventDefault();
                $('#delivery-info').fadeOut();

            }
        );
    
        // нашли дешевле?

        $('a[href="#found-cheaper"]', '#buy').on(
            'click',
            function (event) {

                event.preventDefault();
    
                $('#found-cheaper').fadeIn();

            }
        );

        $('a.close', '#found-cheaper').on(
            'click',
            function (event) {

                event.preventDefault();
                $('#found-cheaper').fadeOut();

            }
        );

    }
);

// обработка GET-парметров

function getParameterByName(name, url) {

    if (!url) url = window.location.href;

    name = name.replace(/[\[\]]/g, "\\$&");

    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)", "i"),
        results = regex.exec(url);

    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));

}

// выбор расцветки

$(document).on(
    'ready',
    function () {

        var offerID = getParameterByName('id');

        if (offerID > 0) {
            var offer = $('#offer-' + offerID);
        }
        else {
            var offer = $('.offer.selected');
        }

        var offerData = $('article', offer);

        offer.addClass('selected');

        $('img', '#main-image').attr('src', offerData.data('preview-image'));

        $('.add.button', '#buy').attr('href', offerData.data('add-url'));
        $('#buy .price .value').text(offerData.data('price'));

        var mainImage = $('img', '#main-image'),
            bigImage  = $('a.colorbox', '#main-image'),
            price     = $('span.value', '#purchase'),
            article   = $('span.value', '#article'),
            color     = $('span.value', '#color'),
            status    = $('div.status', '#purchase'),
            buy       = $('.buy.button', '#purchase'),
            add       = $('.add.button', '#purchase'),
            delivery  = $('#additional-options', '#purchase');

        $('a.link', '.offer', '#colors').on(
            'click',
            function (event) {

                event.preventDefault();

                $('.offer', '#colors').removeClass('selected');
                $(this).closest('.offer').addClass('selected');

                var offer = $(this).parent().find('.product');

                mainImage.attr('src', offer.data('detail-image'));
                bigImage.attr('href', offer.data('detail-image'));
                price.text(offer.data('price'));
                article.text(offer.data('article'));
                color.text(offer.data('color-name'));
                buy.attr('href', offer.data('buy-url'));
                add.attr('href', offer.data('add-url'));
                delivery.data('status', offer.data('status'));

                if (offer.data('quantity') > 0) {
                    status
                        .removeClass('not-available')
                        .addClass('available');
                }
                else {
                    status
                        .removeClass('available')
                        .addClass('not-available');
                }

            }
        );

        $('a.link', offer).trigger('click');

    }
);

// покупка в один клик

$(document).on(
    'ready',
    function () {

        var form = $('#one-click-buy');

        $('input[name*="phone"]', form).mask('+7 999 999-99-99');

        $('a.one-click-buy', '.item').on(
            'click',
            function (event) {

                event.preventDefault();

                var item    = $('article.catalog.item'),
                    element = $('article', '.selected.item', '#colors', item);
    
                var name      = $('#element-title').text(),
                    id        = element.data('id'),
                    image     = element.data('preview-image'),
                    colorname = element.data('color-name'),
                    price     = element.data('price');

                $('input[name="one-click-buy-form-product-id"]', form).val(id);
                $('img.image', form).attr('src', image);
                $('h3.name', form).text(name);
                $('.value', '.colorname', form).text(colorname);
                $('p.price', form).text(price);

                $.colorbox(
                    {
                        inline: true,
                        href: form
                    }
                );

            }
        );

    }
);

// TODO: Проверить добавление в корзину
// TODO: Доделать покупку в один клик


$(document).on(
    'ready',
    function() {

        var rating = $('#rating').data('rating') * 20;

        $('.background-red', '#rating').width(rating + 'px');
});
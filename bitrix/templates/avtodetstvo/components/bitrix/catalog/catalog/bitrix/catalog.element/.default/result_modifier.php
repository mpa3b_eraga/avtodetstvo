<?php

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

// свойства товара

$rsSection = CIBlockSection::GetList(
    array(),
    array(
        'IBLOCK_ID' => $arResult['IBLOCK_ID'],
        'ID' => $arResult['IBLOCK_SECTION_ID']
    ),
    false,
    array(
        'UF_SECTION_SUBTITLE',
        'UF_AGE_GROUP'
    )
);

$arSection = $rsSection->Fetch();
$arResult['SECTION_SUBTITLE'] = $arSection['UF_SECTION_SUBTITLE'];

// Картинка по-умолчанию если пустая

if (empty($arResult['DETAIL_PICTURE'])) {
    $arResult['PREVIEW_PICTURE'] = $arResult['DISPLAY_PROPERTIES']['IMAGE']['FILE_VALUE'][0];
    $arResult['DETAIL_PICTURE'] = $arResult['DISPLAY_PROPERTIES']['IMAGE']['FILE_VALUE'][0];
}

// количество товара из ТП

$quantity = 0;

foreach ($arResult['OFFERS'] as $i => $arOffer) :

    // TODO: Костыль для посчёта количества на складе
    $filter = [
        'PRODUCT_ID' => $arOffer['ID']
    ];
    if ($_SESSION['SUBDOMAIN'] != 'DEFAULT')
        $filter['UF_SUBDOMAIN'] = $_SESSION['SUBDOMAIN'];
    $res = CCatalogStore::GetList([], $filter, false, false, ['PRODUCT_AMOUNT']);
    $count = 0;
    while ($row = $res->Fetch()) {
        $count += $row['PRODUCT_AMOUNT'];
    }
    $arResult['OFFERS'][$i]['CATALOG_QUANTITY'] = $count;

    if (!isset($arResult['MIN_PRICE']['VALUE'])) {
        $arResult['MIN_PRICE']['VALUE'] = $arOffer['MIN_PRICE']['VALUE'];
    }
    
    $quantity = $quantity + $arOffer['CATALOG_QUANTITY'];
    
    if ($arOffer['MIN_PRICE']['VALUE'] < $arResult['MIN_PRICE']['VALUE']) {
        $arResult['MIN_PRICE']['VALUE'] = $arOffer['MIN_PRICE']['VALUE'];
    }

endforeach;

$arResult['CATALOG_QUANTITY'] = $quantity;

if (empty($_GET['id'])) :
    
    $default = false;
    
    foreach ($arResult['OFFERS'] as $offer) {
        if (!$default && $offer['CATALOG_QUANTITY'] > 0) {
            $default = $offer;
        }
    }
    if (!$default) {
        $default = $arResult['OFFERS'][0];
    }
    
    $arResult['SELECTED_ID'] = $default['ID'];
    $arResult['CATALOG_QUANTITY'] = $default['CATALOG_QUANTITY'];
    $arResult['DETAIL_PICTURE'] = $default['DETAIL_PICTURE'];
    $arResult['ARTICLE'] = $default['DISPLAY_PROPERTIES']['CML2_ARTICLE']['VALUE'];
    $arResult['COLOR'] = $default['DISPLAY_PROPERTIES']['COLOR_NAME']['VALUE'];
    $arResult['MIN_PRICE']['VALUE'] = $default['MIN_PRICE']['VALUE'];
    $arResult['ADD_URL'] = '?action=ADD2BASKET&id=' . $default['ID'];

else :
    
    $arResult['SELECTED_ID'] = $_GET['id'];
    $arResult['ARTICLE'] = '';
    $arResult['COLOR'] = '';

    foreach ($arResult['OFFERS'] as $key => $arOffer) {
        if ($arOffer['ID'] == $_GET['id']) {
            $arResult['OFFERS'][$key]['SELECTED'] = 'Y';
        }
    }

endif;

// Подарок

if (!empty($arResult['PROPERTIES']['GIFT']['VALUE'])) {
    
    foreach ($arResult['PROPERTIES']['GIFT']['VALUE'] as $arGift) :
    
        $rsGifts = CIBlockElement::GetByID($arGift);
        $arGift = $rsGifts->Fetch();
        $arResult['GIFTS'][] = $arGift;

    endforeach;
    
}

// TODO: Количество для ТП на складе при поддомене и без

// TODO: Срок ожидания доставки из свойств бренда

// Доставка по бренду

$rsBrands = CIBlockElement::GetList(
    array(),
    array(
        'IBLOCK_ID' => $arResult['PROPERTIES']['BRAND']['LINK_IBLOCK_ID'],
        'ID' => $arResult['PROPERTIES']['BRAND']['VALUE']
    )
);

$arBrand = $rsBrands->Fetch();

$rsValues = CIBlockElement::GetProperty(
    $arResult['PROPERTIES']['BRAND']['LINK_IBLOCK_ID'],
    $arResult['PROPERTIES']['BRAND']['VALUE']
);

while ($arValue = $rsValues->GetNext()) :
    $arResult['BRAND'][$arValue['CODE']] = $arValue;
endwhile;
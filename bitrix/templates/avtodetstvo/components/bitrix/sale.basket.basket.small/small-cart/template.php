<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<div id="<? echo $arParams['COMPONENT_TEMPLATE'] ?>" refreshed="0">
    <?

    $count = 0;

    foreach ($arResult['ITEMS'] as $item) :
        $count = $count + $item['QUANTITY'];
    endforeach;

    $message = 'товаров';
    if (substr($count, strlen($count) - 2, 1) != '1' || $count < 10) {
        switch (substr($count, strlen($count) - 1, 1)) {
            case '1':
                $message = 'товар';
                break;
            case '2':
            case '3':
            case '4':
                $message = 'товара';
                break;
        }
    }

    ?>
    <span>
        <a href="<? echo $arParams["PATH_TO_BASKET"]; ?>">
            <span class="cart-title">В корзине</span>&nbsp;
            <strong id="<? echo $arParams['COMPONENT_TEMPLATE'] ?>-value"><? echo $count; ?></strong>
            &nbsp;<span class="suffix"><? echo $message; ?></span>
        </a>
    </span>
    <div class="floating">
        <a href="<? echo $arParams["PATH_TO_BASKET"]; ?>">
            <span class="cart-title">В корзине</span>&nbsp;
            <strong id="<? echo $arParams['COMPONENT_TEMPLATE'] ?>-floating-value"><? echo $count; ?></strong>
            &nbsp;<span class="suffix"><? echo $message; ?></span>
        </a>
    </div>
</div>
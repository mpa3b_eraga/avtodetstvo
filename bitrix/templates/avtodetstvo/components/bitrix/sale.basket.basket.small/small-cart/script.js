/**
 * Created by mpa3b on 21.03.2016.
 */

$(function () {
    $(window).on('scrollstop', function () {
        if ($(window).scrollTop() > $('#header').height()) {
            $('.floating', '#small-cart').animate({
                top: '2em'
            });
        } else {
            $('.floating', '#small-cart').animate({
                top: '-4em'
            });
        }
    });
});

function ajaxSendCartQuantity(id, action, quantity, callback) {
    $.ajax({
        url: '/ajax/cart.php',
        method: 'POST',
        dataType: 'json',
        data: {
            action: action,
            id: id,
            quantity: quantity,
            time: Date.now()
        },
        success: function (data) {
            if (callback)
                callback(data);
        }
    });
}

function refreshSmallCart(data) {
    $('strong', '#small-cart').text(data.count);
    $('span.suffix', '#small-cart').text(data.suffix);
}
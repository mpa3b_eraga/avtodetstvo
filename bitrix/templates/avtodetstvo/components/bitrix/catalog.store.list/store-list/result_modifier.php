<?php
/**
 * Created by PhpStorm.
 * User: mpa3b
 * Date: 29.03.2016
 * Time: 11:56
 */

$arStores = $arResult['STORES'];
unset($arResult['STORES']);

foreach ($arStores as $arStore) :
    
    $code = GetUserField('CAT_STORE', $arStore['ID'], 'UF_SUBDOMAIN');
    
    $arResult['STORES'][$code] = $arStore;
    $arResult['STORES'][$code]['UF_SUBDOMAIN'] = $code;
    
    if ($code == $_SESSION['SUBDOMAIN']) {
        $_SESSION['REGION_ID'] = $arStore['ID'];
    }
    else {
        $_SESSION['REGION_ID'] = 0;
    }

endforeach;

?>
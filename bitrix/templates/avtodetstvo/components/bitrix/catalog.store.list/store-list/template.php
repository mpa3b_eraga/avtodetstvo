<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
?>

<? if (is_array($arResult["STORES"]) && !empty($arResult["STORES"])): ?>
    
    <div id="region-select" data-subdomain="<? echo $arParams['SUBDOMAIN']; ?>" class="hide-on-mobiles">
        
        <? if ($arParams['SUBDOMAIN'] == 'default') : ?>
            <a href="#" id="region-select-trigger">Другие регионы</a>
        <? else : ?>
            <a href="#"
               id="region-select-trigger"><? echo $arResult['STORES'][$arParams['SUBDOMAIN']]['ADDRESS']; ?></a>
        <? endif; ?>
        
        <div class="wrapper">
            <ul id="regions-list" class="dropdown" style="display:none;">
                <li id="store-all">
                    <a href="http://<? echo $_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']; ?>"
                       title="Другие регионы">Другие регионы</a>
                </li>
                <? foreach ($arResult["STORES"] as $code => $arStore): ?>
                    <li id="store-<? echo $code; ?>">
                        <a href="http://<? echo $arStore["UF_SUBDOMAIN"] . '.' . $_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']; ?>"
                           title="<? echo $arStore["TITLE"] ?>"><? echo $arStore["ADDRESS"] ?></a>
                    </li>
                <? endforeach; ?>
            </ul>
        </div>
    
    </div>
    
    <div class="only-on-mobiles">
        <form action="/">
            <span class="label">Выберите регион:</span>
            <select id="region-select" name="region-select">
                <? if ($_SESSION['REGION_ID'] == 0) : ?>
                    <option value="default" selected="selected" disabled="disabled">другой регион</option>
                <? endif; ?>
                <? foreach ($arResult["STORES"] as $code => $arStore): ?>
                    <? if ($code !== $arParams['SUBDOMAIN']) : ?>
                        <option
                            value="http://<? echo $arStore["UF_SUBDOMAIN"] . '.' . $_SERVER['SERVER_NAME']; ?>"><? echo $arStore["ADDRESS"] ?></option>
                    <? endif; ?>
                <? endforeach; ?>
            </select>
        </form>
    </div>

<? endif; ?>
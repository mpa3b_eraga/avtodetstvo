/**
 * Created by mpa3b on 29.03.2016.
 */

(function ($) {

    $(document).on(
        'ready',
        function () {

            $('a#region-select-trigger', '#region-select').on(
                'click',
                function (event) {

                    event.preventDefault();
                    var dropdown = $(this).closest('#region-select').find('ul.dropdown');

                    dropdown.slideDown(
                        function () {
                            $(document).on(
                                'click',
                                function () {
                                    dropdown.slideUp(
                                        function () {
                                            $(document).unbind('click');
                                        }
                                    );
                                }
                            );
                        }
                    );

                }
            );

            $('select#region-select', '#top-panel-city-select').on(
                'change',
                function () {
                    window.location = $(this).find("option:selected").val();
                }
            );

        }
    );

})(jQuery);
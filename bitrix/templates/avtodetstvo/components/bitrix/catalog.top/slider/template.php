<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */


if (!empty($arResult['ITEMS'])) { ?>
    
    <aside id="<? echo $arParams['COMPONENT_TEMPLATE'] ?>" class="wrap">
        
        <div class="grid secondary-items">
            
            <? if ($arParams['BLOCK_TITLE']) : ?>
                <h2 class="center-on-mobiles"><? echo $arParams['BLOCK_TITLE'] ?></h2>
            <? endif; ?>
            
            <div class="items slider catalog section">
                
                <? foreach ($arResult['ITEMS'] as $num => $arItem) { ?>
                    
                    <article
                        class="item <? if (!empty($arItem['DISCOUNTS'])) echo ' discount'; ?>"
                        data-id="<? echo $arItem['ID']; ?>"
                        data-image="<? echo $arItem['PREVIEW_PICTURE']['SRC']; ?>"
                        data-name="<? echo $arItem['NAME']; ?>"
                        data-price="<? echo $arItem['MIN_PRICE']['VALUE']; ?>"
                        data-brand="<? echo strip_tags($arItem["DISPLAY_PROPERTIES"]['BRAND']['DISPLAY_VALUE']); ?>"
                        data-colorname=""
                        data-quantity="<? echo $arItem['QUANTITY']; ?>"
                    >
                        
                        <div <? if ($USER->IsAdmin()) : ?> id="<? echo $this->GetEditAreaId($arItem['ID']); ?>" <? endif; ?>
                            class="wrapper">
                            
                            <? if (!empty($arItem['DISCOUNTS'])) : ?>
                                <span class="discount-marker">Акция!</span>
                            <? endif; ?>
                            
                            <?
                            $this->AddEditAction(
                                $arItem['ID'],
                                $arItem['EDIT_LINK'],
                                CIBlock::GetArrayByID(
                                    $arItem["IBLOCK_ID"],
                                    "ELEMENT_EDIT"
                                )
                            );
                            $this->AddDeleteAction(
                                $arItem['ID'],
                                $arItem['DELETE_LINK'],
                                CIBlock::GetArrayByID(
                                    $arItem["IBLOCK_ID"],
                                    "ELEMENT_DELETE"
                                ),
                                array(
                                    "CONFIRM" => GetMessage(
                                        'CT_BCT_ELEMENT_DELETE_CONFIRM'
                                    )
                                )
                            );
                            ?>
                            
                            <div class="header">
                                <h3 class="name">
                                    <a href="<? echo $arItem["DETAIL_PAGE_URL"]; ?>"><? echo $arItem["NAME"] ?></a>
                                </h3>
                                <? if (!empty($arItem["DISPLAY_PROPERTIES"]['BRAND']['DISPLAY_VALUE'])) : ?>
                                    <h4 class="brand">
                                        <? echo $arItem["DISPLAY_PROPERTIES"]['BRAND']['DISPLAY_VALUE']; ?>
                                    </h4>
                                <? endif; ?>
                            </div>
                            
                            <?
                            $thumbParams = array(
                                'width' => '140',
                                'height' => '140'
                            );
                            ?>
                            
                            <? if ($arItem['OFFERS']): ?>
                                
                                <div class="image <? if (count($arItem['OFFERS']) > 1) echo 'slider'; ?>">
                                    
                                    <? foreach ($arItem['OFFERS'] as $index => $arOffer) : ?>
                                        
                                        <?
                                        $thumb = CFile::ResizeImageGet(
                                            $arOffer['DETAIL_PICTURE'],
                                            $thumbParams,
                                            BX_RESIZE_BX_RESIZE_IMAGE_PROPORTIONAL_ALT
                                        );
                                        ?>
                                        
                                        <div class="slide"
                                             data-index="<? echo $index; ?>"
                                             data-id="<? echo $arOffer['ID'] ?>"
                                             data-image="<? echo $thumb['src'] ?>"
                                             data-name="<? echo $arOffer['NAME'] ?>"
                                             data-colorname="<? echo $arOffer['PROPERTIES']['COLOR_NAME']['VALUE']; ?>"
                                             data-price="<? echo $arOffer['PRICES'][$arParams['PRICE_CODE'][0]]['VALUE']; ?>"
                                             data-quantity="<? echo $arOffer['QUANTITY']; ?>"
                                        >
                                            
                                            <a href="<? echo $arOffer['DETAIL_PAGE_URL']; ?>">
                                                <img src="<? echo $thumb['src'] ?>"
                                                     alt="<? echo $arOffer['NAME'] ?>"
                                                     title="<? echo $arOffer['NAME'] ?>"/>
                                            </a>
                                        
                                        </div>
                                    
                                    <? endforeach; ?>
                                
                                </div>
                            
                            <? else : ?>
                                
                                <?
                                $thumb = CFile::ResizeImageGet(
                                    $arItem['DETAIL_PICTURE'],
                                    $thumbParams,
                                    BX_RESIZE_BX_RESIZE_IMAGE_PROPORTIONAL
                                );
                                ?>
                                
                                <div class="image grid">
                                    <a class="image" href="<? echo $arItem["DETAIL_PAGE_URL"] ?>">
                                        <img src="<? echo $thumb["src"] ?>"
                                             alt="<? echo $arItem["NAME"] ?>"
                                             title="<? echo $arItem["NAME"] ?>"/>
                                    </a>
                                </div>
                            
                            <? endif; ?>
                            
                            <? if (is_array($arItem['SECTION']) && !empty($arItem['PROPERTIES']['TIER']['VALUE'])) : ?>
                                
                                <ul class="align-center properties list">
                                    
                                    <? if (is_array($arItem['SECTION'])) : ?>
                                        <li class="weight-group">
                                        <span
                                            class="label lowercase">Группа</span>
                                            <strong
                                                class="value"><? echo $arItem['SECTION']['NAME']; ?></strong>
                                        </li>
                                        <li class="age-group">
                                        <span
                                            class="label lowercase">Возраст</span>
                                            <strong
                                                class="value"><? echo $arItem['SECTION']['UF_AGE_GROUP']['VALUE']; ?></strong>
                                        </li>
                                    <? endif; ?>
                                    
                                    <li class="tier">
                                        <span
                                            class="label lowercase"><? echo $arItem['PROPERTIES']['TIER']['NAME']; ?></span>
                                        <ul class="list">
                                            <? foreach ($arItem['PROPERTIES']['TIER']['VALUE_ENUM'] as $property) : ?>
                                                <li class="value">
                                                    <? echo $property; ?>
                                                </li>
                                            <? endforeach; ?>
                                        </ul>
                                    </li>
                                
                                </ul>
                            
                            <? endif; ?>
                            
                            <? if ($arItem['OFFERS']) : ?>
                                
                                <div class="colors grid">
                                    <p>
                                        <a href="#" class="show-colors">Расцветки</a>
                                        <a href="#" class="hide-colors hidden">Скрыть</a>
    
                                        <? if ($arItem['CATALOG_QUANTITY'] > 0) : ?>
                                            <a href="#" class="one-click-buy active">Купить в один клик</a>
                                        <? else : ?>
                                            <a href="#" class="one-click-buy">Заказать</a>
                                        <? endif; ?>

                                    </p>
                                    <p class="colors-list hidden">
                                        <? foreach ($arItem['OFFERS'] as $index => $arOffer) : ?>
                                            <a class="color"
                                               style="background-color: <? echo '#' . $arOffer['PROPERTIES']['COLOR']['VALUE']; ?>"
                                               data-index="<? echo $index; ?>"
                                               href="#"
                                            ></a>
                                        <? endforeach; ?>
                                    </p>
                                </div>
                            
                            <? endif; ?>
                            
                            <div class="buy grid">
                                
                                <div class="half unit price">
                                    <p class="price">
                                        <? if ($arItem['MIN_PRICE']['DISCOUNT_VALUE'] < $arItem['MIN_PRICE']['VALUE']): ?>
                                            <span class="value"><?= $arItem['MIN_PRICE']['DISCOUNT_VALUE'] ?> </span>
                                            <span class="old value"><s><?= $arItem['MIN_PRICE']['VALUE'] ?></s> </span>
                                        <? else: ?>
                                            <span class="value"><?= $arItem['MIN_PRICE']['VALUE'] ?> </span>
                                        <? endif ?>
                                    </p>
                                </div>
                                
                                <div class="half unit action align-right">
                                    
                                    <? //TODO: Переделать на покупку! ?>
                                    
                                    <? if ($arItem["CAN_BUY"]): ?>
                                        <noindex>
                                            <a class="buy button"
                                               href="<? echo $arItem["BUY_URL"] ?>"
                                               rel="nofollow">Купить</a>
                                        </noindex>
                                    <? elseif (count($arItem["PRICES"]) > 0): ?>
                                        <a href="<? echo $arItem['DETAIL_PAGE_URL']; ?>"
                                           class="details button">Подробнее</a>
                                    <? else : ?>
                                        
                                        <a href="<? echo $arItem['DETAIL_PAGE_URL']; ?>"
                                           class="details button">Подробнее</a>
                                    
                                    <? endif; ?>
                                
                                </div>
                            
                            </div>
                        
                        </div>
                    
                    </article>
                    
                    <? /*

                <? if (($num + 1) % $arParams['LINE_ELEMENT_COUNT'] == 0) { ?>
            </div>
            <div class="grid">
                <? } ?>

                */ ?>
                
                <? } ?>
            
            </div>
        
        </div>
    
    </aside>

<? } ?>
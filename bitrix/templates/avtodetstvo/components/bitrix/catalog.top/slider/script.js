/**
 * Created by mpa3b on 01.02.2016.
 */

(function ($) {

    $(document).on(
        'ready',
        function () {
    
            $('.slider.catalog.section', '.secondary-items').slick(
                {
                    slidesToShow: 4,
                    slidesToScroll: 1,
                    dots: false,
                    buttons: true,
                    prevArrow: '<a class="slick-arrow slick-prev" href="#"></a>',
                    nextArrow: '<a class="slick-arrow slick-next" href="#"></a>',
                    useCSS: true,
                    infinite: false,
                    responsive: [
                        {
                            breakpoint: 720,
                            settings: {
                                slidesToShow: 3
                            }
                        },
                        {
                            breakpoint: 480,
                            settings: {
                                slidesToShow: 2
                            }
                        },
                        {
                            breakpoint: 320,
                            settings: {
                                slidesToShow: 1
                            }
                        }
                    ]
                }
            );

            $('.slider', '.secondary-items').on(
                'mousewheel',
                function (event) {

                    event.preventDefault();

                    if (event.deltaY > 0) {

                        $(this).slick('slickNext');
                    }
                    if (event.deltaY < 0) {

                        $(this).slick('slickPrev');
                    }

                }
            );

        }
    );

})(jQuery);
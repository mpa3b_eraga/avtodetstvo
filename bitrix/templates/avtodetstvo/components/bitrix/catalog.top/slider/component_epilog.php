<?php
/**
 * Created by PhpStorm.
 * User: mpa3b
 * Date: 31.03.2016
 * Time: 13:52
 */

$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/lib/mousewheel/jquery.mousewheel.js');

$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/lib/slick/slick.js');
$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/lib/slick/slick.css');
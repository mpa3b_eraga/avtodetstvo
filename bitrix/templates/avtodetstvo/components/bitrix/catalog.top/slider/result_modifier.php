<?php

// список складов

$rsStores = CCatalogStore::GetList(
    array(),
    array(
        'ACTIVE' => 'Y',
    )
);

while ($arStore = $rsStores->GetNext()) :
    $arStoresList[$arStore['ID']] = $arStore;
    $arStoresList[$arStore['ID']]['UF_SUBDOMAIN'] = GetUserField('CAT_STORE', $arStore['ID'], 'UF_SUBDOMAIN');;
endwhile;

// обработка товаров перед отображением

foreach ($arResult['ITEMS'] as $key => $arItem) :
    
    // обработка скидок
    
    $arDiscounts = CCatalogDiscount::GetDiscountByProduct(
        $arItem['ID'],
        $USER->GetUserGroupArray()
    );
    
    if (!empty($arDiscounts)) :
        foreach ($arDiscounts as $discount) :
            $arResult['ITEMS'][$key]['DISCOUNTS'][] = $discount;
        endforeach;
    endif;
    
    // обработка количеств
    // количество суммируется из количества достуных из всех торговых предложения
    // кроме того выводися каждому ТП количетсов на складе по региону
    
    $arItemStore = array();
    
    $rsStore = CCatalogStoreProduct::GetList(
        array(),
        array(
            'PRODUCT_ID' => $arItem['ID']
        )
    );
    
    $quantity = 0;
    
    while ($arStore = $rsStore->GetNext()) :
        
        $subdomain = GetUserField('CAT_STORE', $arStore['STORE_ID'], 'UF_SUBDOMAIN');
        
        if ($subdomain) :
            $arResult['ITEMS'][$key]['STORE'][strtoupper($subdomain)] = $arStore['AMOUNT'];
        endif;
        
        $quantity = $quantity + $arStore['AMOUNT'];
    
    endwhile;
    
    $arResult['ITEMS'][$key]['CATALOG_QUANTITY'] = $quantity;
    
    // обработка цен
    // ставится минимальная из всех доступных
    
    $arMinPrice = array(
        'VALUE' => 0
    );
    
    foreach ($arItem['OFFERS'] as $arOffer) :
        
        if ($arMinPrice['VALUE'] < $arOffer['MIN_PRICE']) :
            $arMinPrice = $arOffer['MIN_PRICE'];
        endif;
    
    endforeach;
    
    $arResult['ITEMS'][$key]['MIN_PRICE'] = $arMinPrice;
    
    // обработка свойств
    
    // мне нужно получить:
    //
    // * название главного товара
    // * название бренда главного товара и ссылку
    // * название раздела и ссылку на раздел главного товара
    // * подназвание раздела и ссылку на раздел главного товара
    
    $rsMainProduct = CIBlockElement::GetByID($arItem['PROPERTIES']['CML2_LINK']['VALUE']);
    $arMainProduct = $rsMainProduct->Fetch();
    
    // Все свойства главного товара
    
    $rsMainProductProperties = CIBlockElement::GetProperty(
        $arMainProduct['IBLOCK_ID'],
        $arMainProduct['ID'],
        array(),
        array()
    );
    
    // TODO: Записывается в одну позицию, вторая перезаписывает первую, должны храниться обе
    
    while ($arMainProductProperty = $rsMainProductProperties->GetNext()) :
        
        foreach ($arMainProductProperty as $mainProductProperty) :
            $arResult['ITEMS'][$key]['PROPERTIES'][$arMainProductProperty['CODE']][] = $mainProductProperty;
        endforeach;
    
    endwhile;
    
    // Раздел
    
    $rsSection = CIBlockSection::GetList(
        array(),
        array(
            'IBLOCK_ID' => $arMainProduct['IBLOCK_ID'],
            'ID' => $arMainProduct['IBLOCK_SECTION_ID']
        ),
        false,
        array(
            'UF_SECTION_SUBTITLE',
            'UF_AGE_GROUP'
        )
    );
    
    $arSection = $rsSection->Fetch();
    
    $arResult['ITEMS'][$key]['SECTION'] = $arSection;
    
    $rsAgeGroup = CUserFieldEnum::GetList(
        array(),
        array(
            "ID" => $arSection["UF_AGE_GROUP"],
        )
    );
    
    $arAgeGroup = $rsAgeGroup->Fetch();
    
    $arResult['ITEMS'][$key]['SECTION']['UF_AGE_GROUP'] = $arAgeGroup;
    $arResult['ITEMS'][$key]['MAIN_PRODUCT'] = $arMainProduct;

endforeach;

?>
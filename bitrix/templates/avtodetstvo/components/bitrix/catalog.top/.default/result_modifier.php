<?php

// список складов

$rsStores = CCatalogStore::GetList(
    array(),
    array(
        'ACTIVE' => 'Y',
    )
);

while ($arStore = $rsStores->GetNext()) :
    $arStoresList[$arStore['ID']] = $arStore;
    $arStoresList[$arStore['ID']]['UF_SUBDOMAIN'] = GetUserField('CAT_STORE', $arStore['ID'], 'UF_SUBDOMAIN');;
endwhile;

global $currentPage,
       $totalPages,
       $countTotal;

// выделение нужных товаров
$items = $arResult['ITEMS'];
/*$sectionId = $_REQUEST['SECTIONS'];*/
$currentPage = (isset($_REQUEST['PAGEN_1'])) ? $_REQUEST['PAGEN_1'] : 1;
$totalPages = (int)ceil(count($arResult['ITEMS']) / $arParams['TOP_PER_PAGE']);
$countTotal = count($items);
$arResult['ITEMS'] = [];
for ($n = ($currentPage - 1) * $arParams['TOP_PER_PAGE']; $n < $currentPage * $arParams['TOP_PER_PAGE']; $n++) {
    if (isset($items[$n]))
        $arResult['ITEMS'][] = $items[$n];
}

// обработка товаров перед отображением
foreach ($arResult['ITEMS'] as $key => $arItem) :  
    
    // обработка скидок
    
    $arProduct = CCatalogProduct::GetByID(
        $arItem['ID']
    );
    
    $arDiscounts = CCatalogDiscount::GetDiscountByProduct(
        $arProduct['ID'],
        $USER->GetUserGroupArray()
    );
    
    if (!empty($arDiscounts)) :
        foreach ($arDiscounts as $discount) :
            $arResult['ITEMS'][$key]['DISCOUNTS'][] = $discount;
        endforeach;
    endif;
    
    // обработка цен
    // ставится минимальная из всех доступных
    
    $arMinPrice = array(
        'VALUE' => 0
    );
    
    foreach ($arItem['OFFERS'] as $arOffer) :
        
        if ($arMinPrice['VALUE'] < $arOffer['MIN_PRICE']) :
            $arMinPrice = $arOffer['MIN_PRICE'];
        endif;
    
    endforeach;
    
    $arResult['ITEMS'][$key]['MIN_PRICE'] = $arMinPrice;
    
    // обработка количеств
    // количество суммируется из количества достуных из всех торговых предложения
    // кроме того выводися каждому ТП количество на складе по региону
    
    $arItemStore = array();

    $itemQuantity = 0;
    foreach ($arItem['OFFERS'] as $index => $arOffer) :
        $rsStore = CCatalogStoreProduct::GetList(
            array(),
            array(
                'PRODUCT_ID' => $arOffer['ID'],
                'ACTIVE' => 'Y'
            )
        );
        while ($arStore = $rsStore->GetNext()) :
            if (isset($arStoresList[$arStore['STORE_ID']])) {
                $subdomain = GetUserField('CAT_STORE', $arStore['STORE_ID'], 'UF_SUBDOMAIN');
                $arResult['ITEMS'][$key]['OFFERS'][$index]['STORE'][strtoupper($subdomain)] = $arStore['AMOUNT'];
                if (getSubDomain() == 'default') {
                    $itemQuantity += $arStore['AMOUNT'];
                } elseif (getSubDomain() == $subdomain) {
                    $itemQuantity += $arStore['AMOUNT'];
                }
            }
        endwhile;
    endforeach;

    $arResult['ITEMS'][$key]['CATALOG_QUANTITY'] = $itemQuantity;
    
    // свойства товара
    
    
    $rsSection = CIBlockSection::GetList(
        array(),
        array(
            'IBLOCK_ID' => $arItem['IBLOCK_ID'],
            'ID' => $sectionID
        ),
        false,
        array(
            'UF_SECTION_SUBTITLE',
            'UF_AGE_GROUP'
        )
    );
    
    $arSection = $rsSection->Fetch();
    
    $arResult['ITEMS'][$key]['SECTION'] = $arSection;
    
    $rsAgeGroup = CUserFieldEnum::GetList(
        array(),
        array(
            "ID" => $arSection["UF_AGE_GROUP"],
        )
    );
    
    $arAgeGroup = $rsAgeGroup->Fetch();
    
    $arResult['ITEMS'][$key]['SECTION']['UF_AGE_GROUP'] = $arAgeGroup;

endforeach;

?>

<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

global  $USER,
        $currentPage,
        $totalPages,
        $countTotal,
        $arrFilter,
        $sectionID;

?>

<div id="catalog-section-top" class="catalog section grid">
    
    <? if ($countTotal) : ?>
        <aside id="section-elements-count" class="grid align-center">
            <div class="whole unit">
                <p>Найдено товаров: <strong id="total-count"><?= $countTotal ?></strong></p>
            </div>
        </aside>
    <? endif; ?>
    
    <?
    $thumbParams = array(
        'width' => '140',
        'height' => '140'
    );
    ?>
    
    <? if ($arParams['DISPlAY_TOP_PAGER']) echo $arResult["NAV_STRING"]; ?>
    
    <section id="section-contents" class="grid" data-page="<?= $currentPage ?>">
        
        <? if ($_REQUEST['AJAX'] == 'Y') {
            $APPLICATION->RestartBuffer();
            ob_start();
        } ?>
        
        <? foreach ($arResult['ITEMS'] as $num => $arItem) {
            
            $this->AddEditAction(
                $arItem['ID'],
                $arItem['EDIT_LINK'],
                CIBlock::GetArrayByID(
                    $arItem["IBLOCK_ID"],
                    "ELEMENT_EDIT"
                )
            );
            
            $this->AddDeleteAction(
                $arItem['ID'],
                $arItem['DELETE_LINK'],
                CIBlock::GetArrayByID(
                    $arItem["IBLOCK_ID"],
                    "ELEMENT_DELETE"
                ),
                array(
                    "CONFIRM" => GetMessage(
                        'CT_BCT_ELEMENT_DELETE_CONFIRM'
                    )
                )
            );
            
            ?>
            
            <article
                class="<? if (!empty($arItem['DISCOUNTS'])) echo 'discount '; ?>item"
                data-id="<? echo $arItem['ID']; ?>"
                data-name="<? echo $arItem['NAME']; ?>"
                data-brand="<? echo strip_tags($arItem["DISPLAY_PROPERTIES"]['BRAND']['DISPLAY_VALUE']); ?>"
                data-colorname="">
                
                <div <? if ($USER->IsAdmin()) : ?> id="<? echo $this->GetEditAreaId($arItem['ID']); ?>" <? endif; ?>
                    class="wrapper">
                    
                    <? if (!empty($arItem['DISCOUNTS'])) : ?>
                        <span class="discount-marker">Акция!</span>
                    <? endif; ?>
                    
                    <? if (!empty($arItem['PROPERTIES']['GIFT']['VALUE'])) : ?>
                        <span class="gift-marker" title="Подарок!"></span>
                    <? endif; ?>
                    
                    <div class="header">
                        <h2 class="name">
                            <a href="<? echo $arItem["DETAIL_PAGE_URL"]; ?>"><? echo $arItem["NAME"] ?></a>
                        </h2>
                        <? if (!empty($arItem["DISPLAY_PROPERTIES"]['BRAND']['DISPLAY_VALUE'])) : ?>
                            <h3 class="brand">
                                <? echo $arItem["DISPLAY_PROPERTIES"]['BRAND']['DISPLAY_VALUE']; ?>
                            </h3>
                        <? endif; ?>
                    </div>
                    
                    <? if ($arItem['OFFERS']): ?>
                        
                        <div class="image grid <? if (count($arItem['OFFERS']) > 1) echo ' slider'; ?>">
                            
                            <? foreach ($arItem['OFFERS'] as $index => $arOffer) : ?>
                                
                                <?
                                $thumb = CFile::ResizeImageGet(
                                    $arOffer['DETAIL_PICTURE'],
                                    $thumbParams,
                                    BX_RESIZE_BX_RESIZE_IMAGE_PROPORTIONAL_ALT
                                );
        
                                ?>
                                
                                <div
                                    class="slide <? if ($index > 0) echo 'hidden-before-load';
                                    else echo 'first'; ?>"
                                    data-index="<? echo $index; ?>"
                                    data-id="<? echo $arOffer['ID'] ?>"
                                    data-image="<? echo $thumb['src'] ?>"
                                    data-name="<? echo $arOffer['NAME'] ?>"
                                    data-colorname="<? echo $arOffer['PROPERTIES']['COLOR_NAME']['VALUE']; ?>"
                                    <? if ($arOffer['PRICES'][$arParams['PRICE_CODE'][0]]['DISCOUNT_VALUE'] < $arOffer['PRICES'][$arParams['PRICE_CODE'][0]]['VALUE']) : ?>
                                        data-price="<? echo $arOffer['PRICES'][$arParams['PRICE_CODE'][0]]['DISCOUNT_VALUE']; ?>"
                                    <? else: ?>
                                        data-price="<? echo $arOffer['PRICES'][$arParams['PRICE_CODE'][0]]['VALUE']; ?>"
                                    <? endif; ?>
                                    data-quantity="<? echo $arOffer['CATALOG_QUANTITY']; ?>"
                                >
                                    
                                    <a href="<? if (!empty($arOffer['DETAIL_PAGE_URL'])) : echo $arOffer['DETAIL_PAGE_URL'];
                                    else : echo $arItem['DETAIL_PAGE_URL'] . '?id=' . $arOffer['ID']; endif; ?>">
                                        <img src="<? echo $thumb['src'] ?>"
                                             alt="<? echo $arOffer['NAME'] ?>"
                                             title="<? echo $arOffer['NAME'] ?>"/>
                                    </a>
                                
                                </div>
                            
                            <? endforeach; ?>
                        
                        </div>
                    
                    <? else : ?>
                        
                        <?
                        $thumb = CFile::ResizeImageGet(
                            $arItem['DETAIL_PICTURE'],
                            $thumbParams,
                            BX_RESIZE_BX_RESIZE_IMAGE_PROPORTIONAL
                        );
                        ?>
                        <div class="image grid">
                            <a class="image" href="<? echo $arItem["DETAIL_PAGE_URL"] ?>">
                                <img src="<? echo $thumb["src"] ?>"
                                     alt="<? echo $arItem["NAME"] ?>"
                                     title="<? echo $arItem["NAME"] ?>"/>
                            </a>
                        </div>
                    <? endif; ?>
                    
                    <? if ($arItem['SECTION']['NAME'] && $arItem['DISPLAY_PROPERTIES']['TIER']['VALUE_ENUM']) : ?>
                        <ul class="properties list">
                            <li class="weight-group property">
                                <span class="property-label lowercase">Группа</span>
                                <span class="value"><? echo $arItem['SECTION']['NAME']; ?></span>
                            </li>
                            <li class="age-group property">
                                <span class="property-label lowercase">Возраст</span>
                                <span class="value"><? echo $arItem['SECTION']['UF_AGE_GROUP']['VALUE']; ?></span>
                            </li>
                            <li class="tier property">
                                <span class="property-label lowercase">Крепление</span>
                                <span class="value">
                                    <?
                                    $propertiesCount = count($arItem['DISPLAY_PROPERTIES']['TIER']['VALUE_ENUM']);
                                    foreach ($arItem['DISPLAY_PROPERTIES']['TIER']['VALUE_ENUM'] as $key => $tier) :
                                        ?>
                                        <strong
                                            class="<? echo $key; ?> <? if ($key + 1 == $propertiesCount) echo 'last'; ?>"><? echo $tier; ?></strong>
                                    <? endforeach; ?>
                                </span>
                            </li>
                        </ul>
                    <? endif; ?>
                    
                    <? if ($arItem['OFFERS']) : ?>
                        
                        <div class="colors grid">
                            <p>
                                <? if (count($arItem['OFFERS']) > 0) : ?>
                                    <a href="#" class="show-colors">Расцветки</a>
                                    <a href="#" class="hide-colors hidden">Скрыть</a>
                                <? endif; ?>
                                <a href="#" class="one-click-buy">Купить в один клик</a>
                            </p>
                            
                            <? if (count($arItem['OFFERS']) > 0) : ?>
                                <p class="colors-list hidden">
                                    <? foreach ($arItem['OFFERS'] as $index => $arOffer) : ?>
                                        <a class="color"
                                           style="background-color: <? echo '#' . $arOffer['PROPERTIES']['COLOR']['VALUE']; ?>"
                                           data-index="<? echo $index; ?>"
                                           href="#"></a>
                                    <? endforeach; ?>
                                </p>
                            <? endif; ?>
                        </div>
                    
                    <? endif; ?>
                    
                    <div class="buy grid no-stacking-on-mobiles">
                        <div class="half unit price">
                            <? if ($arItem['MIN_PRICE']['VALUE'] > $arItem['MIN_PRICE']['DISCOUNT_VALUE']) : ?>
                                <span id="item-<? echo $arItem['ID']; ?>-price-old"
                                      class="old value"><? echo number_format(floatval($arItem['MIN_PRICE']['VALUE']), 0, '', ' '); ?></span>
                                <br>
                                <span id="item-<? echo $arItem['ID']; ?>-price"
                                      class="value"><? echo number_format(floatval($arItem['MIN_PRICE']['DISCOUNT_VALUE']), 0, '', ' '); ?></span>
                            <? else : ?>
                                <span id="item-<? echo $arItem['ID']; ?>-price"
                                      class="value"><? echo number_format(floatval($arItem['MIN_PRICE']['VALUE']), 0, '', ' '); ?></span>
                            <? endif; ?>
                        </div>
                        <? if ($arItem["CAN_BUY"] || is_array($arItem['PRICES'])): ?>
                            <div class="half unit action align-right">
                                <a href="<? echo $arItem['DETAIL_PAGE_URL']; ?>"
                                   class="details button">Подробнее</a>
                            </div>
                        <? endif; ?>
                    </div>
                
                </div>
            
            </article>
        
        <? } ?>
        
        <? if ($_REQUEST['AJAX'] == 'Y') {
            $content = ob_get_clean();
            echo json_encode(
                [
                    'content' => $content,
                    'current_page' => $currentPage,
                    'next_page' => ($currentPage < $totalPages) ? true : false,
                    'pages' => $totalPages,
                    'total_count' => $countTotal,
                    'section_id' => $sectionID
                ]
            );
            die();
        } ?>
    
    </section>
    
    <? if ($arParams['DISPlAY_BOTTOM_PAGER']) echo $arResult["NAV_STRING"]; ?>
    
    <div class="grid">
        <div class="whole unit align-center">
            <button id="show-next-items" class="button<? if ($totalPages < 2) { ?> hidden<? } ?>">Следующие
                <strong><? echo $arParams['TOP_PER_PAGE']; ?></strong></button>
        </div>
    </div>

</div>
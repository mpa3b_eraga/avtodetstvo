<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

$arTemplateParameters = array(
    
    'TITLE' => array(
        "NAME" => 'Заголовок блока',
        "TYPE" => 'TEXT',
        "DEFAULT" => ''
    ),
    'TITLE_AS_LINK' => array(
        "NAME" => 'Заголовок блока как ссылка',
        "TYPE" => 'CHECKBOX',
        "DEFAULT" => 'N'
    )

);
/**
 * Created by mpa3b on 21.03.2016.
 */

(function ($) {
    
    $(document).on(
        'ready',
        function () {
            
            var cartURL = $('a', '#small-cart').attr('href'),
                item    = $('.item', '#best-offers');
            
            $('a.in-cart.button', item).on(
                'click',
                function (event) {
                    
                    event.preventDefault();
                    window.location.href = cartURL;
                    
                }
            );
            
            $('a.add.button', item).on(
                'click',
                function (event) {
                    
                    event.preventDefault();
                    
                    var button     = $(this),
                        buttonText = button.text();
                    
                    button
                        .attr('disabled', 'disabled')
                        .addClass('progress')
                        .text('Добавляю…');
                    
                    ajaxSendCartQuantity(
                        button.data('product-id'),
                        'add',
                        0,
                        function (data) {
                            
                            button
                                .removeClass('progress')
                                .removeAttr('disabled')
                                .addClass('in-cart')
                                .text('Оформить заказ')
                                .attr('href', cartURL)
                                .blur()
                                .unbind('click');
                            
                            refreshSmallCart(data);
                            
                        }
                    );
                    
                }
            );
            
            
        }
    );


// slider

    $(document).on(
        'ready',
        function () {

            $('.slider', '#best-offers').slick(
                {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    useCSS: true,
                    useTransform: true,
                    cssEase: 'ease',
                    prevArrow: '<a class="slick-arrow slick-prev" href="#"></a>',
                    nextArrow: '<a class="slick-arrow slick-next" href="#"></a>',
                    dots: true,
                    responsive: [
                        {
                            breakpoint: 840,
                            settings: {
                                arrows: false,
                                slidesToShow: 2
                            }

                        },
                        {
                            breakpoint: 568,
                            settings: {
                                arrows: false,
                                slidesToShow: 1,
                            }
                        }
                    ]
                }
            );

            $('.slider', '#best-offers').on(
                'mousewheel',
                function (event) {

                    event.preventDefault();

                    if (event.deltaY > 0) {
                        $(this).slick('slickPrev');
                    }
                    if (event.deltaY < 0) {
                        $(this).slick('slickNext');
                    }

                }
            );

        }
    );

})(jQuery);

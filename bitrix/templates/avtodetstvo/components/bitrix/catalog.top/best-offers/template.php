<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */


?>

<? if (!empty($arResult['ITEMS'])) { ?>
    
    <section id="<? echo $arParams['COMPONENT_TEMPLATE'] ?>">

        <? if ($arParams['TITLE']) : ?>
            <h2 class="title align-center"><? echo $arParams['TITLE']; ?></h2>
        <? endif; ?>

        <div class="slider grid items">

            <? foreach ($arResult['ITEMS'] as $arItem) : ?>

                <?
                $image = CFile::ResizeImageGet(
                    $arItem['DETAIL_PICTURE'],
                    array(
                        'width' => 180,
                        'height' => 240
                    ),
                    BX_RESIZE_IMAGE_PROPORTIONAL_ALT
                );
                ?>

                <article id="offer-<? echo $arItem['ID']; ?>"
                         class="one-third unit item best-offer <? if ($arItem['DISCOUNTS']) echo 'discount'; ?>">

                    <div data-id="<? echo $arItem['ID']; ?>"
                         data-image="<? echo $image['src']; ?>"
                         data-brand="<? echo $arItem['DISPLAY_PROPERTIES']['BRAND']['DISPLAY_VALUE']; ?>"
                         data-colorname="<? echo $arItem['PROPERTIES']['COLOR_NAME']['VALUE'] ?>"
                         data-name="<? echo $arItem['NAME']; ?>"
                         data-price="<? echo $arItem['PRICES']['BASE']['VALUE']; ?>"
                         data-quantity="<? echo $arItem['CATALOG_QUANTITY']; ?>"
                         class="wrapper slick-active slide">

                        <div class="header">
                            <h3 class="title">
                                <a href="<? echo $arItem['DETAIL_PAGE_URL']; ?>"><? echo $arItem['NAME']; ?></a>
                            </h3>
                            <? if (!empty($arItem['DISPLAY_PROPERTIES']['BRAND']['NAME'])) : ?>
                                <h4 class="brand">
                                    <? echo $arItem['DISPLAY_PROPERTIES']['BRAND']['NAME']; ?>
                                </h4>
                            <? endif; ?>
                        </div>

                        <div class="image">

                            <a href="<? echo $arItem['DETAIL_PAGE_URL']; ?>">
                                <img src="<? echo $image['src']; ?>" alt="<? echo $arItem['NAME']; ?>">
                            </a>

                        </div>

                        <div class="footer">

                            <? if (is_array($arItem['SECTION'])) : ?>
                                <ul class="align-center properties">

                                    <li class="weight-group">
                                        <span
                                            class="label lowercase">Группа</span>
                                        <strong
                                            class="value"><? echo $arItem['SECTION']['NAME']; ?></strong>
                                    </li>
                                    <li class="age-group">
                                        <span
                                            class="label lowercase">Возраст</span>
                                        <strong
                                            class="value"><? echo $arItem['SECTION']['UF_AGE_GROUP']['VALUE']; ?></strong>
                                    </li>
                                    <li class="tier">
                                        <span
                                            class="label lowercase"><? echo $arItem['PROPERTIES']['TIER']['NAME']; ?></span>
                                        <ul class="list">
                                            <li class="value"><? echo $arItem['PROPERTIES']['TIER']['VALUE_ENUM']; ?></li>
                                        </ul>
                                    </li>

                                </ul>
                            <? endif; ?>

                            <? if ($arItem['CAN_BUY']) : ?>
                                <div class="buy grid no-stacking-on-mobiles">
                                    <div class="price half unit">
                                        <p class="value"><? echo number_format($arItem['PRICES'][$arParams['PRICE_CODE'][0]]['VALUE'], 0, ',', '&nbsp;'); ?></p>
                                    </div>
                                    <div class="one-click half unit align-right">
                                        <? if ($arItem['CATALOG_QUANTITY'] > 0) : ?>
                                            <a href="#buy-one-click" class="one-click-buy available">Купить в один
                                                клик</a>
                                        <? else : ?>
                                            <a href="#order" class="one-click-buy">Заказать</a>
                                        <? endif; ?>
                                    </div>
                                </div>
                            <? endif; ?>

                            <a class="add button" href="<? echo $arItem['ADD_URL'] ?>"
                               data-product-id="<? echo $arItem['ID']; ?>">Положить в корзину</a>

                        </div>

                    </div>

                </article>
    
                <? // v($arItem); ?>

            <? endforeach; ?>

        </div>

    </section>
    
    <? // v($arResult); ?>

<? } ?>



/**
 * Created by mpa3b on 24.03.2016.
 */

function refreshCart(data) {
    
    $('#customer-cart .item').each(
        function () {
            var id = $(this).data('id');
            
            if (!data.cart[id] || data.cart[id] <= 0) {
                $(this).remove();
            }
            else {
                $('#customer-cart .item[data-id="' + id + '"]').find('input.item-quantity').val(data.cart[id]);
            }
        }
    );
    
    $('#customer-cart #cart-total strong').html(data.total);
    
    if (data.total == 0) {
        $('customer-cart-checkout').hide();
        $('customer-cart-to-best').show();
    }
    
    refreshSmallCart(data);
}

$(
    function () {
        
        // Изменение количества
        
        $('input.item-quantity', '.item', '#customer-cart').on(
            'input', function () {
                
                var number = parseInt($(this).val());
                
                if (number < 1) {
                    number = 1;
                    $(this).val(1);
                }
                if (number) {
                    ajaxSendCartQuantity(
                        $(this).data('id'), 'update', number, function (data) {
                            refreshCart(data);
                        }
                    );
                }
                
            }
        );
        
        // Увеличение на 1
        
        $('button.plus', '.item', '#customer-cart').on(
            'click', function (event) {
                
                event.preventDefault();
                
                var quantity = parseInt($(this).parent().find('input.item-quantity').val());
                
                ajaxSendCartQuantity(
                    $(this).data('id'), 'update', quantity + 1, function (data) {
                        refreshCart(data);
                    }
                );
                
                $(this).blur();
            }
        );
        
        // Уменьшение на 1
        
        $('button.minus', '.item', '#customer-cart').on(
            'click', function (event) {
                
                event.preventDefault();
                
                var quantity = parseInt($(this).parent().find('input.item-quantity').val());
                
                if (quantity > 1) {
                    ajaxSendCartQuantity(
                        $(this).data('id'),
                        'update',
                        quantity - 1,
                        function (data) {
                            refreshCart(data);
                        }
                    );
                }
                else {
                    var confirmed = confirm('Удалить товар из корзины?');
                    if (confirmed == true) {
                        $('button.remove', $(this).closest('.item')).trigger('click');
                    }
                }
                
                $(this).blur();
            }
        );
        
        // Удаление товара
        
        $('button.remove', $('.item', '#customer-cart')).on(
            'click', function (event) {
                
                event.preventDefault();
                
                ajaxSendCartQuantity(
                    $(this).data('id'),
                    'delete',
                    0,
                    function (data) {
                        refreshCart(data);
                    }
                );
                
                $(this).blur();
            }
        );
    }
);

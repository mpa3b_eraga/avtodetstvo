<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixBasketComponent $component */
?>

<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<? if ($arResult['WARNING_MESSAGE']) : ?>
    <div id="warning-message">
        <? foreach ($arResult['WARNING_MESSAGE'] as $message) : ?>
            <div class="message grid">
                <div class="whole unit">
                    <p class="error"><? echo $message ?></p>
                </div>
            </div>
        <? endforeach; ?>
    </div>
<? endif; ?>

<div id="customer-cart">
    
    <? if (count($arResult['GRID']['ROWS']) > 0) : ?>
        
        <form method="post" action="<?= POST_FORM_ACTION_URI ?>" name="customer-cart-form" id="customer-cart-form">
    
            <? if (count($arResult["ITEMS"]) > 0) : ?>
        
                <? foreach ($arResult["ITEMS"] as $id => $cartItemTypes) : ?>
                    
                    <section id="<? echo strtolower(str_replace('_', '-', $id)); ?>-items" class="item-type">
                        
                        <table class="items">
                            
                            <? if (count($cartItemTypes) > 0) : ?>
                                <tr>
                                    <th colspan="2"></th>
                                    <th>Количество</th>
                                    <th>Цена</th>
                                </tr>
                            <? endif; ?>
                            
                            <? foreach ($cartItemTypes as $num => $arItem) : ?>
    
                                <tr class="item <? if ($num == 0) echo ' first';
                                if ($arItem['PRICE'] > 0) echo ' gift'; ?>" data-id="<? echo $arItem['ID']; ?>">
                                    <td class="image">
                                        <div class="wrapper">
                                            <img src="<? echo $arItem['PREVIEW_PICTURE_SRC']; ?>"/>
                                        </div>
                                    </td>
                                    <td class="name">
                                        <h5 class="title">
                                            <? if (!empty($arItem['DETAIL_PAGE_URL'])) : ?>
                                                <a href="<? echo $arItem['DETAIL_PAGE_URL']; ?>">
                                                    <? echo $arItem['NAME']; ?>
                                                </a>
                                            <? else : ?>
                                                <? echo $arItem['NAME']; ?>
                                            <? endif; ?>
                                        </h5>
    
                                        <p class="status">
                                            <?
                                            if ($arItem['QUANTITY'] < 0) : echo 'В наличии';
                                            else : echo 'Под заказ';
                                            endif;
                                            ?>
                                        </p>

                                    </td>
                                    <td class="quantity align-center">
                                        <? if ($arItem['PRICE'] > 0) { ?>
                                            <button class="minus control" data-action="minus"
                                                    data-id="<?= $arItem["ID"] ?>"></button>
                                            <input type="text" class="item-quantity" data-id="<?= $arItem["ID"] ?>"
                                                   name="QUANTITY_INPUT_<?= $arItem["ID"] ?>"
                                                   value="<? echo $arItem['QUANTITY'] ?>">
                                            <button class="plus control" data-action="plus"
                                                    data-id="<?= $arItem["ID"] ?>"></button>
                                        <? }
                                        else { ?>
                                            <input type="text" class="item-quantity" data-id="<?= $arItem["ID"] ?>"
                                                   name="QUANTITY_INPUT_<?= $arItem["ID"] ?>"
                                                   value="<? echo $arItem['QUANTITY'] ?>"
                                                   disabled>
                                        <? } ?>
                                    </td>
                                    <td class="price align-center">
                                        <? if ($arItem['PRICE'] > 0) { ?>
                                            <? if ($arItem['PRICE'] !== $arItem['FULL_PRICE']) : ?>
                                                <p class="price value"><? echo $arItem['PRICE_FORMATED']; ?></p>
                                                <p class="old price value">
                                                    <s><? echo $arItem['FULL_PRICE_FORMATED']; ?></s>
                                                </p>
                                            <? else: ?>
                                                <p class="price value"><? echo $arItem['FULL_PRICE_FORMATED']; ?></p>
                                            <? endif; ?>
                                        <? }
                                        else { ?>
                                            Подарок
                                        <? } ?>
                                    </td>
                                    <td class="actions align-right">
                                        <? if ($arItem['PRICE'] > 0) { ?>
                                            <button class="remove control" data-action="remove"
                                                    data-id="<?= $arItem["ID"] ?>"></button>
                                        <? } ?>
                                    </td>
                                </tr>
                            
                            <? endforeach; ?>
                        
                        </table>
                    
                    </section>
                
                <? endforeach; ?>
            
            <? endif; ?>
        
        </form>
        
        <div id="cart-total" class="grid" class="total">
            
            <div class="whole unit align-center">
                <p class="price total">
                    Итого:
                    <strong>
                        <? echo number_format($arResult['allSum'], 0, ',', ' '); ?>
                    </strong>
                </p>
            </div>
        
        </div>
        
        <div class="grid actions">
            
            <div class="half unit">
                <a href="/catalog/" class="button white back">Обратно в магазин</a>
            </div>
            
            <div class="half unit align-right" id="cart-checkout">
                <a id="customer-cart-checkout" class="button" href="/cart/checkout/">Оформить заказ</a>
                <a id="customer-cart-to-best" href="/best/" class="button hidden">Лучшие предложения</a>
            </div>
        
        </div>
    
    <? else: ?>
        
        <h3 class="align-center">Нет товаров в корзине</h3>
        
        <div class="actions grid">
            <div class="half unit">
                <a id="customer-cart-to-catalog" href="/catalog/" class="button white back">Обратно в магазин</a>
            </div>
            <div class="half unit align-right">
                <a id="customer-cart-to-best" href="/best/" class="button">Лучшие предложения</a>
            </div>
        </div>
    
    <? endif; ?>

</div>

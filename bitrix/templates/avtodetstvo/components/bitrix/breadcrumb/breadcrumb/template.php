<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

global $APPLICATION;

if (empty($arResult)) return "";

$strReturn = '<div id="breadcrumb" class="grid">';

$strReturn .= '<nav class="whole unit">';
$strReturn .= '<div class="wrapper">';

$itemSize = count($arResult);

for ($index = 0; $index < $itemSize; $index++) {
    
    $title = htmlspecialcharsex($arResult[$index]["TITLE"]);
    $nextRef = ($index < $itemSize - 2 && $arResult[$index + 1]["LINK"] <> "" ? ' itemref="bx_breadcrumb_' . ($index + 1) . '"' : '');
    $child = ($index > 0 ? ' itemprop="child"' : '');
    
    if ($arResult[$index]["LINK"] <> "" && $index == 0) {
        $strReturn .= '
			<div id="breadcrumb-item-' . $index . '" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"' . $child . $nextRef . ' class="item first">
				<a href="' . $arResult[$index]["LINK"] . '" title="' . $title . '" itemprop="url">
					<span itemprop="title">' . $title . '</span>
				</a>
			</div>';
    }
    elseif ($arResult[$index]["LINK"] <> "" && $index !== $itemSize - 1) {
        $strReturn .= '
			<div id="breadcrumb-item-' . $index . '" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"' . $child . $nextRef . ' class="item">
				<a href="' . $arResult[$index]["LINK"] . '" title="' . $title . '" itemprop="url">
					<span itemprop="title">' . $title . '</span>
				</a>
			</div>';
    }
    else {
        $strReturn .= '
			<div id="breadcrumb-item-' . $index . '" class="item last">
			    <span>' . $title . '</span>
			</div>';
    }
}

$strReturn .= '</div>';
$strReturn .= '</div>';

return $strReturn;
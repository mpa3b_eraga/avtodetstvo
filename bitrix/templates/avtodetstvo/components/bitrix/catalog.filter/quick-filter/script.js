/**
 * Created by mpa3b on 04.02.2016.
 */

(function ($) {

    $(document).on(
        'ready',
        function () {

            $('select', '#quick-filter-property-111', '#quick-filter').chosen(
                {
                    width: '100%'
                }
            );

            $('.range-slider', '#quick-filter-price-base').ionRangeSlider(
                {
                    type: "double",
                    min: 0,
                    max: 100000,
                    from: 1,
                    to: 99999,
                    step: 1000,
                    onFinish: function (data) {
                        $('input[name="arrFilter_cf[5][LEFT]"]').val(data.from);
                        $('input[name="arrFilter_cf[5][RIGHT]"]').val(data.to);
                    }

                }
            );

        }
    );

})(jQuery);
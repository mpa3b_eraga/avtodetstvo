<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

?>

<div id="<? echo $arParams['COMPONENT_TEMPLATE'] ?>">
    
    <div class="wrapper">
        
        <? if ($arParams['FILTER_TITLE']) : ?>
            <h2 class="title align-center"><? echo $arParams['FILTER_TITLE'] ?></h2>
        <? endif; ?>
        
        <form name="<? echo $arResult["FILTER_NAME"] . "_form" ?>" action="<? echo $arParams["CATALOG_PATH"]; ?>"
              method="get">
            
            <?
            foreach ($arResult["ITEMS"] as $arItem):
                if (array_key_exists("HIDDEN", $arItem)):
                    echo $arItem["INPUT"];
                endif;
            endforeach;
            ?>
            
            <? foreach ($arResult["ITEMS"] as $key => $arItem): ?>
                
                <? $key = strtolower(str_replace('_', '-', $key)); ?>
                
                <div id="<? echo $arParams['COMPONENT_TEMPLATE'] . '-' . $key; ?>"
                     class="field <? echo strtolower($arItem['TYPE']); ?>-field">
                    
                    <? /*
                    <? if($arItem['NAME']) : ?>
                        <h3><? echo $arItem['NAME']?></h3>
                    <? endif; ?>
                    */ ?>
                    
                    <?
                    if (!array_key_exists("HIDDEN", $arItem)) :
    
                        switch ($arItem['TYPE']) {
        
                            case 'SELECT':
                                ?>
                                <select name="<? echo $arItem['INPUT_NAME'] ?>" id="<? echo $key; ?>">
                                    <? foreach ($arItem['LIST'] as $value => $option) : ?>
                                        <option value="<? echo $value ?>"><? echo $option; ?></option>
                                    <? endforeach; ?>
                                </select>
                                <?
                                break;
        
                            case 'CHECKBOX': ?>
            
                                <div class="checkboxes">
                                    <? foreach ($arItem['LIST'] as $value => $option) : ?>
                                        <div class="item">
                                            <input id="<? echo $key . '-' . $value; ?>" type="checkbox"
                                                   value="<? echo $value ?>">
                                            <label for="<? echo $key . '-' . $value; ?>"><? echo $option; ?></label>
                                        </div>
                                    <? endforeach; ?>
                                </div>
            
                                <? break;
        
                            case 'RANGE': ?>
                                <div class="range-slider">
                                    <div class="hidden">
                                        <? foreach ($arItem['INPUT_NAMES'] as $value => $option) : ?>
                                            <input id="<? echo $key . '-' . $value; ?>" name="<? echo $option ?>"
                                                   type="text" value="<? echo $arItem['INPUT_VALUE'][$value]; ?>">
                                        <? endforeach; ?>
                                    </div>
                                </div>
                                <?
                                break;
        
                        }

                    endif;
                    ?>
                </div>
            
            <? endforeach; ?>
            
            <div class="actions">
                <input type="hidden" name="set_filter" value="Y"/>
                <button type="submit">Подобрать</button>
            </div>
        
        </form>
    
    </div>
    
    <? /*

        <pre>
            <? print_r($_POST); ?>
        </pre>
        <pre>
            <? print_r($arResult); ?>
        </pre>

        */ ?>

</div>

<? /*
<form name="<?echo $arResult["FILTER_NAME"]."_form"?>" action="<?echo $arResult["FORM_ACTION"]?>" method="get">
    <?foreach($arResult["ITEMS"] as $arItem):
        if(array_key_exists("HIDDEN", $arItem)):
            echo $arItem["INPUT"];
        endif;
    endforeach;?>
    <table class="data-table" cellspacing="0" cellpadding="2">
        <thead>
        <tr>
            <td colspan="2" align="center"><?=GetMessage("IBLOCK_FILTER_TITLE")?></td>
        </tr>
        </thead>
        <tbody>
        <?foreach($arResult["ITEMS"] as $arItem):?>
            <?if(!array_key_exists("HIDDEN", $arItem)):?>
                <tr>
                    <td valign="top"><?=$arItem["NAME"]?>:</td>
                    <td valign="top"><?=$arItem["INPUT"]?></td>
                </tr>
            <?endif?>
        <?endforeach;?>
        </tbody>
        <tfoot>
        <tr>
            <td colspan="2">
                <input type="submit" name="set_filter" value="<?=GetMessage("IBLOCK_SET_FILTER")?>" /><input type="hidden" name="set_filter" value="Y" />&nbsp;&nbsp;<input type="submit" name="del_filter" value="<?=GetMessage("IBLOCK_DEL_FILTER")?>" /></td>
        </tr>
        </tfoot>
    </table>
</form>
*/ ?>

<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

$arTemplateParameters = array(
    
    'FILTER_TITLE' => array(
        "NAME" => 'Название фильтра',
        "TYPE" => 'TEXT',
        "DEFAULT" => ''
    ),
    
    'CATALOG_PATH' => array(
        "NAME" => 'Путь к каталогу',
        "TYPE" => 'TEXT',
        "DEFAULT" => ''
    )

);
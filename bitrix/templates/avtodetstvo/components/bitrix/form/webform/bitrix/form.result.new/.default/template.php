<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<? // TODO: Сделать AJAX-отправку формы ?>

<div id="callback-form" class="form hidden">
    
    <?= $arResult["FORM_HEADER"] ?>
    
    <a href="#" class="close"></a>
    
    <? if ($arResult["isFormErrors"] == "Y"): ?>
        <div class="form-errors">
            <? echo $arResult["FORM_ERRORS_TEXT"]; ?>
        </div>
    <? endif; ?>
    
    <? if (!empty($arResult["FORM_NOTE"])) : ?>
        <div class="form-note">
            <? echo $arResult["FORM_NOTE"] ?>
        </div>
    <? endif; ?>
    
    <? if ($arResult["isFormNote"] !== "Y") : ?>
        
        <? echo $arResult["FORM_HEADER"] ?>
        
        <? if ($arResult["isFormTitle"]) : ?>
            <h6 class="title"><? echo $arResult["FORM_TITLE"] ?></h6>
        <? endif; ?>
        
        <? if (!empty($arResult["FORM_DESCRIPTION"])) : ?>
            <p><? echo $arResult["FORM_DESCRIPTION"] ?></p>
        <? endif; ?>
    
    <? endif; ?>
    
    <? foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion) : ?>
        
        <div class="form-item<? if ($arQuestion["REQUIRED"] == "Y") echo ' required'; ?>"
             data-placeholder="<? echo $arQuestion["CAPTION"]; ?>">
            
            <? /*
            <? if (is_array($arResult["FORM_ERRORS"]) && array_key_exists($FIELD_SID, $arResult['FORM_ERRORS'])): ?>
                <span class="error" title="<? echo $arResult["FORM_ERRORS"][$FIELD_SID] ?>"></span>
            <? endif; ?>

            <label class="form-item-label">
                <? echo $arQuestion["CAPTION"]; ?>
            </label>

            <? echo $arQuestion["IS_INPUT_CAPTION_IMAGE"] == "Y" ? "<br />" . $arQuestion["IMAGE"]["HTML_CODE"] : "" ?>
            */ ?>
            
            <? echo $arQuestion["HTML_CODE"]; ?>
        
        </div>
    
    <? endforeach; ?>
    
    <? if ($arResult["isUseCaptcha"] == "Y") : ?>
        <div class="form-item captcha">
    
            <div class="form-item-caption">
                <? echo GetMessage("FORM_CAPTCHA_TABLE_TITLE") ?>
            </div>
    
            <input type="hidden" name="captcha_sid" value="<? echo htmlspecialcharsbx($arResult["CAPTCHACode"]); ?>"/>
            <img src="/bitrix/tools/captcha.php?captcha_sid=<? echo htmlspecialcharsbx($arResult["CAPTCHACode"]); ?>"/>
    
            <input type="text" name="captcha_word" value="" class="inputtext"/>

        </div>
    <? endif; ?>
    
    <div class="form actions">
        
        <input <? echo(intval($arResult["F_RIGHT"]) < 10 ? "disabled=\"disabled\"" : ""); ?>
            type="submit"
            name="web_form_submit"
            value="<? echo htmlspecialcharsbx(strlen(trim($arResult["arForm"]["BUTTON"])) <= 0 ? GetMessage("FORM_ADD") : $arResult["arForm"]["BUTTON"]); ?>"/>
        
        <? /*
        <? if ($arResult["F_RIGHT"] >= 15): ?>
            <input type="hidden" name="web_form_apply" value="Y"/>
            <input type="submit" name="web_form_apply" value="<? echo GetMessage("FORM_APPLY") ?>"/>
        <? endif; ?>
         <input type="reset" value="<? echo GetMessage("FORM_RESET"); ?>"/>
        */ ?>
    
    </div>
    
    <? /*
    <p><? echo $arResult["REQUIRED_SIGN"]; ?> - <? echo GetMessage("FORM_REQUIRED_FIELDS") ?></p>
    */ ?>
    
    <? echo $arResult["FORM_FOOTER"] ?>
</div>

/**
 * Created by mpa3b on 12.04.2016.
 */

(function ($) {

    $(document).on(
        'ready',
        function () {

            $('input', '.form-item', '#callback-form').each(
                function () {

                    var placeholder = $(this).parent('.form-item').data('placeholder');
                    $(this).attr('placeholder', placeholder);

                }
            );

            $('select', '#callback-form').chosen(
                {
                    width: '100%',
                    placeholder_text_single: $(this).parent('.form-item').data('placeholder')
                }
            );

            $('a[href="#callback"]', '#contact').on(
                'click',
                function (event) {

                    event.preventDefault();

                    $('#callback-form').fadeIn();

                }
            );

            $('a.close', '#contact').on(
                'click',
                function (event) {

                    event.preventDefault();

                    $('#callback-form').fadeOut();

                }
            );

            $('form', '#callback-form').on(
                'submit',
                function (event) {

                    event.preventDefault();

                    console.log('clicked!');

                    $.ajax(
                        {
                            url: $(this).attr("action") + "?AJAX_REQUEST=Y",
                            data: $(this).serialize() + '&web_form_submit=' + $('input[type="submit"]', this).val(),
                            type: 'POST',
                            success: function (data) {

                                // TODO: Вывести Оповещение об успешной отправке с контактными данными

                                $('#callback-form').fadeOut();

                            }
                        }
                    );

                }
            );


        }
    );

})(jQuery);
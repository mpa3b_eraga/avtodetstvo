(function ($) {

    // colorbox settings redefine

    $(document).on(
        'ready',
        function () {

            if ($().colorbox()) {

                $.extend(
                    $.colorbox.settings, {
                        opacity: 0.2,
                        close: 'закрыть',
                        current: '{current} из {total}',
                        previous: 'предыдущий',
                        next: 'следующий',
                        xhrError: 'Ошибка загрузки',
                        imgError: 'Ошибка загрузки изображения',
                        scalePhotos: true,
                        maxWidth: '85%',
                        maxHeight: '85%'
                    }
                );

            }

        }
    );

    // $.wait(5000).then(disco);

    $.wait = function (ms) {
        var defer = $.Deferred();
        setTimeout(function () {
            defer.resolve();
        }, ms);
        return defer;
    };


})(jQuery);
/**
 * Created by ilya_ on 26.03.2016.
 */

(function ($) {

    $(document).on(
        'ready',
        function () {

            var form = $('#one-click-buy');

            $('input[name*="phone"]', form).mask('+7 999 999-99-99');

            $('a.one-click-buy', '.item', '.catalog.section').on(
                'click',
                function (event) {

                    event.preventDefault();

                    var item    = $(this).closest('.item'),
                        element = $('.slide.slick-current', item);

                    var id = element.data('id'),
                        image = element.data('image'),
                        name = element.data('name'),
                        colorname = element.data('colorname'),
                        price = element.data('price'),
                        brand = item.data('brand');
    
                    console.log(id);
                    console.log(name);
                    console.log(image);
                    console.log(colorname);
                    console.log(price);
                    console.log(brand);

                    $('input[name="one-click-buy-form-product-id"]', form).val(id);

                    $('img.image', form).attr('src', image);
                    $('h2.name', form).text(name);
                    $('h4.brand', form).text(brand);
                    $('.value', '.colorname', form).text(colorname);
                    $('p.price', form).text(price);

                    $.colorbox(
                        {
                            inline: true,
                            href: form
                        }
                    );

                }
            );

            $('form', form).on(
                'submit',
                function (event) {

                    event.preventDefault();

                    $.ajax(
                        {
                            url: '/ajax/one-click-buy.php',
                            method: 'POST',
                            dataType: 'json',
                            data: {
                                id: $('input[name="one-click-buy-form-product-id"]', form).val(),
                                name: $('input[name="one-click-buy-form-name"]', form).val(),
                                phone: $('input[name="one-click-buy-form-phone"]', form).val(),
                            },
                            beforeSend: function () {
                                form.addClass('disabled');
                                $('.loader', form).fadeIn();
                            },
                            success: function (data) {

                                setTimeout(
                                    function () {

                                        if (data.type == 'error') {
                                            alert(data.message)
                                        }

                                        if (data.type == 'success') {

                                            $('p.body', '.result', form).text(data.message);
                                            $('.title', '.result', form).text(data.title);

                                            $('.wrapper', form).fadeOut(
                                                function () {
                                                    $('.result', form).hide().removeClass('hidden').show();

                                                    setTimeout(
                                                        function () {
                                                            $.colorbox.close();
                                                        },
                                                        1000
                                                    );

                                                }
                                            );

                                        }

                                    },
                                    500
                                );

                                setTimeout(
                                    function () {
                                        $('.loader', form).fadeOut();
                                        form.removeClass('disabled');
                                        console.log('result: ' + data.type);
                                    },
                                    500
                                );

                            }
                        }
                    );

                }
            );

        }
    );

})(jQuery);
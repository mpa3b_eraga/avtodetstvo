/**
 * Created by mpa3b on 11.02.2016.
 */

$(function(){

    // Данные формы
    function formData(form) {
        var array = form.serializeArray();
            assoc = {};
        if (array.length > 0) {
            for (var i in array) {
                assoc[array[i].name] = array[i].value;
            }
        }
        return assoc;
    };

    // Инициализация слайдера
    function initSlider() {
        var item = $('article.item', '.catalog.section');

        item.each(
            function () {
                var firstURL = $(this).find('.slider .slide.first a').attr('href');
                $(this).find('a.details').attr('href', firstURL);
            }
        );

        item.on(
            'mousewheel',
            function (event) {

                event.preventDefault();

                if (event.deltaY > 0) {
                    $(this).find('.slider').slick('slickPrev');
                }
                if (event.deltaY < 0) {
                    $(this).find('.slider').slick('slickNext');
                }

            }
        );

        item.find('.slider:not(.slick-initialized)').slick(
            {
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: true,
                buttons: false,
                prevArrow: '<a class="slick-arrow slick-prev" href="#"></a>',
                nextArrow: '<a class="slick-arrow slick-next" href="#"></a>',
                useCSS: true,
                infinite: false
            }
        );

        item.find('.slider').on(
            'afterChange',
            function () {
                var url = $(this).find('.slick-active a').attr('href');
                $('a.details', $(this).closest('.item')).attr('href', url);
            }
        );

        item.find('a.color').on(
            'click',
            function (event) {
                event.preventDefault();
                $('.slider', $(this).closest('.item')).slick('slickGoTo', $(this).data('index'));
            }
        );

        item.find('a.hide-colors').on(
            'click',
            function (event) {

                event.preventDefault();
                var parent = $(this).closest('.item');

                $(this).hide();
                $('a.show-colors', parent).show();
                $('.colors-list', parent).hide();
                $('.buy', parent).show();

            }
        );

        item.find('a.show-colors').on(
            'click',
            function (event) {

                event.preventDefault();
                var parent = $(this).closest('.item');

                $(this).hide();
                $('a.hide-colors', parent).show();
                $('.colors-list', parent).show();
                $('.buy', parent).hide();

            }
        );
    }

    // Обновление списка товаров
    function renewItems(renew) {
        data = formData($('form[name="arrFilter_form"]'));
        data.CATALOG_SORT_MODE = $('select[name="CATALOG_SORT_MODE"]').val();
        data.AJAX = 'Y';
        data.PAGEN_1 = (renew) ? 1: parseInt($('#section-contents').data('page')) + 1;
        $.ajax({
            url: window.location.href,
            data: data,
            dataType : 'json',
            method: 'POST',
            cache: false,
            success: function (response) {
                if (renew) {
                    $('#section-contents').html(response.content);
                } else {
                    $('#section-contents').append(response.content);
                }
                $('#total-count').html(response.total_count);
                $('#section-contents').data('page', response.current_page);
                $('article.item', '.catalog.section').find('.slide').removeClass('hidden-before-load');
                $('button#show-next-items').blur();
                if (response.next_page) {
                    $('button#show-next-items').css('display', 'inline-block');
                } else {
                    $('button#show-next-items').css('display', 'none');
                }
                initSlider();
            }
        });
    }

    // Чекбоксы фильтра
    $('form[name="arrFilter_form"] :checkbox').on('change', function(){
        renewItems(true);
    });

    $('form[name="arrFilter_form"]').on('filter-change', function(){
        renewItems(true);
    });

    // Следующие товары
    $('button#show-next-items', '.catalog.section').on('click', function(event) {
        event.preventDefault();
        renewItems(false);
        return false;
    });

    // Сортировка
    $('#catalog-sort-form').on('change', function() {
        renewItems(true);
    });



    $(document).on('ready', function () {
        $('.hidden-before-load').fadeIn();
        $('select#catalog-sort-form-select', '#catalog-sort-form').chosen({
            disable_search: true
        });
        initSlider();
    });

});
<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die(); ?>

</div>

</div>

</main>

<? $APPLICATION->IncludeComponent(
    "bitrix:main.include",
    "aside",
    array(
        "AREA_FILE_SHOW" => "sect",
        "AREA_FILE_SUFFIX" => "below",
        "COMPONENT_TEMPLATE" => ".default",
        "EDIT_TEMPLATE" => "",
        "AREA_FILE_RECURSIVE" => "Y"
    ),
    false
); ?>

<footer id="footer" class="wrap">

    <div class="grid">

        <div class="one-third unit">

            <? $APPLICATION->IncludeComponent(
                "bitrix:catalog.section.list",
                "catalog-navigation-footer",
                array(
                    "ADD_SECTIONS_CHAIN" => "Y",    // Включать раздел в цепочку навигации
                    "CACHE_GROUPS" => "Y",    // Учитывать права доступа
                    "CACHE_TIME" => "36000000",    // Время кеширования (сек.)
                    "CACHE_TYPE" => "A",    // Тип кеширования
                    "COMPONENT_TEMPLATE" => "catalog-navigation-footer",
                    "COUNT_ELEMENTS" => "N",    // Показывать количество элементов в разделе
                    "IBLOCK_ID" => "6",    // Инфоблок
                    "IBLOCK_TYPE" => "catalog",    // Тип инфоблока
                    "SECTION_CODE" => $_REQUEST["SECTION_CODE"],    // Код раздела
                    "SECTION_FIELDS" => "",    // Поля разделов
                    "SECTION_ID" => $_REQUEST["SECTION_ID"],    // ID раздела
                    "SECTION_URL" => "",    // URL, ведущий на страницу с содержимым раздела
                    "SECTION_USER_FIELDS" => "",    // Свойства разделов
                    "SHOW_PARENT_NAME" => "Y",    // Показывать название раздела
                    "TOP_DEPTH" => "1",    // Максимальная отображаемая глубина разделов
                    "BLOCK_TITLE" => "Каталог",    // Вид списка подразделов
                ),
                false
            ); ?>

        </div>

        <div class="two-thirds unit">

            <div class="grid">

                <div class="three-fifths unit">
                    <div class="grid">
                        <div class="three-fifths unit">
                            <? $APPLICATION->IncludeComponent(
                                "bitrix:menu",
                                "bottom-menu",
                                array(
                                    "COMPONENT_TEMPLATE" => "bottom-menu",
                                    "ROOT_MENU_TYPE" => "main",
                                    "MENU_CACHE_TYPE" => "N",
                                    "MENU_CACHE_TIME" => "3600",
                                    "MENU_CACHE_USE_GROUPS" => "Y",
                                    "MENU_CACHE_GET_VARS" => array(),
                                    "MAX_LEVEL" => "1",
                                    "CHILD_MENU_TYPE" => "",
                                    "USE_EXT" => "N",
                                    "DELAY" => "N",
                                    "ALLOW_MULTI_SELECT" => "N"
                                ),
                                false
                            ); ?>
                        </div>
                        <div class="two-fifths unit center-on-mobiles">
                            <a href="https://clck.yandex.ru/redir/dtype=stred/pid=47/cid=2508/*https://market.yandex.ru/shop/166171/reviews"
                               target="_blank">
                                <img
                                    src="https://clck.yandex.ru/redir/dtype=stred/pid=47/cid=2506/*https://grade.market.yandex.ru/?id=166171&amp;action=image&amp;size=1"
                                    alt="Читайте отзывы покупателей и оценивайте качество магазина на Яндекс.Маркете">
                            </a>
                        </div>
                    </div>
                </div>

                <div class="two-fifths unit unit center-on-mobiles">

                    <div class="align-right center-on-mobiles">
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            ".default",
                            array(
                                "AREA_FILE_SHOW" => "sect",
                                "AREA_FILE_SUFFIX" => "social",
                                "COMPONENT_TEMPLATE" => ".default",
                                "EDIT_TEMPLATE" => "",
                                "AREA_FILE_RECURSIVE" => "Y"
                            ),
                            false
                        ); ?>
                    </div>

                    <? $APPLICATION->IncludeComponent(
                        "bitrix:search.form",
                        "footer-search",
                        array(
                            "COMPONENT_TEMPLATE" => "footer-search",
                            "PAGE" => "#SITE_DIR#search/",
                            // "USE_SUGGEST" => "Y"
                        ),
                        false
                    ); ?>

                </div>

            </div>

        </div>

    </div>

    <div class="grid">

        <div class="three-quarters unit">
            <? $APPLICATION->IncludeComponent(
                "bitrix:news.list",
                "brands-list-bottom",
                array(
                    "ACTIVE_DATE_FORMAT" => "d.m.Y",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "AJAX_MODE" => "N",
                    "AJAX_OPTION_ADDITIONAL" => "",
                    "AJAX_OPTION_HISTORY" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "CACHE_FILTER" => "N",
                    "CACHE_GROUPS" => "N",
                    "CACHE_TIME" => "36000000",
                    "CACHE_TYPE" => "A",
                    "CHECK_DATES" => "Y",
                    "COMPONENT_TEMPLATE" => "brands-list-bottom",
                    "DETAIL_URL" => "",
                    "DISPLAY_BOTTOM_PAGER" => "Y",
                    "DISPLAY_DATE" => "N",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "N",
                    "DISPLAY_PREVIEW_TEXT" => "N",
                    "DISPLAY_TOP_PAGER" => "N",
                    "FIELD_CODE" => array(
                        0 => "",
                        1 => "",
                    ),
                    "FILTER_NAME" => "",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "IBLOCK_ID" => "11",
                    "IBLOCK_TYPE" => "-",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "INCLUDE_SUBSECTIONS" => "Y",
                    "MESSAGE_404" => "",
                    "NEWS_COUNT" => "",
                    "PAGER_BASE_LINK_ENABLE" => "N",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => ".default",
                    "PAGER_TITLE" => "Новости",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => "",
                    "PREVIEW_TRUNCATE_LEN" => "",
                    "PROPERTY_CODE" => array(
                        0 => "",
                        1 => "",
                    ),
                    "SET_BROWSER_TITLE" => "N",
                    "SET_LAST_MODIFIED" => "N",
                    "SET_META_DESCRIPTION" => "N",
                    "SET_META_KEYWORDS" => "N",
                    "SET_STATUS_404" => "N",
                    "SET_TITLE" => "N",
                    "SHOW_404" => "N",
                    "SORT_BY1" => "ACTIVE_FROM",
                    "SORT_BY2" => "SORT",
                    "SORT_ORDER1" => "DESC",
                    "SORT_ORDER2" => "ASC",
                    "LIST_LIMIT" => "5"
                ),
                false
            ); ?>
        </div>

        <div class="one-quarter unit">
            <? $APPLICATION->IncludeComponent(
                "bitrix:main.include",
                ".default",
                array(
                    "AREA_FILE_SHOW" => "sect",
                    "AREA_FILE_SUFFIX" => "cards",
                    "COMPONENT_TEMPLATE" => ".default",
                    "EDIT_TEMPLATE" => "",
                    "AREA_FILE_RECURSIVE" => "Y"
                ),
                false
            ); ?>
        </div>

    </div>

    <div id="copyright" class="grid center-on-mobiles">

        <div class="half unit">
            © АвтоДетство, 2012-<? echo date('Y'); ?> гг
        </div>

        <div class="half unit align-right center-on-mobiles">
            <a href="http://xdesign-nn.ru" target="_blank">
                <img src="/upload/content/xd-logo.png" alt="X-Design">
            </a>
        </div>

    </div>

</footer>

<noindex>

    <div class="hidden">
        <? $APPLICATION->IncludeComponent(
            "bitrix:main.include",
            ".default",
            array(
                "AREA_FILE_SHOW" => "sect",
                "AREA_FILE_SUFFIX" => "hidden",
                "COMPONENT_TEMPLATE" => ".default",
                "EDIT_TEMPLATE" => "",
                "AREA_FILE_RECURSIVE" => "Y"
            ),
            false
        ); ?>
    </div>

</noindex>

</body>
</html>
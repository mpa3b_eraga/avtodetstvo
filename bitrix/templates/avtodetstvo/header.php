<!DOCTYPE html>

<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die(); ?>

<?

//CSS
$APPLICATION->SetTemplateCSS(SITE_TEMPLATE_PATH . '/css/reset.css');
$APPLICATION->SetTemplateCSS(SITE_TEMPLATE_PATH . '/css/grid.css');
$APPLICATION->SetTemplateCSS(SITE_TEMPLATE_PATH . '/css/common.css');
$APPLICATION->SetTemplateCSS(SITE_TEMPLATE_PATH . '/css/layout.css');

$APPLICATION->SetTemplateCSS(SITE_TEMPLATE_PATH . '/css/fonts.css');

// JS
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/css-ua.js', false);
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/lib/jquery/jquery.js', false);

$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/common.js', true);

if ($APPLICATION->GetCurPage() === '/') {
    $path = 'front';
    $path_classes = 'page';
    $page_id = $path . '-page';
} else {
    $path = explode("/", trim($APPLICATION->GetCurPage(), '/'));
    $path_classes = implode(' ', $path) . ' page';
    $page_id = implode('-', $path) . '-page';
};

global $USER, $subdomain;

$subdomain = getSubDomain();

?>

<html>

<head>

    <title><? $APPLICATION->ShowTitle(false); ?></title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>

    <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width">

    <meta charset="<? echo LANG_CHARSET; ?>">

    <link rel="shortcut icon" type="image/x-icon" href="<? echo SITE_TEMPLATE_PATH; ?>/favicon.ico"/>
    <link rel="icon" type="image/x-icon" href="<? echo SITE_TEMPLATE_PATH; ?>/favicon.ico"/>

    <? /*
    <!--[if lt IE 9]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE8.js"></script>
    <![endif]-->
    */ ?>

    <!-- META -->
    <? $APPLICATION->ShowMeta("robots"); ?>
    <? $APPLICATION->ShowMeta("keywords"); ?>
    <? $APPLICATION->ShowMeta("description"); ?>

    <!-- CSS -->
    <? $APPLICATION->ShowCSS(); ?>

    <!-- JS Strings -->
    <? $APPLICATION->ShowHeadStrings(); ?>

    <!-- JS Scripts -->
    <? $APPLICATION->ShowHeadScripts(); ?>

</head>

<body id="<? echo $page_id; ?>" class="<? echo $path_classes; ?>" data-subdomain="<? echo $subdomain; ?>">

<? if ($USER->IsAdmin()) : ?>
    <noindex>
        <div id="bitrix-panel" class="wrap">
            <? $APPLICATION->ShowPanel(); ?>
        </div>
    </noindex>
<? endif; ?>

<?
global $arrHeadline;

$arrHeadline = array(
    'ACTIVE' => 'Y',
    'PROPERTY_HEADLINE_VALUE' => 'Y'
);
?>

<? $APPLICATION->IncludeComponent(
    "bitrix:news.list",
    "headline",
    array(
        "ACTIVE_DATE_FORMAT" => "d.m.Y",
        "ADD_SECTIONS_CHAIN" => "N",
        "AJAX_MODE" => "N",
        "AJAX_OPTION_ADDITIONAL" => "",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "N",
        "CACHE_FILTER" => "N",
        "CACHE_GROUPS" => "N",
        "CACHE_TIME" => "36000000",
        "CACHE_TYPE" => "A",
        "CHECK_DATES" => "Y",
        "COMPONENT_TEMPLATE" => "headline",
        "DETAIL_URL" => "",
        "DISPLAY_BOTTOM_PAGER" => "N",
        "DISPLAY_DATE" => "N",
        "DISPLAY_NAME" => "N",
        "DISPLAY_PICTURE" => "N",
        "DISPLAY_PREVIEW_TEXT" => "N",
        "DISPLAY_TOP_PAGER" => "N",
        "FIELD_CODE" => array(
            0 => ""
        ),
        "FILTER_NAME" => "arrHeadline",
        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
        "IBLOCK_ID" => "9",
        "IBLOCK_TYPE" => "content",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
        "INCLUDE_SUBSECTIONS" => "Y",
        "MESSAGE_404" => "",
        "NEWS_COUNT" => "1",
        "PAGER_BASE_LINK_ENABLE" => "N",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "N",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_TEMPLATE" => ".default",
        "PAGER_TITLE" => "Новости",
        "PARENT_SECTION" => "",
        "PARENT_SECTION_CODE" => "",
        "PREVIEW_TRUNCATE_LEN" => "",
        "PROPERTY_CODE" => array(
            0 => "HEADLINE_IMAGE",
            1 => "HEADLINE_TEXT"
        ),
        "SET_BROWSER_TITLE" => "N",
        "SET_LAST_MODIFIED" => "N",
        "SET_META_DESCRIPTION" => "N",
        "SET_META_KEYWORDS" => "N",
        "SET_STATUS_404" => "N",
        "SET_TITLE" => "N",
        "SHOW_404" => "N",
        "SORT_BY1" => "ACTIVE_FROM",
        "SORT_BY2" => "SORT",
        "SORT_ORDER1" => "DESC",
        "SORT_ORDER2" => "ASC",
    ),
    false
); ?>

<aside id="top-panel" class="wrap">

    <div class="grid no-gutters">

        <div id="top-panel-city-select" class="one-fifth unit center-on-mobiles">

            <? $APPLICATION->IncludeComponent("bitrix:catalog.store.list", "store-list", Array(
                "CACHE_TIME" => "36000000",    // Время кеширования (сек.)
                "CACHE_TYPE" => "A",    // Тип кеширования
                "COMPONENT_TEMPLATE" => ".default",
                "MAP_TYPE" => "0",    // Тип карты
                "PATH_TO_ELEMENT" => "",    // Шаблон пути к каталогу STORE(относительно корня)
                "PHONE" => "N",    // Отображать телефон
                "SCHEDULE" => "N",    // Отображать график работы
                "SET_TITLE" => "N",    // Устанавливать заголовок страницы
                "SUBDOMAIN" => $subdomain
            ),
                false
            ); ?>

        </div>

        <div id="top-panel-main-menu" class="three-fifths unit align-center">
            <? $APPLICATION->IncludeComponent(
                "bitrix:menu",
                "top-menu",
                array(
                    "COMPONENT_TEMPLATE" => "top-menu",
                    "ROOT_MENU_TYPE" => "main",
                    "MENU_CACHE_TYPE" => "N",
                    "MENU_CACHE_TIME" => "3600",
                    "MENU_CACHE_USE_GROUPS" => "Y",
                    "MENU_CACHE_GET_VARS" => array(),
                    "MAX_LEVEL" => "1",
                    "CHILD_MENU_TYPE" => "",
                    "USE_EXT" => "N",
                    "DELAY" => "N",
                    "ALLOW_MULTI_SELECT" => "N"
                ),
                false
            ); ?>
            &nbsp;
        </div>

        <div id="top-panel-small-cart" class="one-fifth unit align-right center-on-mobiles">
            <? $APPLICATION->IncludeComponent(
                "bitrix:sale.basket.basket.small",
                "small-cart",
                Array(
                    "COMPONENT_TEMPLATE" => "small-cart",
                    "PATH_TO_BASKET" => "/cart",
                    "PATH_TO_ORDER" => "/cart/checkout",
                    "SHOW_DELAY" => "Y",
                    "SHOW_NOTAVAIL" => "Y",
                    "SHOW_SUBSCRIBE" => "Y"
                )
            ); ?>
        </div>

    </div>
</aside>

<header id="header" class="wrap">
    <div class="grid">

        <div id="logo" class="two-fifths unit center-on-mobiles">
            <? if ($APPLICATION->GetCurPage() == '/') : ?>
                <img src="<? echo SITE_TEMPLATE_PATH . '/img/logo.png'; ?>" alt="АвтоДетство">
                <? ;
            else : ?>
                <a href="/">
                    <img src="<? echo SITE_TEMPLATE_PATH . '/img/logo.png'; ?>" alt="АвтоДетство">
                </a>
            <? endif; ?>
        </div>

        <div class="three-fifths unit">
            <div class="grid">

                <? $APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    ".default",
                    array(
                        "AREA_FILE_SHOW" => "sect",
                        "AREA_FILE_SUFFIX" => "header_" . $subdomain,
                        "COMPONENT_TEMPLATE" => ".default",
                        "EDIT_TEMPLATE" => "",
                        "AREA_FILE_RECURSIVE" => "Y"
                    ),
                    false
                ); ?>

            </div>
        </div>

    </div>
</header>

<aside id="catalog-navigation-wrap" class="wrap">
    <div class="grid">
        <? $APPLICATION->IncludeComponent(
            "bitrix:catalog.section.list",
            "catalog-navigation",
            array(
                "ADD_SECTIONS_CHAIN" => "Y",
                "CACHE_GROUPS" => "N",
                "CACHE_TIME" => "36000000",
                "CACHE_TYPE" => "A",
                "COMPONENT_TEMPLATE" => "catalog-navigation",
                "COUNT_ELEMENTS" => "N",
                "IBLOCK_ID" => "6",
                "IBLOCK_TYPE" => "catalog",
                "SECTION_CODE" => $_REQUEST["SECTION_CODE"],
                "SECTION_FIELDS" => array(
                    0 => "NAME",
                    1 => "",
                ),
                "SECTION_ID" => $_REQUEST["SECTION_ID"],
                "SECTION_URL" => "",
                "SECTION_USER_FIELDS" => array(
                    0 => "UF_SECTION_SUBTITLE",
                    1 => "",
                ),
                "SHOW_PARENT_NAME" => "Y",
                "TOP_DEPTH" => "1"
            ),
            false
        ); ?>

        <? $APPLICATION->IncludeComponent(
            "bitrix:news.list",
            "brands-list",
            array(
                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                "ADD_SECTIONS_CHAIN" => "N",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_ADDITIONAL" => "",
                "AJAX_OPTION_HISTORY" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "CACHE_FILTER" => "N",
                "CACHE_GROUPS" => "N",
                "CACHE_TIME" => "36000000",
                "CACHE_TYPE" => "A",
                "CHECK_DATES" => "Y",
                "COMPONENT_TEMPLATE" => "brands-list",
                "DETAIL_URL" => "",
                "DISPLAY_BOTTOM_PAGER" => "N",
                "DISPLAY_DATE" => "N",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "N",
                "DISPLAY_PREVIEW_TEXT" => "N",
                "DISPLAY_TOP_PAGER" => "N",
                "FIELD_CODE" => array(
                    0 => "",
                    1 => "",
                ),
                "FILTER_NAME" => "",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "IBLOCK_ID" => "11",
                "IBLOCK_TYPE" => "-",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "INCLUDE_SUBSECTIONS" => "Y",
                "MESSAGE_404" => "",
                "NEWS_COUNT" => 9999999999,
                "PAGER_BASE_LINK_ENABLE" => "N",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "Y",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_TEMPLATE" => ".default",
                "PAGER_TITLE" => "Новости",
                "PARENT_SECTION" => "",
                "PARENT_SECTION_CODE" => "",
                "PREVIEW_TRUNCATE_LEN" => "",
                "PROPERTY_CODE" => array(
                    0 => "MAIN",
                    1 => "",
                ),
                "SET_BROWSER_TITLE" => "N",
                "SET_LAST_MODIFIED" => "N",
                "SET_META_DESCRIPTION" => "N",
                "SET_META_KEYWORDS" => "N",
                "SET_STATUS_404" => "N",
                "SET_TITLE" => "N",
                "SHOW_404" => "N",
                "SORT_BY1" => "NAME",
                "SORT_ORDER1" => "ASC",
//                "SORT_BY2" => "SORT",
//                "SORT_ORDER2" => "ASC",
//                "LIST_LIMIT" => "8"
            ),
            false
        ); ?>
    </div>
</aside>

<main id="main" class="wrap">

    <? if ($APPLICATION->GetCurPage() !== '/' && $APPLICATION->GetdirProperty('SHOW_BREADCRUMBS') !== 'N') : ?>
        <aside class="grid">
            <? $APPLICATION->IncludeComponent(
                "bitrix:breadcrumb",
                "breadcrumb",
                array(
                    "COMPONENT_TEMPLATE" => "breadcrumb",
                    "PATH" => "",
                    "SITE_ID" => "-",
                    "START_FROM" => "0"
                ),
                false
            ); ?>
        </aside>
    <? endif; ?>

    <div class="grid">

        <div class="whole unit">

            <? if ($APPLICATION->GetdirProperty('HIDE_TITLE') !== 'Y' && $APPLICATION->GetCurPage() !== '/') : ?>
            <h1 class="center-on-mobiles"><? echo $APPLICATION->ShowTitle(false); ?></h1>
<? endif; ?>
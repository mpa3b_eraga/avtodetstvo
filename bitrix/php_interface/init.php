<?php
/**
 * Created by PhpStorm.
 * User: mpa3b
 * Date: 18.03.2016
 * Time: 13:19
 */

include_once('var.php');

///// Подарки
AddEventHandler("sale", "OnBasketAdd", "ChangeGiftInBasket");
AddEventHandler("sale", "OnBasketUpdate", "ChangeGiftInBasket");
AddEventHandler("sale", "OnBasketDelete", "DeleteGiftFromBasket");

function ChangeGiftInBasket($Id, $arFields)
{
    $product = CCatalogSku::GetProductInfo(
        $arFields['PRODUCT_ID']
    );
    if ($product) {
    
        $productInBasket = CSaleBasket::GetList(
            [], [
            'PRODUCT_ID' => $arFields['PRODUCT_ID'],
            'FUSER_ID' => CSaleBasket::GetBasketUserID(),
            'LID' => SITE_ID,
            'ORDER_ID' => 'NULL'
            ]
        )->Fetch()
        ;
        
        $dbProductsGifts = CIBlockElement::GetList(
            [],
            [
            'IBLOCK_ID' => $product['IBLOCK_ID'],
            'ID' => $product['ID']
            ],
            false,
            false,
            ['PROPERTY_GIFT']
        );
    
        while ($res = $dbProductsGifts->Fetch()) {
        
            $giftInBasket = CSaleBasket::GetList(
                [], [
                'PRODUCT_ID' => $res['PROPERTY_GIFT_VALUE'],
                'FUSER_ID' => CSaleBasket::GetBasketUserID(),
                'LID' => SITE_ID,
                'ORDER_ID' => 'NULL'
                ]
            )->Fetch()
            ;
            
            if ($giftInBasket) {
                CSaleBasket::Update(
                    $giftInBasket['ID'], [
                    'QUANTITY' => $productInBasket['QUANTITY']
                    ]
                );
            }

            else {
    
                $dbGift = CIBlockElement::GetByID($res['PROPERTY_GIFT_VALUE']);
    
                if ($gift = $dbGift->Fetch()) {
                    CSaleBasket::Add(
                        [
                            'PRODUCT_ID' => $res['PROPERTY_GIFT_VALUE'],
                            'PRODUCT_PRICE_ID' => 0,
                            'PRICE' => 0.00,
                            'CURRENCY' => 'RUB',
                            'WEIGHT' => 0,
                            'QUANTITY' => $productInBasket['QUANTITY'],
                            'LID' => SITE_ID,
                            'DELAY' => 'N',
                            'CAN_BUY' => 'Y',
                            'NAME' => $gift['NAME'],
                            'MODULE' => 'catalog',
                            'NOTES' => 'Товар в подарок'
                        ]
                    );
                }
            }
        }
    }
}

function DeleteGiftFromBasket($Id)
{
    $arFields = CSaleBasket::GetByID($Id);
    
    $product = CIBlockElement::GetByID($arFields['PRODUCT_ID'])->Fetch();
    
    $dbProductsGifts = CIBlockElement::GetList(
        [],
        [
            'IBLOCK_ID' => $product['IBLOCK_ID'],
            'ID' => $product['ID']
        ],
        false,
        false,
        ['PROPERTY_GIFT']
    );
    
    while ($res = $dbProductsGifts->Fetch()) {
        $giftInBasket = CSaleBasket::GetList(
            [],
            [
                'PRODUCT_ID' => $res['PROPERTY_GIFT_VALUE'],
                'FUSER_ID' => CSaleBasket::GetBasketUserID(),
                'LID' => SITE_ID,
                'ORDER_ID' => 'NULL'
            ]
        )->Fetch()
        ;
        
        if ($giftInBasket) {
            CSaleBasket::Delete($giftInBasket['ID']);
        }
    }
}

/*

AddEventHandler("main", "OnEndBufferContent", "deleteKernelJs");
AddEventHandler("main", "OnEndBufferContent", "deleteKernelCss");

function deleteKernelJs(&$content)
{
    global $USER, $APPLICATION;
    if ((is_object($USER) && $USER->IsAuthorized()) || strpos($APPLICATION->GetCurDir(), "/bitrix/") !== false) return;
    if ($APPLICATION->GetProperty("save_kernel") == "Y") return;

    $arPatternsToRemove = Array(
        '/<script.+?src=".+?kernel_main\/kernel_main\.js\?\d+"><\/sc ript\>/',
        '/<script.+?src=".+?bitrix\/js\/main\/core\/core[^"]+"><\/sc ript\>/',
        '/<script.+?>BX\.(setCSSList|setJSList)\(\[.+?\]\).*?<\/sc ript>/',
        '/<script.+?>if\(\!window\.BX\)window\.BX.+?<\/sc ript>/',
        '/<script[^>]+?>\(window\.BX\|\|top\.BX\)\.message[^<]+<\/sc ript>/',
    );

    $content = preg_replace($arPatternsToRemove, "", $content);
    $content = preg_replace("/\n{2,}/", "\n\n", $content);
}

function deleteKernelCss(&$content)
{
    global $USER, $APPLICATION;
    if ((is_object($USER) && $USER->IsAuthorized()) || strpos($APPLICATION->GetCurDir(), "/bitrix/") !== false) return;
    if ($APPLICATION->GetProperty("save_kernel") == "Y") return;

    $arPatternsToRemove = Array(
        '/<link.+?href=".+?kernel_main\/kernel_main\.css\?\d+"[^>]+>/',
        '/<link.+?href=".+?bitrix\/js\/main\/core\/css\/core[^"]+"[^>]+>/',
        '/<link.+?href=".+?bitrix\/templates\/[\w\d_-]+\/styles.css[^"]+"[^>]+>/',
        '/<link.+?href=".+?bitrix\/templates\/[\w\d_-]+\/template_styles.css[^"]+"[^>]+>/',
    );

    $content = preg_replace($arPatternsToRemove, "", $content);
    $content = preg_replace("/\n{2,}/", "\n\n", $content);
}

*/

function getSubDomain()
{
    
    $hostArr = explode('.', $_SERVER['HTTP_HOST']);
    
    if (count($hostArr) > 3 && $hostArr[0] !== 'www' && $hostArr[0] !== 'avtodetstvo') $subDomain = $hostArr[0];
    else $subDomain = 'default';
    
    $_SESSION['SUBDOMAIN'] = strtoupper($subDomain);
    
    return $subDomain;
    
}

// header("Last-Modified: " . date("D, d M Y H:i:s", time()) . " GMT");

function moySkladRequest($url, $login, $password, $params = false)
{
    
    $request = curl_init();
    
    if (is_array($params)) {
        
        $url .= '?';
        
        foreach ($params as $param => $value) {
            $url .= $param . '=' . $value . '&';
        }
        
    }
    
    curl_setopt($request, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($request, CURLOPT_USERPWD, $login . ':' . $password);
    curl_setopt($request, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
    
    curl_setopt($request, CURLOPT_URL, $url);
    
    $result = curl_exec($request);
    
    curl_close($request);
    
    return json_decode($result, true);
    
}

function moySkladUpdateStoresList($login, $password)
{
    
    // склады
    
    $url = 'https://online.moysklad.ru/api/remap/1.0/entity/store';
    
    $arRemoteStores = moySkladRequest($url, $login, $password);
    
    if (empty($arRemoteStores['errors'])) {
    
        // текущие склады на сайте
    
        if (CModule::IncludeModule("catalog")) {
        
            $rsLocalStores = CCatalogStore::GetList();
        
            while ($localStore = $rsLocalStores->GetNext()) :
                $existingStores[] = $localStore;
            endwhile;
        
            if (!is_array($existingStores)) :
            
                // если новый
            
                foreach ($arRemoteStores['rows'] as $remoteStore) :
                
                    // добавить новый
                
                    CCatalogStore::Add(
                        array(
                            "TITLE" => $remoteStore['name'],
                            "ACTIVE" => 'Y',
                            "ADDRESS" => $remoteStore['description'],
                            "DESCRIPTION" => null,
                            "IMAGE_ID" => null,
                            "GPS_N" => null,
                            "GPS_S" => null,
                            "PHONE" => null,
                            "SCHEDULE" => null,
                            "XML_ID" => $remoteStore['id']
                        )
                    );
            
                endforeach;
        
            else :
            
                // обновить название
            
                foreach ($arRemoteStores as $remoteStore) :
                
                    $rsLocalStores = CCatalogStore::GetList(
                        array(),
                        array(
                            'XML_ID' => $remoteStore['externalCode']
                        )
                    );
                
                    $localStore = $rsLocalStores->Fetch();
                
                    CCatalogStore::Update(
                        $localStore['ID'],
                        array(
                            "TITLE" => $remoteStore['name'],
                        )
                    );
            
                endforeach;
        
            endif;
        
        }
    
    }
    
}

function moySkladUpdateProductsList($login, $password)
{
    // получение всех товаров
    $url = 'https://online.moysklad.ru/api/remap/1.0/entity/product';
    
    $params = array(
        'limit' => 0,
    );
    
    if (CModule::IncludeModule('currency') && CModule::IncludeModule('iblock')) {
        $currency = CCurrency::GetBaseCurrency();
        $arRemoteProducts = moySkladRequest($url, $login, $password, $params);

        if (empty($arRemoteProducts['errors'])) {
    
            $rsElements = CIBlockElement::GetList(
                array(), array('IBLOCK_ID' => 18), false, false,
                array(
                    'ID',
                    'PROPERTY_CML2_ARTICLE'
                )
            );
    
            $arElements = [];
    
            while ($arElement = $rsElements->GetNext()) {
                $arElements[$arElement['PROPERTY_CML2_ARTICLE_VALUE']] = $arElement;
            }
    
            foreach ($arRemoteProducts['rows'] as $arRemoteProduct) {

                //TODO: Надо будет проверять товары не по артикулу а по внешнему коду
        
                // $arRemoteProduct['article']
                // $arRemoteProduct['externalCode'] и EXTERNAL_ID
        
                // проверить на существование
        
                if (isset($arElements[$arRemoteProduct['article']])) {
            
                    $productID = $arElements[$arRemoteProduct['article']]['ID'];
                    $targetElement = new CIBlockElement;
            
                    if ($arRemoteProduct['archived'] !== 'true') {
                        $targetElement->Update(
                            $productID,
                            array(
                                'NAME' => $arRemoteProduct['name'],
                                'CODE' => CUtil::translit($arRemoteProduct['name'], 'ru'),
                                'ACTIVE' => 'Y',
                                'EXTERNAL_ID' => $arRemoteProduct['externalCode']
                            )
                        );
                    }
            
                    else {
                        $targetElement->Update(
                            $productID,
                            array(
                                'NAME' => $arRemoteProduct['name'],
                                'ACTIVE' => 'N',
                                'CODE' => CUtil::translit($arRemoteProduct['name'], 'ru'),
                                'EXTERNAL_ID' => $arRemoteProduct['externalCode']
                            )
                        );
                
                    }
                    unset($arElements[$arRemoteProduct['article']]);
                } else {
                    // создать элемент
                    $newElement = new CIBlockElement;
                    $newElementFields = array(
                        'NAME' => $arRemoteProduct['name'],
                        'IBLOCK_ID' => 18,
                        'IBLOCK_TYPE' => 'catalog',
                        'CODE' => CUtil::translit($arRemoteProduct['name'], 'ru'),
                        'PREVIEW_TEXT' => $arRemoteProduct['description'],
                        'DETAIL_PICTURE' => null,
                        'EXTERNAL_ID' => $arRemoteProduct['externalCode'],
                        'PROPERTY_VALUES' => array(
                            'MODEL_NAME' => $arRemoteProduct['name'],
                            'CML2_ARTICLE' => $arRemoteProduct['article'],
                            'CML2_LINK' => '',
                        )
                    );
                    if ($arRemoteProduct['archived'] == 'true') {
                        $newElementFields['ACTIVE'] = 'N';
                    }
                    else {
                        $newElementFields['ACTIVE'] = 'Y';
                    }
            
                    $productID = $newElement->Add($newElementFields);
                }
                // Определение товара по названию ТП
//                $dbOffer = CIBlockElement::GetList([], [
//                    'IBLOCK_ID' => 18,
//                    'ID' => $productID
//                ], false, false, [
//                    'ID',
//                    'NAME',
//                    'PROPERTY_MODEL_NAME',
//                    'PROPERTY_CML2_ARTICLE',
//                    'PROPERTY_CML2_LINK'
//                ]);
//                $offer = $dbOffer->getNext();
//                if (!$offer['PROPERTY_CML2_LINK_VALUE']) {
//                    $parts = explode(' ', $offer['NAME']);
//                    $color = '';
//                    while (!empty($parts)) {
//                        $color = trim(array_pop($parts).' '.$color);
//                        $name = trim(implode(' ', $parts));
//                        $dbProduct = CIBlockElement::GetList([], [
//                            'IBLOCK_ID' => 6,
//                            'NAME' => $name
//                        ], false, false, [
//                            'ID',
//                            'NAME'
//                        ]);
//                        $product = $dbProduct->GetNext();
//                        if ($product) {
//                            if (!$name)
//                                $color = '';
//                            $element = new CIBlockElement;
//                            $element->Update($offer['ID'], [
//                                'PROPERTY_VALUES' => [
//                                    'CML2_ARTICLE' => $offer['PROPERTY_CML2_ARTICLE_VALUE'],
//                                    'COLOR_NAME' => $color,
//                                    'MODEL_NAME' => $offer['PROPERTY_MODEL_NAME_VALUE'],
//                                    'CML2_LINK' => $product['ID']
//                                ]
//                            ]);
//                            break;
//                        }
//                    }
//                }
                // цена
                CPrice::SetBasePrice(
                    $productID,
                    $arRemoteProduct['salePrices'][0]['value'] / 100,
                    $currency
                );
                priceToProduct($productID);
            }
    
            // удаление отсутствующих элементов
            if (!empty($arElements)) {
                foreach ($arElements as $arElement) {
                    $element = new CIBlockElement;
                    $element->Update($arElement['ID'],[
                        'ACTIVE' => 'Y'
                    ]);
//                    CIBlockElement::Delete($arElement['ID']);
                }
            }
        }
    }
    
}

function moySkladUpdateProductQuantities($login, $password)
{
    
    // количество товара
    
    // список всех локальных товаров
    
    if (CModule::IncludeModule("iblock")) {
    
        $rsLocalProducts = CIBlockElement::GetList(
            array(),
            array(
                'IBLOCK_ID' => 18
            )
        );
    
        while ($arLocalProduct = $rsLocalProducts->GetNext()) :
            $arLocalProducts[] = $arLocalProduct;
        endwhile;
    
        // список складов
    
        $rsLocalStores = CCatalogStore::GetList([], [
            'ACTIVE' => 'Y'
        ]);

        // количество товаров

        $productsCounts = [];

        // каждый склад
    
        while ($arLocalStore = $rsLocalStores->GetNext()) :
        
            // запрос к АПИ по складу, все товары
        
            $url = 'https://online.moysklad.ru/api/remap/1.0/report/stock/all';
        
            $params = array(
                'limit' => 0,
                'store.id' => $arLocalStore['XML_ID']
            );
        
            $response = moySkladRequest($url, $login, $password, $params);
        
            if (empty($response['errors'])) {
            
                foreach ($response['rows'] as $arRemoteProduct) :
                    $arRemoteProducts[$arRemoteProduct['article']] = $arRemoteProduct;
                endforeach;
            
                // обновить все локальные товары по состоянию по складу

                foreach ($arLocalProducts as $arLocalProduct) :
                
                    // получаем артикул товара для целевого запроса
                
                    $rsArticle = CIBlockElement::GetProperty(
                        $arLocalProduct['IBLOCK_ID'],
                        $arLocalProduct['ID'],
                        array(
                            'EXTERNAL_ID'
                        ),
                        array(
                            'CODE' => 'CML2_ARTICLE'
                        )
                    );
                
                    $article = $rsArticle->Fetch();
                
                    if (!empty($article['VALUE'])) :
                    
                        // обновляем состояние на складе
                    
                        if (CModule::IncludeModule("catalog")) {
                        
                            CCatalogStoreProduct::UpdateFromForm(
                                array(
                                    'PRODUCT_ID' => $arLocalProduct['ID'],
                                    'STORE_ID' => $arLocalStore['ID'],
                                    'AMOUNT' => $arRemoteProducts[$article['VALUE']]['quantity']
                                )
                            );
                        
                        }

                        $productsCounts[$arLocalProduct['ID']][$arLocalStore['ID']] = [
                            'quantity' => $arRemoteProducts[$article['VALUE']]['quantity'],
                            'reserve' => $arRemoteProducts[$article['VALUE']]['reserve']
                        ];
                
                    endif;
            
                endforeach;
                
            }
    
        endwhile;

        // общее количество товарных предложений
        foreach ($productsCounts as $prodId => $counts) {
            $quantity = 0;
            $reserve = 0;
            foreach ($counts as $count) {
                $quantity += $count['quantity'];
                $reserve += $count['reserve'];
            }
            CCatalogProduct::Update(
                $prodId,
                array(
                    'QUANTITY' => $quantity,
                    'QUANTITY_RESERVED' => $reserve
                )
            );
        }
    
    }
    
}


function updateProductsQuantities() {
    if (CModule::IncludeModule("iblock")) {
        $rsProducts = CIBlockElement::GetList([], [
            'IBLOCK_ID' => 18,
            'ACTIVE' => 'Y',
            '!PROPERTY_CML2_LINK' => false
        ], false, false, ['IBLOCK_ID', 'ID', 'PROPERTY_CML2_LINK', 'CATALOG_QUANTITY']);
        $products = [];
        while ($product = $rsProducts->GetNext()) {
            $products[$product['PROPERTY_CML2_LINK_VALUE']][$product['ID']] = [
                'quantity' => $product['CATALOG_QUANTITY'],
                'reserved' => $product['CATALOG_QUANTITY_RESERVED']
            ];
        }
        foreach ($products as $prodId => $counts) {
            $quantity = 0;
            $reserved = 0;
            foreach ($counts as $count) {
                $quantity += $count['quantity'];
                $reserved += $count['reserved'];
            }
            CCatalogProduct::Update($prodId, [
                'QUANTITY' => $quantity,
                'QUANTITY_RESERVED' => $reserved
            ]);
        }
    }
}


// Агент обновления

function moySkladUpdate()
{
    
    $login = 'adm@ice-yalta';
    $password = '123456';
    
    moySkladUpdateStoresList($login, $password);
    
    moySkladUpdateProductsList($login, $password);
    
    moySkladUpdateProductQuantities($login, $password);

    updateProductsQuantities();
    
    return 'moySkladUpdate();';
    
}

if (isset($_GET['xxxxx'])) {
//    CModule::IncludeModule("iblock");
//    $rsLocalProducts = CIBlockElement::GetList(
//        array('ID' => 'ASC'),
//        array(
//            'IBLOCK_ID' => 18
//        ), false, false, [
//            'ID', 'PROPERTY_CML2_ARTICLE', 'DETAIL_PICTURE'
//        ]
//    );
//    while ($arLocalProduct = $rsLocalProducts->GetNext()) :
//        $arLocalProducts[] = $arLocalProduct;
//    endwhile;
//    $same = [];
//    foreach ($arLocalProducts as $prod1) {
//        foreach ($arLocalProducts as $prod2) {
//            $id1 = (int)trim($prod1['ID']);
//            $id2 = (int)trim($prod2['ID']);
//            if ($id1 < $id2) {
//                $art1 = trim($prod1['PROPERTY_CML2_ARTICLE_VALUE']);
//                $art2 = trim($prod2['PROPERTY_CML2_ARTICLE_VALUE']);
//                $pic1 = $prod1['DETAIL_PICTURE'];
//                $pic2 = $prod2['DETAIL_PICTURE'];
//                if ($id1 != $id2 && $art1 == $art2) {
//                    $art = $art1;
//                    if ($pic1) {
//                        $same[] = $id2;
//                    } else {
//                        if ($pic2)
//                            $same[] = $id1;
//                        else
//                            $same[] = $id2;
//                    }
//                }
//            }
//        }
//    }
//    foreach ($same as $id)
//        CIBlockElement::Delete($id);
//    v($same, 1);
    moySkladUpdate();
    exit;
}

AddEventHandler("catalog", "OnPriceAdd", "processPrices");
AddEventHandler("catalog", "OnPriceUpdate", "processPrices");

function processPrices($id, $arFields)
{
    if (CModule::IncludeModule("iblock") && CModule::IncludeModule("catalog") && CModule::IncludeModule('currency')) :
        if ($arFields['PRODUCT_ID'])
            priceToProduct($arFields['PRODUCT_ID']);
    endif;
}

function priceToProduct($productID)
{
    $rsCML2Property = CIBlockElement::GetProperty(
        18,
        $productID,
        array(),
        array(
            'CODE' => 'CML2_LINK'
        )
    );
    $arCML2LinkProperty = $rsCML2Property->Fetch();
    $productId = $arCML2LinkProperty['VALUE'];
    if ($productId) {
        // Получить цену товара
        $arProductPrice = CPrice::GetBasePrice($productId);
        // Получить цену ТП
        $arOfferPrice = CPrice::GetBasePrice($productID);
        // Изменить значение цены если нужно
        if ($arProductPrice['PRICE'] == 0 || $arOfferPrice['PRICE'] <= $arProductPrice['PRICE']) :
            CPrice::SetBasePrice(
                $productId,
                $arOfferPrice['PRICE'],
                CCurrency::GetBaseCurrency()
            );
        endif;
    }
}


// получение деннах пользовательских полей

function GetUserField($entity, $id, $userFieldCode)
{
    $arUserField = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields($entity, $id);
    return $arUserField[$userFieldCode]["VALUE"];
}

// обработка региональных скидок

AddEventHandler("catalog", "OnCondCatControlBuildList", array("CatalogCondCtrlUserProps", "GetControlDescr"));

if (CModule::IncludeModule("catalog")) {
    class CatalogCondCtrlUserProps extends \CCatalogCondCtrlComplex
    {
    
        public static function GetSkladArr()
        {
            $rsLocalStores = CCatalogStore::GetList(array(), array("ACTIVE" => "Y"));
    
            while ($localStore = $rsLocalStores->GetNext()) :
                $existingStores[] = $localStore;
            endwhile;
            $SkladArr = array();
            foreach ($existingStores as $arStore) :
                $code = GetUserField('CAT_STORE', $arStore['ID'], 'UF_SUBDOMAIN');
                $SkladArr[strtolower($code)] = $arStore['TITLE'];
            endforeach;
            return $SkladArr;
        }
    
        public static function GetClassName()
        {
            return __CLASS__;
        }
    
        public static function GetControlID()
        {
            return array('Sklad');
        }
    
        public static function GetControlShow($arParams)
        {
    
            $arControls = static::GetControls();
    
            $arResult = array(
                'controlgroup' => TRUE,
                'group' => FALSE,
                'label' => 'Поля Пользователя',
                'showIn' => static::GetShowIn($arParams['SHOW_IN_GROUPS']),
                'children' => array()
            );
    
            foreach ($arControls as &$arOneControl) {
        
                $arResult['children'][] = array(
                    'controlId' => $arOneControl['ID'],
                    'group' => FALSE,
                    'label' => $arOneControl['LABEL'],
                    'showIn' => static::GetShowIn($arParams['SHOW_IN_GROUPS']),
                    'control' => array(
                        array(
                            'id' => 'prefix',
                            'type' => 'prefix',
                            'text' => $arOneControl['PREFIX']
                        ),
                        static::GetLogicAtom($arOneControl['LOGIC']),
                        static::GetValueAtom($arOneControl['JS_VALUE'])
                    )
        
                );
        
            }
    
            if (isset($arOneControl))
                unset($arOneControl);
            return $arResult;
        }
    
        public static function GetControls($strControlID = FALSE)
        {
            $arControlList = array(
                'Sklad' => array(
                    'ID' => 'Sklad',
                    'FIELD' => 'UF_SUBDOMAIN',
                    'FIELD_TYPE' => 'string',
                    'LABEL' => 'Склад',
                    'PREFIX' => 'поле Склад',
                    'LOGIC' => static::GetLogic(array(BT_COND_LOGIC_EQ, BT_COND_LOGIC_NOT_EQ)),
                    'JS_VALUE' => array(
                        'type' => 'select',
                        'values' => self::GetSkladArr(),
                    ),
                    'PHP_VALUE' => ''
                ),
            );
    
            foreach ($arControlList as &$control) {
                if (!isset($control['PARENT']))
                    $control['PARENT'] = TRUE;
                $control['EXIST_HANDLER'] = 'Y';
                $control['MODULE_ID'] = 'catalog';
                $control['MULTIPLE'] = 'N';
                $control['GROUP'] = 'N';
            }
    
            unset($control);
    
            if ($strControlID === FALSE) {
                return $arControlList;
            }
            elseif (isset($arControlList[$strControlID])) {
                return $arControlList[$strControlID];
            }
            else {
                return FALSE;
            }
        }
    
        public static function Generate($arOneCondition, $arParams, $arControl, $arSubs = FALSE)
        {
            $strResult = '';
            //            file_put_contents("/var/www/eragadesign.com/avtodetstvo/skald_test.txt", 12312312213123);
            $resultValues = array();
            $arValues = FALSE;
            if (is_string($arControl)) {
                $arControl = static::GetControls($arControl);
            }
            $boolError = !is_array($arControl);
    
            if (!$boolError) {
                $arValues = static::Check($arOneCondition, $arOneCondition, $arControl, FALSE);
                $boolError = ($arValues === FALSE);
            }
            if (!$boolError) {
                $boolError = !isset($arControl['MULTIPLE']);
            }
            if (!$boolError) {
    
                $arLogic = static::SearchLogic($arValues['logic'], $arControl['LOGIC']);
    
                if (!isset($arLogic['OP'][$arControl['MULTIPLE']]) || empty($arLogic['OP'][$arControl['MULTIPLE']])) {
                    $boolError = TRUE;
                }
                else {
                    $strField = "\\CatalogCondCtrlUserProps::checkUserField('{$arControl['FIELD']}', '{$arLogic['OP'][$arControl['MULTIPLE']]}', '{$arValues['value']}')";
                    if (is_array($arValues['value'])) {
                        $boolError = TRUE;
                    }
                    else {
                        
                        $strResult = str_replace(
                            array('#FIELD#', '#VALUE#'),
                            array($strField, '"' . EscapePHPString($arValues['value']) . '"'),
                            $arLogic['OP'][$arControl['MULTIPLE']]
                        );
    
                        // file_put_contents("/var/www/eragadesign.com/avtodetstvo/skald_test.txt", $strResult);
                        //$strResult = true;
    
                    }
                    
                    switch ($arControl['FIELD_TYPE']) {
                        case 'int':
                        case 'double':
                            if (is_array($arValues['value'])) {
                                if (!isset($arLogic['MULTI_SEP'])) {
                                    $boolError = TRUE;
                                }
                                else {
                                    foreach ($arValues['value'] as &$value) {
                                        $resultValues[] = str_replace(
                                            array('#FIELD#', '#VALUE#'),
                                            array($strField, $value),
                                            $arLogic['OP'][$arControl['MULTIPLE']]
                                        );
                                    }
                                    unset($value);
                                    $strResult = '(' . implode($arLogic['MULTI_SEP'], $resultValues) . ')';
                                    unset($resultValues);
                                }
                            }
                            else {
                                $strResult = str_replace(
                                    array('#FIELD#', '#VALUE#'),
                                    array($strField, $arValues['value']),
                                    $arLogic['OP'][$arControl['MULTIPLE']]
                                );
                            }
                            break;
                        case 'char':
                        case 'string':
                        case 'text':
                            if (is_array($arValues['value'])) {
                                $boolError = TRUE;
                            }
                            else {
    
                                // $strResult = str_replace(
                                //  array('#FIELD#', '#VALUE#'),
                                //  array($strField, '"'.EscapePHPString($arValues['value']).'"'),
                                //  $arLogic['OP'][$arControl['MULTIPLE']]
                                // );
    
                                $strResult = $strField;
                            }
                            break;
                        case 'date':
                        case 'datetime':
                            if (is_array($arValues['value'])) {
                                $boolError = TRUE;
                            }
                            else {
                                $strResult = str_replace(
                                    array('#FIELD#', '#VALUE#'),
                                    array($strField, $arValues['value']),
                                    $arLogic['OP'][$arControl['MULTIPLE']]
                                );
                            }
                            break;
                    }
                    
                    
                }
            }
            return (!$boolError ? $strResult : FALSE);
        }
    
        public static function checkUserField($strUserField, $strCond, $strValue)
        {
            //global $USER;
            //$arUser = $USER->GetByID($USER->GetID())->Fetch();
            $field = getSubDomain();
            return str_replace(array('#FIELD#', '#VALUE#'), array($field, $strValue), $strCond);
        }
    }
}
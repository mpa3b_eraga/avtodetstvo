<?php
// Отображение значения переменной
function v($data, $stop=false) {
    $v = function($data, $name='...') use(&$v) {
        if ($name == '...') {
            $border_top = 'none';
            echo '<div var style="border:2px solid #000; font-family:Verdana, Geneva, sans-serif; font-style:normal; font-size:13px; background:#eee; margin:5px;">';
        } else {
            $border_top = '1px solid #eee';
        }
        // element
        if (is_object($data)) {
            // object
            $vars = get_object_vars($data);
            $length = count($vars);
            $class = get_class($data);
            $classes = [$class];
            do {
                $class = get_parent_class($class);
                if ($class) {
                    array_unshift($classes, $class);
                }
            } while($class);
            $value = implode(' -> ', $classes);
            if ($length > 0) {
                echo '<div var-line style="padding:5px; background:#aaa; border-top:'.$border_top.'; cursor:pointer;"
            onclick="if (this.nextElementSibling.style.display == \'none\') { this.nextElementSibling.style.display = \'block\'; } else { this.nextElementSibling.style.display = \'none\'; }
            "><b>'.$name.'</b> <i>(Object '.$length.')</i> '.$value.'</div>
            <div var style="display:none; font-family:Verdana, Geneva, sans-serif; font-style:normal; font-size:12px; background:#eee; margin-left:25px;">';
                foreach ($vars as $key => $val) {
                    $v($val, (string)$key);
                }
                echo '</div>';
            } else {
                echo '<div var-line style="padding:5px; background:#aaa; border-top:'.$border_top.'; cursor:pointer;"><b>'.$name.'</b> <i>(Object 0)</i> '.$value.'</div>';
            }
        } elseif (is_array($data)) {
            // array
            $length = count($data);
            if ($length > 0) {
                echo '<div var-line style="padding:5px; background:#aaa; border-top:'.$border_top.'; cursor:pointer;"
            onclick="if (this.nextElementSibling.style.display == \'none\') { this.nextElementSibling.style.display = \'block\'; } else { this.nextElementSibling.style.display = \'none\'; }
            "><b>'.$name.'</b> <i>(Array '.$length.')</i></div>
            <div var style="display:none; font-family:Verdana, Geneva, sans-serif; font-style:normal; font-size:12px; background:#eee; margin-left:25px;">';
                foreach ($data as $key => $value) {
                    $v($value, (string)$key);
                }
                echo '</div>';
            } else {
                echo '<div var-line style="padding:5px; background:#aaa; border-top:'.$border_top.'; cursor:pointer;"><b>'.$name.'</b> <i>(Array 0)</i></div>';
            }
        } else {
            // value
            echo '<div var-line style="padding:5px; background:#ccc; border-top:'.$border_top.';">';
            switch (gettype($data)) {
                case 'string':
                    $length = (strlen($data)) ? strlen($data): '0';
                    echo '<b>'.$name.'</b> <i>(String '.$length.')</i> <pre style="display:inline-block; vertical-align:top; margin:0; font-family:Verdana, Geneva, sans-serif; font-style:normal; font-size:12px;">'.$data.'</pre>';
                    break;
                case 'integer':
                    $value = (string)$data;
                    echo '<b>'.$name.'</b> <i>(Integer)</i> '.$value;
                    break;
                case 'boolean':
                    $value = ($data) ? 'true': 'false';
                    echo '<b>'.$name.'</b> <i>(Boolean)</i> '.$value;
                    break;
                case 'NULL':
                    echo '<b>'.$name.'</b> <i>(Null)</i>';
                    break;
                default:
                    echo '<b>'.$name.'</b> <i>'.gettype($data).'</i> => <pre style="display:inline-block; vertical-align:top; margin:0; font-family:Verdana, Geneva, sans-serif; font-style:normal; font-size:12px;">'.print_r($data, 1).'</pre>';
                    break;
            }
            echo '</div>';
        }
        if ($name == '...') {
            echo '</div>';
        }
    };
    $v($data);
    if ($stop) {
        exit();
    }
}
BxmodPropFilter = {
    // Сбор данных из всех полей
    Collect: function ( el, skipCollectAll )
    {
        var parent = el.closest("div.BxmodPropFilterPropsFieldsBlock");
        var iblock = el.closest("div.BxmodPropFilterPropsFields").find("select.BxmodPropFilterPropsFieldIblock").val();
        var type = el.closest("div.BxmodPropFilterPropsFields").find("select.BxmodPropFilterPropsFieldType").val();
        
        var data = iblock + "," + type + ",";
        var fields = BxmodPropFilter.GetFields( el );
        
        jQuery.each( fields, function (key, field) {
            if ( field["field"].hasClass("BxmodPropFilterPropsFieldValue") ) {
                if ( field["field"].attr("disabled") ) {
                    data = data + field["field"].closest("div").find("input.BxmodPropFilterPropsFieldValue").val() + ",";
                    return;
                } else {
                    data = data + field["field"].closest("div").find("select.BxmodPropFilterPropsFieldValue").val() + ",";
                    return;
                }
            }
            
            data = data + field["field"].val() + ",";
        });
        
        parent.find("input.data").val( data.substr(0, data.length - 1) );
        
        if ( skipCollectAll != true ) {
            BxmodPropFilter.CollectAll(el);
        }
        
        return data;
    },
    
    // Сбор данных из всех полей
    CollectAll: function ( el )
    {
        var parent = el.closest("div.BxmodPropFilterPropsFields");
        var allDataField = parent.find("input.allData");
        var iblock = el.closest("div.BxmodPropFilterPropsFields").find("select.BxmodPropFilterPropsFieldIblock").val();
        var allData = "";
        
        parent.find(".BxmodPropFilterPropsFieldsBlock").each(function(i){
            BxmodPropFilter.Collect( jQuery(this).find("select:first"), true );
            
            allData = allData + "("+ jQuery(this).find("input.data").val() +")";
        });
        
        allDataField.val( allData );
    },
    
    // Список полей, принимающих участие в сборе информации
    GetFields: function ( el )
    {
        var parent = el.closest("div.BxmodPropFilterPropsFieldsBlock");
        
        return {
            0 : { "field" : parent.find("select.BxmodPropFilterPropsFieldProp"), "index" : "prop" },
            1 : { "field" : parent.find("select.BxmodPropFilterPropsFieldCond"), "index" : "cond" },
            2 : { "field" : parent.find(".BxmodPropFilterPropsFieldValue"), "index" : "value" }
        };
    },
    
    // Декодирование строк
    Urldecode: function (utftext) {
        utftext = unescape(utftext.replace(/\+/g, ' '));
        
        var string = ""; 
        var i = 0; 
        var c = 0;
        var c2 = 0; 
 
        while ( i < utftext.length ) { 
 
            c = utftext.charCodeAt(i); 
 
            if (c < 128) { 
                string += String.fromCharCode(c); 
                i++; 
            } 
            else if((c > 191) && (c < 224)) { 
                c2 = utftext.charCodeAt(i+1); 
                string += String.fromCharCode(((c & 31) << 6) | (c2 & 63)); 
                i += 2; 
            } 
            else { 
                c2 = utftext.charCodeAt(i+1); 
                c3 = utftext.charCodeAt(i+2); 
                string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63)); 
                i += 3; 
            } 
 
        } 
 
        return string;
    },

    
    // Отправка данных
    LoadData: function (el, notCollect)
    {
        var parent = el.closest("div.BxmodPropFilterPropsFieldsBlock");
        var fields = BxmodPropFilter.GetFields( el );
        
        if ( notCollect != undefined && notCollect != null ) {
            var data = parent.find("input.data").val();
        } else {
            var data = BxmodPropFilter.Collect( el );
        }
        
        parent.find("select, input.BxmodPropFilterPropsFieldValue").attr("disabled", "disabled");
        
        jQuery.ajax({
            url: "/bitrix/tools/bxmod_propfilter.php",
            data: "data=" + data,
            dataType: "json",
            success: function(data){
                
                jQuery.each( fields, function (key, field) {
                    
                    field["field"].attr("disabled", "disabled");
                    field["field"].find("optgroup, option").remove();
                    
                    if ( data[ field["index"] ]["type"] == "text" ) {
                        parent.find("select." + field["field"].attr("class")).hide();
                        parent.find("input." + field["field"].attr("class")).attr("disabled", false).show();
                        parent.find("input." + field["field"].attr("class")).val( data[ field["index"] ]["value"] );
                    } else {
                        if ( parent.find("input." + field["field"].attr("class")).length > 0 ) {
                            parent.find("input." + field["field"].attr("class")).hide();
                            parent.find("select." + field["field"].attr("class")).attr("disabled", false).show();
                        }

                        jQuery.each( data[ field["index"] ], function (ind, val) {
                            selected = val["selected"] == "1" ? ' selected="selected"' : '';
                            field["field"].append('<option value="'+ val["id"] +'"'+ selected +'>'+ BxmodPropFilter.Urldecode(val["title"]) +'</option>');
                        });
                        
                        if ( data[ field["index"] ].length > 1 ) field["field"].attr("disabled", false);
                        else field["field"].attr("disabled", "disabled");
                    }
                });
                
                parent.find("select:first").attr("disabled", false);
                
                if ( !notCollect ) {
                    BxmodPropFilter.Collect( el );
                }
            }
        });
    },
    
    // Добавление нового блока
    AddBlock: function (el)
    {
        var par = el.closest("div.BxmodPropFilterPropsFields");
        var newBlock = par.find(".BxmodPropFilterPropsFieldsBlock:first").clone();
        
        newBlock.find("input, select").val("");
        newBlock.insertBefore( par.find(".BxmodPropFilterPropsFieldAdd") );
        newBlock.removeClass("loaded");
        
        return newBlock;
    },
    
    // Бинд событий, стартовая подгрузка
    Start: function ()
    {
        jQuery(".BxmodPropFilterPropsFields").not(".loaded").each(function(i){
            jQuery(this).addClass("loaded");
            
            // Начальные данные
            var elem = jQuery(this);
            var data = jQuery(this).find("input.allData").val();
            data = data.substr(1, (data.length - 2));
            var startData = data.split(")(");
            
            jQuery(startData).each(function(i, val){
                if ( parseInt( elem.find("select.BxmodPropFilterPropsFieldIblock").val() ) < 1 ) {
                    var ids = val.split(",");
                    elem.find("select.BxmodPropFilterPropsFieldIblock").find("option[value='"+ids[0]+"']").attr("selected", "selected");
                    elem.find("select.BxmodPropFilterPropsFieldType").find("option[value='"+ids[1]+"']").attr("selected", "selected");
                }
                if ( elem.find(".BxmodPropFilterPropsFieldsBlock").eq(i).length > 0 ) {
                    elem.find(".BxmodPropFilterPropsFieldsBlock").eq(i).find("input.data").val( val );
                    BxmodPropFilter.LoadData( elem.find(".BxmodPropFilterPropsFieldsBlock").eq(i), true );
                } else {
                    var newBlock = BxmodPropFilter.AddBlock( elem.find(".BxmodPropFilterPropsFieldAdd") );
                    newBlock.find("input.data").val( val );
                    BxmodPropFilter.LoadData( newBlock.find("select:first"), true );
                }
            });
            
            // Биндим событие выбора инфоблока
            jQuery(this).find("select.BxmodPropFilterPropsFieldIblock").change(function(){
                // Удаляем лишние блоки с условиями
                jQuery(this).closest(".BxmodPropFilterPropsFields").find(".BxmodPropFilterPropsFieldsBlock").find("input, select").val("");
                jQuery(this).closest(".BxmodPropFilterPropsFields").find(".BxmodPropFilterPropsFieldsBlock").not(":first").remove();
                BxmodPropFilter.LoadData( jQuery(this).closest(".BxmodPropFilterPropsFields").find("div.BxmodPropFilterPropsFieldsBlock:first select") );
            });
            
            // Биндим событие выбора типа
            jQuery(this).find("select.BxmodPropFilterPropsFieldType").change(function(){
                BxmodPropFilter.Collect( jQuery(this) );
            });
        });
        
        jQuery(".BxmodPropFilterPropsFieldsBlock").not(".loaded").each(function(i){
            jQuery(this).addClass("loaded");
            
            // Биндим событие выбора в списках
            jQuery(this).find("select").change(function(){
                BxmodPropFilter.LoadData( jQuery(this) );
            });
            
            // Биндим событие выбора в списках
            jQuery(this).find("input.BxmodPropFilterPropsFieldValue").keyup(function(){
                BxmodPropFilter.CollectAll( jQuery(this) );
            });
            jQuery(this).find("input.BxmodPropFilterPropsFieldValue").blur(function(){
                BxmodPropFilter.CollectAll( jQuery(this) );
            });
            
            // Подгружаем данные
            //BxmodPropFilter.LoadData( jQuery(this).find("select.BxmodPropFilterPropsFieldIblock"), true );
        });
        
        // Бинд события добавления блока
        jQuery(".BxmodPropFilterPropsFieldAdd").not(".loaded").each(function(i){
            jQuery(this).addClass("loaded");
            jQuery(this).click(function(){
                BxmodPropFilter.AddBlock( jQuery(this), false );
                return false;
            });
        });
    }
};

jQuery(document).ready(function() {
    // Пускаем в цикл бинд. На случай динамического добавления полей
    setInterval('BxmodPropFilter.Start()', 500);
});
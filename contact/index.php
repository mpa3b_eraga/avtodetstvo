<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Контакты");
$APPLICATION->SetAdditionalCSS('/contact/style.css');

$shops = [];
$dbRes = CIBlockElement::GetList([], [
    'IBLOCK_ID' => 14,
    'ACTIVE' => 'Y'
], false, false, [
    'ID',
    'PROPERTY_ADDRESS',
    'PROPERTY_DETAILS',
    'PROPERTY_LOCATION',
    'PROPERTY_TIME_START',
    'PROPERTY_TIME_END'
]);
while ($shop = $dbRes->GetNext()) {
    $shops[$shop['ID']] = $shop;
}

?>

<div id="contacts-page">

    <script src="https://api-maps.yandex.ru/2.0-stable/?load=package.full&amp;lang=ru-RU" type="text/javascript"></script>
    <script type="text/javascript">
        ymaps.ready(function () {
            // Создание экземпляра карты и его привязка к созданному контейнеру.
            <? $loc = explode(',', current($shops)['PROPERTY_LOCATION_VALUE']); ?>
            var myMap = new ymaps.Map("map_init", {
                center: [<?= $loc[0] ?>, <?= $loc[1] ?>],
                zoom: 15,
                behaviors: ['default', 'scrollZoom']
            });
        
            // Создаем коллекцию геообъектов.
            myCollection = new ymaps.GeoObjectCollection();
        
            <? foreach ($shops as $id => $shop) { ?>
                    <? $loc = explode(',', $shop['PROPERTY_LOCATION_VALUE']); ?>
                myCollection.add(new ymaps.Placemark([<?= $loc[0] ?>, <?= $loc[1] ?>], {
                    hintContent: '<?= $shop['PROPERTY_ADDRESS_VALUE'] ?>',
                    balloonContent: '',
                    balloonContentHeader: '<?= $shop['PROPERTY_ADDRESS_VALUE'] ?>',
                    balloonContentBody: '',
                    balloonContentFooter: ''
                }));
            <? } ?>
        
            myMap.geoObjects.add(myCollection);
        
            // Устанавливаем карте центр и масштаб так, чтобы охватить коллекцию целиком.
            myMap.setBounds(myCollection.getBounds(), {
                checkZoomRange: true
            });
        
            $(".address a").bind("click", function(e){
                e.preventDefault();
                geoPoint = $(this).data("geopoint")
        
                myMap.panTo(geoPoint, {
                    flying: true,
                    duration: 1000,
                    callback: function(){
                        myMap.setZoom(15, {duration: 1000});
                    }
                });
            });
        });
    </script>
    <div class="map_init" id="map_init" style="width:100%; height:400px;"></div>

    <h2>Посмотреть, выбрать автокресло или забрать свой заказ можно по адресам:</h2>
    <div class="row">
        <div class="left-block pull-left">
            <? foreach ($shops as $id => $shop) { ?>
                <div class="row address-row">
                    <div class="address">
                        <? $loc = explode(',', $shop['PROPERTY_LOCATION_VALUE']); ?>
                        <a href="#" data-geopoint="[<?= $loc[0] ?>, <?= $loc[1] ?>]"><?= $shop['PROPERTY_ADDRESS_VALUE'] ?></a>
                        <span class="address-details"><?= $shop['PROPERTY_DETAILS_VALUE'] ?></span>
                    </div>
                    <? $start = explode(':', $shop['PROPERTY_TIME_START_VALUE']); ?>
                    <? $end = explode(':', $shop['PROPERTY_TIME_END_VALUE']); ?>
                    <div class="work-hours">
                        <?= $start[0] ?><sup><?= $start[1] ?></sup> — <?= $end[0] ?><sup><?= $end[1] ?></sup> каждый день
                    </div>
                </div>
            <? } ?>
            <h3>Реквизиты</h3>
            <p>
                ИП Умеренкова Олеся Александровна<br>
                ОГРНИП  313524907300020
            </p>
        </div>
        <div class="right-block pull-right">
            <div class="contact-phone pull-right">
                Единый телефон для справок
                <div class="value">+7 930 707 01 03</div>
                E-mail
                <div class="value">info@avtodetstvo.ru</div>
            </div>
        </div>
    </div>
    
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
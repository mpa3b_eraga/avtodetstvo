<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Доставка");
$APPLICATION->SetAdditionalCSS('/delivery/style.css');

?>

<div class="page-delivery">
    <h2>Отправка в регионы</h2>
    <p>100% предоплата за заказ.&nbsp;</p>
    <p><strong>Внимание:</strong>&nbsp;товар бронируется за вами только после оплаты.</p>
    <p>Заказ будет отправлен в течение 1 дня&nbsp;с момента оплаты.</p>
    <table border="0" cellpadding="1" cellspacing="1" class="table table-striped" style="width:600px;">
        <thead>
        <tr>
            <th scope="col">
                <p><strong>Стоимость&nbsp;заказа</strong></p></th>
            <th scope="col">
                <p><strong>Доставка&nbsp;до&nbsp;ТК*</strong></p></th>
            <th scope="col">
                <p><strong>Стоимость пересылки до терминала ТК* в Вашем городе</strong></p></th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>
                <p>до&nbsp;6000&nbsp;<span class="rouble">a</span></p></td>
            <td>
                <p>200 <span class="rouble">a</span></p></td>
            <td>
                <p>по тарифам ТК* за счет покупателя &nbsp; &nbsp; &nbsp; &nbsp;</p></td>
        </tr>
        <tr>
            <td>
                <p>от&nbsp;6000&nbsp;<span class="rouble">a</span></p></td>
            <td>
                <p>бесплатно</p></td>
            <td>
                <p>по тарифам ТК* за счет покупателя</p></td>
        </tr>
        <tr>
            <td>
                <p>от&nbsp;10&nbsp;000&nbsp;<span class="rouble">a</span></p></td>
            <td>
                <p>бесплатно</p></td>
            <td>
                <p>бесплатно**</p>
                <p><span style="color:#696969;">** автокресла со сниженной ценой (участвующие в распродаже), доставляются бесплатно только до ТК - пересылка за счет покупателя.</span></p></td>
        </tr>
        </tbody>
    </table>
    <p>ТК* - транспортная компания</p>
    <h2>​Самовывоз</h2>
    <p>Вы можете забрать заказ самостоятельно по адресам:</p>
    <ul>
        <li><strong>г. Нижний Новгород, ул. Ильинская, 57 (9:00— 20:00 каждый день)</strong></li>
        <li><strong>г. Дзержинск, ул. Петрищева, 31 Б (9:00— 20:00 каждый день)</strong></li>
        <li><strong>г. Казань, переулок Дорожный, 6 А&nbsp;(9:00— 20:00 каждый день)</strong></li></ul>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
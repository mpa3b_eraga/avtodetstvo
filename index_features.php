<div class="grid align-center">
    
    <div class="one-third unit">
        <img src="/upload/content/adv_1.png" alt="Лучшая цена">
        <h2>Лучшая цена</h2>
        <p>
            Нашли цену ниже?<br>
            Забирайте со скидкой!
        </p>
    </div>
    
    <div class="one-third unit">
        <img src="/upload/content/adv_2.png" alt="Расширенная Гарантия">
        <h2>Расширенная Гарантия</h2>
        <p>
            Дарим бесплатное<br>
            обслуживание
        </p>
    </div>
    
    <div class="one-third unit">
        <img src="/upload/content/adv_3.png" alt="Бесплатная доставка">
        <h2>Бесплатная доставка</h2>
        <p>
            на сумму от 6000<br>
            a Доставляем в день заказа!
        </p>
    </div>

</div>


<?php
/**
 * Created by PhpStorm.
 * User: mpa3b
 * Date: 18.03.2016
 * Time: 13:19
 */
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");


    // получение всех товаров
    $url = 'https://online.moysklad.ru/api/remap/1.0/entity/product';

    $params = array(
        'limit' => 0,
    );

    if (CModule::IncludeModule('currency') && CModule::IncludeModule('iblock')) {
        $currency = CCurrency::GetBaseCurrency();
        $arRemoteProducts = moySkladRequest('https://online.moysklad.ru/api/remap/1.0/entity/store', 'adm@ice-yalta',
        '123456', false);

        if (empty($arRemoteProducts['errors'])) {

            $rsElements = CIBlockElement::GetList(
                array(),
                array(
                    'IBLOCK_ID' => 18
                ),
                false,
                false,
                array(
                    'ID',
                    'PROPERTY_CML2_ARTICLE'
                )
            );

            $arElements = [];

            while ($arElement = $rsElements->GetNext()) {
                $arElements[$arElement['PROPERTY_CML2_ARTICLE_VALUE']] = $arElement;

            }

            foreach ($arRemoteProducts['rows'] as $arRemoteProduct) {

                //TODO: Надо будет проверять товары не по артикулу а по внешнему коду

                // $arRemoteProduct['article']
                // $arRemoteProduct['externalCode'] и EXTERNAL_ID

                // проверить на существование


                if (isset($arElements[$arRemoteProduct['article']])) {

                    $productID = $arElements[$arRemoteProduct['article']]['ID'];
                    $targetElement = new CIBlockElement;

                    if ($arRemoteProduct['archived'] !== 'true') {
                        $targetElement->Update(
                            $productID,
                            array(
                                'NAME' => $arRemoteProduct['name'],
                                'CODE' => CUtil::translit($arRemoteProduct['name'], 'ru'),
                                'ACTIVE' => 'Y',
                                'EXTERNAL_ID' => $arRemoteProduct['externalCode']
                            )
                        );
                    }

                    else {
                        $targetElement->Update(
                            $productID,
                            array(
                                'NAME' => $arRemoteProduct['name'],
                                'ACTIVE' => 'N',
                                'CODE' => CUtil::translit($arRemoteProduct['name'], 'ru'),
                                'EXTERNAL_ID' => $arRemoteProduct['externalCode']
                            )
                        );

                    }
                    unset($arElements[$arRemoteProduct['article']]);
                } else {
                    // создать элемент
                    $newElement = new CIBlockElement;
                    $newElementFields = array(
                        'NAME' => $arRemoteProduct['name'],
                        'IBLOCK_ID' => 18,
                        'IBLOCK_TYPE' => 'catalog',
                        'CODE' => CUtil::translit($arRemoteProduct['name'], 'ru'),
                        'PREVIEW_TEXT' => $arRemoteProduct['description'],
                        'DETAIL_PICTURE' => null,
                        'EXTERNAL_ID' => $arRemoteProduct['externalCode'],
                        'PROPERTY_VALUES' => array(
                            'MODEL_NAME' => $arRemoteProduct['name'],
                            'CML2_ARTICLE' => $arRemoteProduct['article'],
                            'CML2_LINK' => '',
                        )
                    );
                    if ($arRemoteProduct['archived'] == 'true') {
                        $newElementFields['ACTIVE'] = 'N';
                    }
                    else {
                        $newElementFields['ACTIVE'] = 'Y';
                    }

                    $productID = $newElement->Add($newElementFields);
                }

                // цена
                CPrice::SetBasePrice(
                    $productID,
                    $arRemoteProduct['salePrices'][0]['value'] / 100,
                    $currency
                );
                priceToProduct($productID);
            }

            // удаление отсутствующих элементов
            if (!empty($arElements)) {
                foreach ($arElements as $arElement) {
                    $element = new CIBlockElement;
                    $element->Update($arElement['ID'],[
                        'ACTIVE' => 'Y'
                    ]);
//                    CIBlockElement::Delete($arElement['ID']);
                }
            }
        }
    }


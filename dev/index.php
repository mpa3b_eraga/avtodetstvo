<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Отладка");
?>

<?

if (CModule::IncludeModule("iblock")) {
    
    $rsOffers = CIBlockElement::GetList(
        array(
            'CODE' => 'PROPERTY_CML2_ARTICLE'
        ),
        array(
            'IBLOCK_ID' => 18,
        ),
        false,
        false,
        array(
            'ID',
            'NAME',
            'PROPERTY_CML2_ARTICLE',
        )
    );
    
    while ($arOffer = $rsOffers->GetNext()) :
        
        $arOffer['IMAGE'] = CFile::GetPath($arOffer['PREVIEW_PICTURE']);
        $arOffers[$arOffer['ID']] = $arOffer;
    
    endwhile;
    
}

foreach ($arOffers as $i => $arOffer) :
    
    if ($arOffers[$i]['ID'] !== $arOffers[array_search($arOffer['PROPERTY_CML2_ARTICLE_VALUE'], $arOffers)]['ID']):
    
    endif;

endforeach;


?>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>
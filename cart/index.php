<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Ваш заказ");
?>

<? $APPLICATION->IncludeComponent(
    "bitrix:sale.basket.basket",
    "cart",
    Array(
        "ACTION_VARIABLE" => "action",
        "COLUMNS_LIST" => array("NAME", "PROPS", "DELETE", "DELAY", "QUANTITY", "PROPERTY_IMAGE"),
        "COMPONENT_TEMPLATE" => ".default",
        "COUNT_DISCOUNT_4_ALL_QUANTITY" => "N",
        "HIDE_COUPON" => "N",
        "OFFERS_PROPS" => array("CML2_ARTICLE"),
        "PATH_TO_ORDER" => "/cart/checkout",
        "PRICE_VAT_SHOW_VALUE" => "N",
        "QUANTITY_FLOAT" => "N",
        "SET_TITLE" => "Y",
        "TEMPLATE_THEME" => "",
        "USE_PREPAYMENT" => "N"
    )
); ?>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>